<?php

/*
|--------------------------------------------------------------------------
| Основные пользовательские страницы
|--------------------------------------------------------------------------
*/

Route::middleware(['web'])->group(function () {
    /** Домашняя страница */
    Route::view('/', 'app')->name('home');

    /** Страницы методик */
    Route::view('/method/{path?}', 'app')->where('path', '.*');
    Route::view('/methods', 'app');

    /** Всероссийская научно-техническая конференция */
    Route::view('conf/{path?}', 'app')->where('path', '.*');
});

Route::middleware(['auth'])->group(function () {
    /** Страницы прохождения тестирования */
    Route::view('test/{id}', 'app');
    Route::view('tests', 'app')->name('test.main');

    /** Страница настроек профиля */
    Route::view('/profile/{path?}', 'app')->where('path', '.*')->name('profile.main');
});

/*
|--------------------------------------------------------------------------
| Документы из локального диска и базы данных
|--------------------------------------------------------------------------
*/

Route::get('/document', 'Database\mddb\DocumentController@load');

Route::get('/document/local/{path}', 'Database\mddb\DocumentController@localLoad')
    ->where('path', '.*');

Route::get('/document/conf', 'Database\conf\DocumentController@load');

/*
|--------------------------------------------------------------------------
| Логирование ошибок и временные ссылки
|--------------------------------------------------------------------------
*/

Route::view('/logs', 'develop.logs')->middleware(['auth']);

Route::redirect('/api-doc', 'https://cmkd-rest-api.postman.co/collection/13826413-95a2481c-f7a1-4aca-857d-cfeec8baba91?source=rip_html');

/** Информационная страница об Углеве В.А. */
Route::view('uglev-about', 'other.uglev-about');

/*
|--------------------------------------------------------------------------
| Аутентификация, авторизация и регистрация
|--------------------------------------------------------------------------
*/

Route::get('/auth', 'Database\mddb\AuthController@auth');

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'Auth\RegisterController@register');
Route::get('/register/{login}/exists', 'Auth\RegisterController@userLoginExists');

/*
|--------------------------------------------------------------------------
| Отладка и пробы
|--------------------------------------------------------------------------
*/

Route::middleware('debug')->group(function () {
    Route::get('/purpose-grid-only', 'Other\DevelopController@grid');
    Route::any('/purpose-test-only', 'Other\DevelopController@test');
});

Route::get('/cache/flm-builder', 'Other\CacheController@flmBuilderStat');

/*
|--------------------------------------------------------------------------
| Консольные пути по выдачи Blade компонентов [ ПЕРЕПИСАТЬ ]
|--------------------------------------------------------------------------
*/

/**
 * Консоль
 */
Route::name('console.')->prefix('console')->middleware(['auth', 'permission:admin|console'])->group(function () {
    Route::get('', 'ConsoleController@main')->name('main');

    Route::name('spec.')->group(function () {
        Route::get('specialities', 'ConsoleController@Speciality')->name('main');
    });

    Route::name('branch.')->group(function () {
        Route::get('branches', 'ConsoleController@Branch')->name('main');
        Route::post('branches', 'ConsoleController@BranchNext')->name('next');
        Route::post('branches/search', 'ConsoleController@BranchSearch')->name('search');
    });

    Route::name('subgroup.')->group(function () {
        Route::get('subgroups', 'ConsoleController@Subgroup')->name('main');
        Route::post('subgroups', 'ConsoleController@SubgroupNext')->name('next');
        Route::post('subgroups/search', 'ConsoleController@SubgroupSearch')->name('search');
    });

    Route::name('disc.')->group(function () {
        Route::get('disciplines', 'ConsoleController@Discipline')->name('main');
        Route::post('disciplines', 'ConsoleController@DisciplineNext')->name('next');
        Route::post('disciplines/search', 'ConsoleController@DisciplineSearch')->name('search');
    });

    Route::name('competence.')->group(function () {
        Route::get('competences', 'ConsoleController@Competence')->name('main');
        Route::post('competence', 'ConsoleController@CompetenceNext')->name('next');
    });

    Route::name('test.')->group(function () {
        Route::get('tests', 'ConsoleController@Test')->name('main');
        Route::post('tests', 'ConsoleController@TestNext')->name('next');
        Route::post('tests/search', 'ConsoleController@TestSearch')->name('search');
    });

    Route::name('quest.')->group(function () {
        Route::get('quests', 'ConsoleController@Quest')->name('main');
        Route::post('quests', 'ConsoleController@QuestNext')->name('next');
        Route::post('quests/search', 'ConsoleController@QuestSearch')->name('search');
    });

    Route::name('user.')->group(function () {
        Route::get('users', 'ConsoleController@User')->name('main');
        Route::post('users', 'ConsoleController@UserNext')->name('next');
        Route::post('users/search', 'ConsoleController@UserSearch')->name('search');
    });

    Route::name('passed.')->group(function () {
        Route::get('passed', 'ConsoleController@Passed')->name('main');
        Route::post('passed', 'ConsoleController@PassedNext')->name('next');
        Route::post('passed/search', 'ConsoleController@PassedSearch')->name('search');
        Route::get('passed/download/{id}', 'ConsoleController@PassedDownload')->name('download');
    });

    Route::name('conference.')->group(function () {
        Route::get('conference', 'ConsoleController@Conference')->name('main');
    });

    Route::name('develop.')->middleware(['permission:develop'])->group(function () {
        Route::get('develop', 'ConsoleController@Develop')->name('main');
    });
});

/**
 * Редактор
 */
Route::prefix('editor')->middleware(['auth', 'permission:admin|console'])->group(function () {
    /** Редактирование статьи */
    Route::get('/article/{id}', 'EditorController@Article');

    /** Редактирование участника */
    Route::get('/member/{id}', 'EditorController@Member');
});

Route::name('editor.')->prefix('console')->middleware(['auth', 'permission:admin|console|editor'])->group(function () {
    Route::name('spec.')->prefix('speciality')->group(function () {
        Route::get('new', 'EditorController@SpecialityNew')->name('new');
        Route::post('create', 'EditorController@SpecialityCreate')->name('create');
        Route::get('{id}', 'EditorController@SpecialityEdit')->name('edit');
        Route::post('update/{id}', 'EditorController@SpecialityUpdate')->name('update');
        Route::post('delete/{id}', 'EditorController@SpecialityDelete')->name('delete');
        Route::post('pairs/{id}', 'EditorController@SpecialityPairs')->name('pairs');
    });

    Route::name('branch.')->prefix('branch')->group(function () {
        Route::get('new', 'EditorController@BranchNew')->name('new');
        Route::post('create', 'EditorController@BranchCreate')->name('create');
        Route::get('{id}', 'EditorController@BranchEdit')->name('edit');
        Route::post('update/{id}', 'EditorController@BranchUpdate')->name('update');
        Route::post('delete/{id}', 'EditorController@BranchDelete')->name('delete');
        Route::post('pairs/{id}', 'EditorController@BranchPairs')->name('pairs');
        Route::post('userupdate/{id}', 'EditorController@BranchUserUpdate')->name('userupdate');
        Route::post('usersearch/', 'EditorController@BranchUserSearch')->name('usersearch');
        Route::post('userpairs/{id}', 'EditorController@BranchUserPairs')->name('userpairs');
    });

    Route::name('subgroup.')->prefix('subgroup')->group(function () {
        Route::get('new', 'EditorController@SubgroupNew')->name('new');
        Route::post('create', 'EditorController@SubgroupCreate')->name('create');
        Route::get('{id}', 'EditorController@SubgroupEdit')->name('edit');
        Route::post('update/{id}', 'EditorController@SubgroupUpdate')->name('update');
        Route::post('delete/{id}', 'EditorController@SubgroupDelete')->name('delete');
        Route::post('pairs/{id}', 'EditorController@SubgroupPairs')->name('pairs');
        Route::post('userupdate/{id}', 'EditorController@SubgroupUserUpdate')->name('userupdate');
        Route::post('usersearch/', 'EditorController@SubgroupUserSearch')->name('usersearch');
        Route::post('userpairs/{id}', 'EditorController@SubgroupUserPairs')->name('userpairs');
        Route::post('testupdate/{id}', 'EditorController@SubgroupTestUpdate')->name('testupdate');
        Route::post('testsearch/', 'EditorController@SubgroupTestSearch')->name('testsearch');
        Route::post('testpairs/{id}', 'EditorController@SubgroupTestPairs')->name('testpairs');
    });

    Route::name('disc.')->prefix('discipline')->group(function () {
        Route::get('new', 'EditorController@DisciplineNew')->name('new');
        Route::post('create', 'EditorController@DisciplineCreate')->name('create');
        Route::get('{id}', 'EditorController@DisciplineEdit')->name('edit');
        Route::post('update/{id}', 'EditorController@DisciplineUpdate')->name('update');
        Route::post('delete/{id}', 'EditorController@DisciplineDelete')->name('delete');
        Route::post('pairs/{id}', 'EditorController@DisciplinePairs')->name('pairs');
        Route::post('branchupdate/{id}', 'EditorController@DisciplineBranchUpdate')->name('branchupdate');
        Route::post('branchsearch/', 'EditorController@DisciplineBranchSearch')->name('branchsearch');
        Route::post('branchpairs/{id}', 'EditorController@DisciplineBranchPairs')->name('branchpairs');
        Route::post('competences/{id}', 'EditorController@DisciplineCompetenceUpdate')->name('compupdate');
    });

    Route::name('competence.')->prefix('competence')->group(function () {
        Route::post('create', 'EditorController@CompetenceCreate')->name('create');
        Route::post('{id?}', 'EditorController@CompetenceEdit')->name('edit');
        Route::post('update/{id?}', 'EditorController@CompetenceUpdate')->name('update');
        Route::post('delete/{id?}', 'EditorController@CompetenceDelete')->name('delete');
    });

    Route::name('test.')->prefix('test')->group(function () {
        Route::get('new', 'EditorController@TestNew')->name('new');
        Route::post('create', 'EditorController@TestCreate')->name('create');
        Route::get('{id}', 'EditorController@TestEdit')->name('edit');
        Route::post('update/{id}', 'EditorController@TestUpdate')->name('update');
        Route::post('access/{id}', 'EditorController@TestAccess')->name('access');
        Route::post('delete/{id}', 'EditorController@TestDelete')->name('delete');
        Route::post('pairs/{id}', 'EditorController@TestPairs')->name('pairs');
        Route::post('questsupdate/{id}', 'EditorController@TestQuestUpdate')->name('questupdate');
        Route::post('questssearch/', 'EditorController@TestQuestSearch')->name('questsearch');
        Route::post('questspairs/{id}', 'EditorController@TestQuestPairs')->name('questpairs');
    });

    Route::name('quest.')->prefix('quest')->group(function () {
        Route::get('new', 'EditorController@QuestNew')->name('new');
        Route::post('create', 'EditorController@QuestCreate')->name('create');
        Route::get('{id}', 'EditorController@QuestEdit')->name('edit');
        Route::post('update/{id}', 'EditorController@QuestUpdate')->name('update');
        Route::post('delete/{id}', 'EditorController@QuestDelete')->name('delete');
        Route::post('pairs/{id}', 'EditorController@QuestPairs')->name('pairs');
        Route::post('competences/{id}', 'EditorController@QuestCompetenceUpdate')->name('compupdate');
        Route::post('admatrix/{id}', 'EditorController@QuestMatrixUpdate')->name('matrixupdate');
    });

    Route::name('user.')->prefix('user')->group(function () {
        Route::get('{id}', 'EditorController@UserEdit')->name('edit');
        Route::post('delete/{id}', 'EditorController@UserDelete')->name('delete');
        Route::post('permission/{id}', 'EditorController@UserPermission')->name('permission');
        Route::post('password/{id}', 'EditorController@UserResetPassword')->name('reset');
    });

    Route::name('passed.')->prefix('passed')->group(function () {
        Route::get('{id}', 'EditorController@PassedEdit')->name('edit');
        Route::post('delete/{id}', 'EditorController@PassedDelete')->name('delete');
    });
});