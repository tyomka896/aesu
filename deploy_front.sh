#!/bin/bash

set -e

PRIVATE_KEY=$(realpath ~/.ssh/aesfu-user)
USER="user"
SERVER_IP="89.108.103.91"
BUILD_FOLDER=build
ARCHIVE_NAME="$BUILD_FOLDER.tar.gz"
REMOTE_PATH="/var/www/aesfu.ru/html/public"

log() {
    echo "> $1"
}

if [ ! -f "$PRIVATE_KEY" ]; then
    log "Error: Private key '$PRIVATE_KEY' does not exit"
    exit 1
fi

log "Building the application. . ."
npm run build

if [ $? -ne 0 ]; then
    log "Error: Failed to build the application"
    exit 1
fi

echo
log "Archiving the '$BUILD_FOLDER' directory. . ."
cd public
tar -czf "$ARCHIVE_NAME" "$BUILD_FOLDER"/
mv "$ARCHIVE_NAME" ..
cd ..

if [ $? -ne 0 ]; then
    log "Error: Failed to archive the build"
    exit 1
fi

log "Copying the build to the server. . ."
scp -i "$PRIVATE_KEY" "$ARCHIVE_NAME" "$USER@$SERVER_IP:$REMOTE_PATH"

if [ $? -ne 0 ]; then
    log "Error: Failed to copy the build to the server"
    exit 1
fi

log "Unpacking the service build. . ."
ssh -i "$PRIVATE_KEY" "$USER@$SERVER_IP" "
    rm -rf $REMOTE_PATH/$BUILD_FOLDER &&
    tar -xzf $REMOTE_PATH/$ARCHIVE_NAME -C $REMOTE_PATH &&
    rm -f $REMOTE_PATH/$ARCHIVE_NAME
"

if [ $? -ne 0 ]; then
    log "Error: Filed to unpack the build archive"
    exit 1
fi

log "Removing local archive. . ."
rm -f "$ARCHIVE_NAME"

log "Deploy is successful"
