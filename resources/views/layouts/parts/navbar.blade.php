
<!-- [DELETE] -->

<div class="ui top inverted large borderless square menu">
    <div class="header item">
        <div class="ui grid">
            <div class="column" style="color: yellow;">{{ config('app.name') }}</div>
        </div>
    </div>

    <a class="item" href="{{ route('home') }}">
        <div class="ui grid">
            <div class="mobile only five wide column"><i class="home icon"></i></div>
            <div class="tablet computer only eleven wide column" style="margin:0 6px;">Домой</div>
        </div>
    </a>

    @auth
        <a class="{{ Request::is('test*') ? 'active ' : ''}}item" href="{{ route('test.main') }}">
            <div class="ui grid">
                <div class="mobile only five wide column"><i class="tasks icon"></i></div>
                <div class="tablet computer only eleven wide column" style="margin:0 6px;">Тесты</div>
            </div>
        </a>
    @endauth

    <a class="item" href="/methods">
        <div class="ui grid">
            <div class="mobile only five wide column"><i class="graduation cap icon"></i></div>
            <div class="tablet computer only eleven wide column" style="margin:0 6px;">Методики</div>
        </div>
    </a>

    @auth
        @canany(['admin', 'console'])
            <a class="{{ Request::is('console*') ? 'active ' : ''}}item" href="{{ route('console.main') }}">
                <div class="ui grid">
                    <div class="mobile only five wide column"><i class="laptop icon"></i></div>
                    <div class="tablet computer only eleven wide column" style="margin:0 6px;">Консоль</div>
                </div>
            </a>
        @endcanany
    @endauth

    <div class="right menu">
        @auth
            <div class="ui dropdown item" tabindex="0">
                <div class="ui grid">
                    <div class="mobile only five wide column"><i class="user outline icon"></i></div>
                    <div class="tablet computer only eleven wide column" style="margin:0 5px;">{{ Auth::user()->surname }}{{ mb_substr(Auth::user()->name, 0, 1) }}{{ mb_substr(Auth::user()->patronym, 0, 1) }}</div>
                </div>
                <div class="menu" tabindex="-1">
                    <a class="item" href="/profile">Профиль</a>

                    <div class="ui divider"></div>

                    <a class="ui item" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Выйти</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display:none;">@csrf</form>
                </div>
            </div>
        @else
            <a class="{{ Request::is('login') ? 'active ' : ''}}item" href="{{ route('login') }}">
                <div class="ui grid">
                    <div class="mobile only five wide column"><i class="sign-in icon"></i></div>
                    <div class="tablet computer only eleven wide column" style="margin:0 6px;">Войти</div>
                </div>
            </a>
            <a class="{{ Request::is('register') ? 'active ' : ''}}item" href="{{ route('register') }}">
                <div class="ui grid">
                    <div class="mobile only four wide column"><i class="edit icon"></i></div>
                    <div class="tablet computer only twelve wide column" style="margin:0 6px;">Регистрация</div>
                </div>
            </a>
        @endauth
    </div>
</div>
