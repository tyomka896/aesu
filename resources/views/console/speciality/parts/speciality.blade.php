<tr>
    <td class="two wide">{{ $spec->code }}</td>
    <td class="thirteen wide">{{ $spec->name }}</td>
    <td class="one wide">
    
		@canany(['admin', 'editor'])
			<a class="control" href="{{ route('editor.spec.edit', [$spec->id]) }}" title="Настройки специальности">
				<i class="cog icon"></i>
			</a>
		@endcanany
   
    </td>
</tr>
