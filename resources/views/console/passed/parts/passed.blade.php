<div class="ui card square">
    <div class="content">
        <div class="header">
            <a href="{{ route('editor.user.edit', [$basket->user->id]) }}" title="Просмотр пользователя" style="color:rgba(0,0,0,.85);">
                {{ $basket->user->name() }}
            </a>
        </div>

        <div class="meta">
            <span class="category">{{ $test->option() }}</span>-
            <span class="category">{{ $test->name ?? '-' }}</span>
        </div>

        <div class="description">

            @if ($basket->timer)
                <p>Пройдено за время: {{ date('H:i:s', $basket->timer) }}</p>
            @endif
        </div>

    </div>

    <div class="extra content">

        <div class="left floated">
            <a class="control" href="{{ route('editor.passed.edit', [$basket->id]) }}" title="Просмотр ответов">
                <i class="file icon"></i>
            </a>

            <a class="control" href="{{ route('console.passed.download', [$basket->id]) }}" title="Сохранить результаты в CSV-формате">
                <i class="save icon"></i>
            </a>

            @if ($test->option === 1 && $test->discipline !== null)
                <a class="control" href="/method/profile?y={{ $basket->passed_at->year }}&d={{ $test->discipline_id }}&b={{ $basket->id }}" target="_blank" title="Компетентностный профиль">
                    <i class="graduation cap icon"></i>
                </a>
            @endif

        </div>

        <div class="right floated author">Пройден: {{ $passed_at }}</div>

    </div>

</div>
