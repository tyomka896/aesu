@extends('layouts.main', ['title' => config('app.name').' - Пройденные'])

@section('content')

<!-- [DELETE] -->

<div class="ui stackable grid">
    @include('console.parts.menu')
    
    <div class="column" style="flex-grow:1;">
       
        <div class="ui one column grid">
            <div class="column" style="padding-bottom:0 !important;">
                
                {{-- ФОРМА ПОИСКА --}}
                <div class="ui form">
                    
                    {{-- СОРТИРОВКА --}}
                    <div class="inline left aligned fields">
                        <div class="field">
                            <div class="ui dropdown" tabindex="-1">
                                <i class="list ul icon"></i>
                                <span>Выбрать</span>
                                <div class="menu transition hidden" tabindex="-1">
                                    <a class="item{{ Request::input('sort') == null ? ' active selected' : '' }}" href="{{ route('console.passed.main') }}">Все</a>
                                    <a class="item{{ Request::input('sort') == 'bl' ? ' active selected' : '' }}" href="{{ route('console.passed.main', ['sort' => 'bl']) }}">Анкетирование</a>
                                    <a class="item{{ Request::input('sort') == 'qz' ? ' active selected' : '' }}" href="{{ route('console.passed.main', ['sort' => 'qz']) }}">Тестирование</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    {{-- ПОИСКОВА СТРОКА --}}
					<div class="field">      
						<div class="ui fluid icon input">
							<input id="search-text" type="text" placeholder="Ф.И.О., тест, дисциплина">
							<i id="search-icon" class="search link icon"></i>
						</div>
					</div>
                </div>
                
            </div>
        </div>
        
        <div class="ui one column grid">
            <div id="models" class="column" route-next="{{ route('console.passed.next', ['sort' => Request::input('sort')]) }}" route-search="{{ route('console.passed.search', ['sort' => Request::input('sort')]) }}">
            	<div id="models-list" page-count="1"></div>
            </div>
            <div class="row centered">
                <button id="models-next" class="ui button basic small" type="button">Загрузить еще</button>
            </div>
        </div>
    </div>
    
</div>

@endsection

@section('script')

@include('console.parts.search')

@endsection
