<tr>
    <td class="fifteen wide">{{ $disc->name }}</td>
    <td class="right aligned one wide">
    
		@canany(['admin', 'editor'])
			<a class="control disabled" href="{{ route('editor.disc.edit', [$disc->id]) }}" title="Настройки дисциплины">
				<i class="cog icon"></i>
			</a>
		@endcanany
   
    </td>
</tr>
