<tr>
    <td class="eight wide">{{ $user->name() }}</td>
    
	<td class="three wide">
		<a href="{{ $user->branch ? route('editor.branch.edit', [$user->branch->id]) : '#' }}" style="color:rgba(0,0,0,.85);">
			{{ $user->branch ? $user->branch->name : '-' }}
		</a>
	</td>
    
    <td class="four wide">{{ $user->userstatus->name }}</td>
    <td class="right aligned one wide">
       	
       	@canany(['admin', 'editor'])
			@if(!$user->can('admin') || Auth::user()->id == 1)
				<a class="control disabled" href="{{ route('editor.user.edit', [$user->id]) }}" title="Просмотр пользователя">
					<i class="cog icon"></i>
				</a>
			@endif
  		@endcanany
   	
    </td>
</tr>
