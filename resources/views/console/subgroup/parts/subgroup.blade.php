<tr>
    <td class="eight wide">{{ $group->name }}</td>
    <td class="seven wide">{{ $group->users->count() }}</td>
    <td class="right aligned one wide">
    
		@canany(['admin', 'editor'])
			<a class="control disabled" href="{{ route('editor.subgroup.edit', [$group->id]) }}" title="Редактировать спецгруппу">
				<i class="cog icon"></i>
			</a>
		@endcanany
   
    </td>
</tr>
