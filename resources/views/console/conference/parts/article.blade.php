<tr>
    <td class="fifteen wide">{{ $article->name }}</td>
    <td class="center aligned one wide">
        <a class="control" href="{{ '/editor/article/'.$article->id }}" title="Просмотр статьи">
            <i class="cog icon"></i>
        </a>
    </td>
</tr>
