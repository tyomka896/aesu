<tr>
    <td class="fifteen wide">{{ $quest->name }}</td>
    <td class="right aligned one wide">
    
		@canany(['admin', 'editor'])
			<a class="control disabled" href="{{ route('editor.quest.edit', [$quest->id]) }}" title="Редактировать вопрос">
				<i class="cog icon"></i>
			</a>
		@endcanany
   
    </td>
</tr>
