@extends('layouts.develop')

@section('script')

<script type="text/javascript">
    window._user=@json($user,JSON_UNESCAPED_UNICODE);
    window._hasPermissions=@json($hasPermissions,JSON_UNESCAPED_UNICODE);
    window._permissions=@json($permissions,JSON_UNESCAPED_UNICODE);
</script>

@vite('resources/src/pages/Develop/Permission/Permission.jsx')

@endsection
