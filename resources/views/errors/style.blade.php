<style type="text/css">
    body {
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        height: 100vh;
        margin: 0;
        padding: 0 16px;
        background: #ddd;
        font-family: Lato, "Helvetica Neue", Arial, Helvetica, sans-serif
    }

    .wrapper {
        width: 100%;
        max-width: 350px;
        margin: 16px auto
    }

    .box {
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        max-width: 350px;
        padding: 48px 16px;
        -webkit-align-items: center;
        -ms-flex-align: center;
        align-items: center;
        background: #fafafa;
        box-shadow: 0 0 8px 1px #ccc;
        border-radius: 8px
    }

    .box__header {
        font-size: 30px;
        text-align: center;
        margin: 0
    }

    .box__message {
        font-size: 12px;
        text-align: center;
        text-align: center;
        margin: 4px auto 12px;
        width: 90%
    }

    .box__message p {
        margin: 0;
        line-height: 16px
    }

    .error-list {
        padding: 0;
        margin: 0;
    }

    .error-list__item {
        padding: 8px 4px;
        text-align: left;
    }

    .box__button {
        font-size: .9em;
        text-decoration: none;
        text-transform: uppercase;
        color: rgba(0, 0, 0, .6);
        background: 0 0;
        border: none;
        border-radius: 5px;
        box-shadow: inset 0 0 0 1px rgba(34, 36, 38, .15);
        padding: .7em 1.2em;
        -webkit-user-select: none;
        -ms-user-select: none;
        user-select: none;
        transition: color .1s ease, background .1s ease, box-shadow .1s ease
    }

    .box__button:hover {
        color: rgba(0, 0, 0, .75);
        background: #fff;
        box-shadow: inset 0 0 0 1px rgba(34, 36, 38, .3)
    }

    @media (min-width:768px) {
        .wrapper {
            max-width: 600px
        }

        .box {
            max-width: 600px;
            padding: 96px 16px
        }

        .box__header {
            font-size: 42px
        }

        .box__message {
            font-size: 16px;
            margin: 8px auto 24px
        }

        .box__message p {
            line-height: 32px
        }

        .box__button {
            font-size: 1.1em
        }
    }
</style>