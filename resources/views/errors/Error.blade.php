<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $error ?? 'Error' }}</title>

    @include('errors.style')
</head>

<body>
    <div class="wrapper">
        <div class="box">
            @if ($errors->any())
                <h1 class="box__header">{{ $message ?? 'Ошибки' }}</h1>
                <div class="box__message">
                    <ul class="error-list">
                        @foreach ($errors->all() as $error)
                            <li class="error-list__item">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @else
                <h1 class="box__header">{{ $error ?? 'Ошибка' }}</h1>
                <div class="box__message">
                    <p>{{ $message ?? 'Ошибка' }}</p>
                </div>
            @endif
            <a class="box__button" href="/">Домой</a>
        </div>
</body>

</html>