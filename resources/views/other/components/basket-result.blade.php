<p><b><i>Время: </i></b>{{ $basket->timer ? date('H:i:s', $basket->timer) : '-' }}</p>
<p><b><i>Баллов: </i></b>{{ $basket->score ? $basket->score : ' - ' }}/100</p>

@auth
    @canany(['admin', 'console'])
        <p><b><i>Группа: </i></b>@if ($basket->user) {{ $basket->user->branch ? $basket->user->branch->name : '-' }} @else
        {{ '-' }} @endif
        </p>
        <p><b><i>Пользователь: </i></b>{{ $basket->user ? $basket->user->name() : '[пользователь не задан]' }}</p>
    @endcanany
@endauth

<p><b><i>Дисципилна: </i></b>{{ $basket->test->discipline ? $basket->test->discipline->name : '-' }}</p>
<p><b><i>Название теста: </i></b>{{ $basket->test ? $basket->test->name : '-' }}</p>

@isset($passedAt) <p><b><i>Пройдено: </i></b>{{ $passedAt }}</p> @endisset

<div class="ui divider"></div>

@foreach ($quests as $quest)

    <div><b>{{ $quest['pos'] }}. {{ $quest['name'] }}</b></div>

    @foreach($quest['answers'] as $answer)
        <div class="ui small feed">{!! $answer !!}</div>
    @endforeach

@endforeach