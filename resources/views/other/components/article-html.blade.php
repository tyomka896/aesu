<div>
    @foreach ($data['blocks'] as $block)
        @switch($block['type'])
            @case('header')
                <h{{ $block['data']['level'] }}>{{ $block['data']['text'] }}</h{{ $block['data']['level'] }}>
                @break
            @case('paragraph')
                <p>{{ $block['data']['text'] }}</p>
                @break
            @case('list')
                <ul>
                    @foreach ($block['data']['items'] as $item)
                        <li>{{ $item }}</li>
                    @endforeach
                </ul>
                @break
            @case('table')
                <table>
                    @foreach ($block['data']['content'] as $row)
                        <tr>
                            @foreach ($row as $cell)
                                <td>{!! $cell !!}</td>
                            @endforeach
                        </tr>
                    @endforeach
                </table>
                @break
            @case('delimiter')
                <hr>
                @break
            @case('image')
                <img src="{{ $block['data']['url'] }}" alt="{{ $block['data']['caption'] }}"
                     @if ($block['data']['withBorder'])style="border: 1px solid #ccc;"@endif
                     @if ($block['data']['stretched'])style="width: 100%;"@endif>
                @break
            @default
                {{ $block['type'] }} block is not supported.
        @endswitch
    @endforeach
</div>
