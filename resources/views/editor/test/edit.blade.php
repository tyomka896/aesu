@extends('layouts.main', ['title' => config('app.name').' - Анкета или Тест - '.$test->name])

@section('style')

<link rel="stylesheet" type="text/css" href="{{ asset('css/editor/simditor.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/datapick/daterangepicker.css') }}" />

@endsection

@section('content')

<div class="ui centered grid">

    <div class="sixteen wide mobile tablet twelve wide computer centered column">

        <div class="ui tiny breadcrumb">
            <a class="section bread" href="{{ route('console.main') }}">Консоль</a>
            <i class="right angle icon divider"></i>
            <a class="section bread" href="{{ route('console.test.main') }}">Анкеты и Тесты</a>
            <i class="right angle icon divider"></i>
            <div class="section active">#{{ $test->id }}</div>
        </div>

        <div class="ui divider"></div>

        {{-- ОСНОВНЫЕ ПАРАМЕТРЫ --}}
        <div class="ui fluid accordion">
            <div class="title active">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Основные параметры</b>
            </div>


            <div class="content active">

                <div class="main-holder" route-pairs="{{ route('editor.test.pairs', [$test->id]) }}" route-update="{{ route('editor.test.update', ['id' => $test->id]) }}">

                    @include('editor.test.parts.info')

                </div>

            </div>
        </div>

        <div class="ui divider"></div>

        {{-- НАСТРОЙКИ ДАТЫ СДАЧИ ТЕСТА --}}
        <div class="ui fluid accordion">
            <div class="title">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Срок сдачи теста</b>
            </div>

            <div class="content">
                <div class="ui segment" style="background-color:#fbfbfb;">
                    <form id="fm-access" class="ui form" method="post">
                        @csrf

                        <div class="eight wide field">
                            <div class="field">
                                <label>
                                    <u>
                                        Срок сдачи до <span id="access-till">{{ $accessTill }}</span>
                                    </u>
                                </label>
                            </div>
                            <div class="field">
                                <label>Время окончания сдачи теста</label>
                                <input id="datapick" name="access" type="text" readonly>
                            </div>
                        </div>

                        <div class="ui two column grid">
                            <div class="left aligned column">
                                <div class="column">
                                    <button id="btn-access-save" class="ui primary button" type="submit">Сохранить</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>

            </div>
        </div>

        <div class="ui divider"></div>

        {{-- НАСТРОЙКИ ВОПРОСОВ К ТЕСТУ --}}
        <div class="ui fluid accordion">
            <div class="title">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Вопросы</b>
            </div>

            <div class="content">
                <br/>
                <div class="link-holder" route-pairs="{{ route('editor.test.questpairs', [$test->id]) }}" route-search="{{ route('editor.test.questsearch') }}" route-update="{{ route('editor.test.questupdate', [$test->id]) }}" model="quests">

                    @include('editor.test.parts.tablequest')

                </div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('script')

@include('editor.parts.attach')
@include('editor.test.parts.script')

<script type="text/javascript" src="{{ asset('js/editor/module.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/editor/hotkeys.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/editor/simditor.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datapick/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datapick/daterangepicker.js') }}"></script>
<script type="text/javascript">
    {{-- КНОПКА УДАЛИТЬ [button_delete] --}}
    const bd = function(){
        let val = confirm("Вы уверены, что хотите удалить тест?");

        if (val) $(".main-holder form").attr("action", "{{ route('editor.test.delete', [$test->id]) }}").off("submit").submit();
    };
    {{-- ИНИЦИАЛИЗАЦИЯ РЕДАКТОРА ОПИСАНИЯ [initialize_editor] --}}
    const ie = function(){
        let editor = new Simditor({
            toolbar: [
                "bold", "italic", "underline", "strikethrough", "color", "|",
                "ol", "ul", "|",
                "indent", "outdent", "|",
                 "link", "hr"
            ],
            textarea: $("#portrait"),
            placeholder: "Вступительное описание теста"
        });
    };
    {{-- УДАЧНОЕ ОБНОВЛЕНИЕ МОДЕЛИ [on_success] --}}
    const osc = function(holder, data, edit){
        holder.empty().append(data.view);
        holder.find("#btn-pairs").click(function(){ gmf(this) });

        if (edit){
            ie(); // [initialize_editor]
            holder.find("form").submit(bms);
            holder.find("#btn-delete").click(bd);
            holder.find("#discipline").dropdown("set selected", `${data.test.disc}`);
            holder.find("#option").dropdown("set selected", `${data.test.option}`);
        }
    };
    {{-- ЗАПРОС НА ОБНОВЛЕНИЕ ДОСТУПА ТЕСТА [update_access] --}}
    const ua = function(ev){
        let btn = $("#btn-access-save");
        ev.preventDefault();

        btn.addClass("loading");

        const accessTill = moment($("#datapick").val(), "DD.MM.YYYY HH:mm");

        $.ajax({
            method: "POST",
            url: "{{ route('editor.test.access', ['id' => $test->id]) }}",
            data: { accessTill: accessTill.valueOf() },
            headers: { "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content') },
        })
        .done(data => {
            btn.addClass("positive icon").html('<i class="check icon"></i>');
            $('#access-till').html(accessTill.format("DD.MM.YYYY HH:mm"))
        })
        .fail((jqXHR, textStatus, errorThrown) => {
            console.log('$ jqXHR', jqXHR)
            console.log('$ textStatus', textStatus)
            console.log('$ errorThrown', errorThrown)
        })
        .always(() => {
            btn.removeClass("loading");
        })
    };

    {{-- ИНИЦИАЛИЗАЦИЯ ПОЛЯ ДЛЯ ВЫБОРА ДАТЫ --}}
    $("#datapick").daterangepicker({
        locale: {
            format: "DD.MM.YYYY HH:mm",
            applyLabel: 'Применить',
            cancelLabel: 'Отмена',
            daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт','Сб'],
            firstDay: 1
        },
        drops: "auto",
        showDropdowns: true,
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        startDate: moment()
    });

    $("#fm-access").submit(ua);
    $(".main-holder #btn-pairs").click(function(){ gmf(this) });
    $(".link-holder form").find("#btn-pairs").click(gaf);
    $(".link-holder form").find("#btn-save").click(bas);
</script>

@endsection