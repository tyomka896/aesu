<form class="ui form" method="post">
    <div class="field">
        
        <table class="ui very basic unstackable table">
            <thead>
                <tr>
                    <th class="one wide center aligned" style="padding-left:5px;">#</th>
                    <th class="fourteen wide">Вопрос</th>
                    <th class="center aligned one wide"></th>
                </tr>
            </thead>
            
            <tbody>
                
                @if ($quests->count() == 0 && !$edit)
                	<tr>
                		<td class="center aligned">-</td>
                		<td>-</td>
                		<td class="center aligned"></td>
                	</tr>
                @endif
                
                @foreach ($quests as $quest)
                    <tr>
                        <td id="count" class="center aligned">{{ $quest->pivot->position }}</td>

                        <td>
                            {{ $quest->name }}
                            
                            @if ($edit)
                                <input name="quests[]" type="hidden" value="{{ $quest->id }}">
                            @endif
                            
                        </td>

                        <td class="center aligned two wide">

                            @if ($edit)
                                <button id="btn-remove" class="ui icon basic negative mini button" type="button">
                                    <i class="trash icon"></i>
                                </button>
                            @else
                                <a class="control disabled" href="{{ route('editor.quest.edit', [$quest->id]) }}" title="Редактировать вопрос">
                                    <i class="cog icon"></i>
                                </a>
                            @endif

                        </td>
                    </tr>
                @endforeach
                
            </tbody>
        </table>
        
        @if ($edit)
            <div class="ui two column grid">
                <div class="left aligned column">
                    <div class="column">
                        <button id="btn-save" class="ui primary button" type="submit">Сохранить</button>
                        <button id="btn-add" class="ui basic positive button" type="button">
                        	<i class="plus icon"></i>Добавить
                        </button>
                    </div>
                </div>
                <div class="right aligned column">
                    <div class="column">
                        <button id="btn-pairs" class="ui basic button" type="button">Отмена</button>
                    </div>
                </div>
            </div>
        @else
            <div class="ui one column grid">
                <div class="right aligned column">
                    <div class="column">
                        <button id="btn-pairs" class="ui positive button" type="button">Изменить</button> 
                    </div>
                </div>
            </div>
        @endif
        
    </div>
</form>
