@extends('layouts.main', ['title' => config('app.name').' - Факультативная группа - '.$branch->name])

@section('content')

<div class="ui centered grid">

    <div class="sixteen wide mobile tablet twelve wide computer centered column">

        <div class="ui tiny breadcrumb">
            <a class="section bread" href="{{ route('console.main') }}">Консоль</a>
            <i class="right angle icon divider"></i>
            <a class="section bread" href="{{ route('console.branch.main') }}">Факультативные группы</a>
            <i class="right angle icon divider"></i>
            <div class="section active">#{{ $branch->id }}</div>
        </div>

        <div class="ui divider"></div>

        {{-- ОСНОВНЫЕ ПАРАМЕТРЫ --}}
        <div class="ui fluid accordion">
            <div class="title active">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Основные параметры</b>
            </div>


            <div class="content active">

                <div class="main-holder" route-pairs="{{ route('editor.branch.pairs', [$branch->id]) }}" route-update="{{ route('editor.branch.update', ['id' => $branch->id]) }}">

                    @include('editor.branch.parts.info')

                </div>

            </div>
        </div>

        <div class="ui divider"></div>

        {{-- ПРИВЯЗКА СТУДЕНТОВ --}}
        <div class="ui fluid accordion">
            <div class="title">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Студенты</b>
            </div>

            <div class="content">
                <br/>
                <div class="link-holder" route-pairs="{{ route('editor.branch.userpairs', [$branch->id]) }}" route-search="{{ route('editor.branch.usersearch') }}" route-update="{{ route('editor.branch.userupdate', [$branch->id]) }}" model="users">

                    @include('editor.branch.parts.tableuser')

                </div>
            </div>
        </div>

        <div class="ui divider"></div>

        {{-- ДИСЦИПЛИНЫ --}}
        <div class="ui fluid accordion">
            <div class="title">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Дисциплины</b>
            </div>

            <div class="content">

                <br/>
                <div>

                    @include('editor.branch.parts.tablediscipline')

                </div>

            </div>
        </div>

    </div>
</div>

@endsection

@section('script')

@include('editor.parts.attach')
@include('editor.branch.parts.script')

<script type="text/javascript">
    {{-- КНОПКА УДАЛИТЬ [button_delete] --}}
    const bd = function(){
        let val = confirm("Вы уверены, что хотите удалить группу?");

        if (val) $(".main-holder form").attr("action", "{{ route('editor.branch.delete', [$branch->id]) }}").off("submit").submit();
    };
    {{-- УДАЧНОЕ ОБНОВЛЕНИЕ МОДЕЛИ [on_success] --}}
	const osc = function(holder, data, edit){
		holder.empty().append(data.view);
		holder.find("#btn-pairs").click(function(){ gmf(this) });

		if (edit){
			holder.find("form").submit(bms);
			holder.find("#btn-delete").click(bd);
			holder.find("#speciality").dropdown("set selected", `${data.branch.spec}`);
		}
	};

    $(".main-holder #btn-pairs").click(function(){ gmf(this) });
    $(".link-holder form").find("#btn-pairs").click(gaf);
</script>


@endsection
