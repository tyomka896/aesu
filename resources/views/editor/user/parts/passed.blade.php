<div class="ui segment basic left aligned">
    <div class="ui one column grid">

        <div class="column">
            <table class="ui very basic unstackable table">
                <thead>
                    <tr>
                        <th class="one wide center aligned">#</th>
                        <th>Наименование теста</th>
                        <th>Дисциплина</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    @if ($passed->count() == 0)
                        <tr>
                            <td class="one wide center aligned">-</td>
                            <td class="five wide">-</td>
                            <td class="eight wide">-</td>
                            <td class="two wide right aligned"></td>
                        </tr>
                    @endif

                    @foreach ($passed as $key => $pass)
                        <tr>
                            <td class="one wide">{{ $key + 1 }}</td>
                            <td class="six wide">{{ $pass->test ? $pass->test->name : '-' }}</td>
                            <td class="eight wide">{{ $pass->discipline->name ?? '-' }}</td>
                            <td class="two wide right aligned">
                                <a class="control" href="{{ route('editor.passed.edit', [$pass->id]) }}"
                                    title="Просмотр ответов">
                                    <i class="file icon"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>

        </div>

    </div>
</div>