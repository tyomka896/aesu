<div class="ui segment basic left aligned">
    <table class="ui very basic unstackable table">
        <tbody>

            <tr>
                <td class="four wide"><b>ФИО</b></td>
                <td class="ten wide">{{ $user->name() }}</td>
            </tr>

            <tr>
                <td class="four wide"><b>Статус</b></td>
                <td class="ten wide">{{ $user->userstatus->name ?? '-' }}</td>
            </tr>

            <tr>
                <td class="four wide"><b>Создан</b></td>
                <td class="ten wide">{{ $created_at }}</td>
            </tr>

            <tr>
                <td class="four wide"><b>Последний визит</b></td>
                <td class="ten wide">{{ $last_visit }}</td>
            </tr>

            <tr>
                <td class="four wide"><b>Группа</b></td>
                <td class="ten wide">{{ $user->branch->name ?? '-' }}</td>
            </tr>

            <tr>
                <td class="four wide"><b>Специальность</b></td>
                <td class="ten wide">{{ $user->branch ? $user->branch->speciality->name() : '-' }}</td>
            </tr>
        </tbody>
    </table>

    <div class="ui divider" style="visibility:hidden;"></div>

    <table class="ui very basic unstackable table">
        <tbody>
            <tr>
                <td class="four wide"><b>Логин</b></td>
                <td class="ten wide">{{ $user->login }}</td>
            </tr>

            <tr>
                <td class="four wide"><b>Email-адрес</b></td>
                <td class="ten wide">{{ $user->email ?? '-' }}</td>
            </tr>

            @can('admin')
                <tr>
                    <td class="four wide"><b>Пароль</b></td>
                    <td class="ten wide">
                        <button id="btn-reset" class="ui mini basic button" type="button">Сбросить</button>
                        <small>(сброс на 123456)</small>
                    </td>
                </tr>
            @endcan

        </tbody>
    </table>

    @can('admin')
        <div class="ui one column grid">
            <div class="column right aligned">
                <a id="delete-item" class="control" title="Удалить">
                    <i class="trash link icon"></i>
                </a>
                <form id="delete-form" class="ui form" method="post">@csrf</form>
            </div>
        </div>
    @endcan

</div>
