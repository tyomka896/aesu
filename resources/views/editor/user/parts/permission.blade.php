<div class="ui segment" style="background-color:#fbfbfb;">
    <form id="fm-permission" class="ui form" method="post">
        @csrf

        <div class="field">
            <label>Права пользователя</label>
            <div id="permissions" class="ui fluid dropdown selection multiple" tabindex="0">
                <select name="permissions[]" multiple="" aria-label="user's permission">

                    @foreach ($perms as $perm)
                        <option value="{{ $perm->name }}">{{ $perm->definition }}</option>
                    @endforeach

                </select>

                <i class="dropdown icon"></i>
                <div class="default text">Возможности</div>

                <div class="menu" tabindex="-1">

                    @foreach ($perms as $perm)
                        <div class="item" data-value="{{ $perm->name }}">{{ $perm->definition }}</div>
                    @endforeach

                </div>
            </div>
        </div>

        <button id="btn-permission" class="ui primary button" type="submit">Сохранить</button>
    </form>
</div>