<form id="fm-branches" class="ui form" method="post">
    <div class="field">
        
        <table id="tb-branches" class="ui very basic unstackable table">
            <thead>
                <tr>
                    <th class="one wide center aligned" style="padding-left:5px;">#</th>
                    <th class="fourteen wide">Наименование</th>
                    <th class="center aligned one wide"></th>
                </tr>
            </thead>
            
            <tbody>
                
                @if ($tests->count() == 0)
                	<tr>
                		<td class="center aligned">-</td>
                		<td>-</td>
                		<td class="center aligned"></td>
                	</tr>
                @endif
                
                @for ($q = 0; $q < $tests->count(); $q++)
                    <tr>
                        <td id="count" class="center aligned">{{ $q + 1 }}</td>

                        <td>
                            {{ $tests[$q]->name }}                            
                        </td>

                        <td class="center aligned two wide">
							<a class="control" href="{{ route('editor.test.edit', [$tests[$q]->id]) }}" title="Настройки теста">
								<i class="cog icon"></i>
							</a>
                        </td>
                    </tr>
                @endfor
                
            </tbody>
        </table>
        
    </div>
</form>
