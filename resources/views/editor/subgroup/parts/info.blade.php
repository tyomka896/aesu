<div class="ui segment basic left aligned">         
    <table class="ui very basic unstackable table">
        <tbody>

            <tr>
                <td><b>Наименование</b></td>
                <td class="right aligned">
                    <button id="btn-pairs" class="ui icon basic small button">
                        <i class="pencil icon"></i>
                    </button>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    {{ $group->name }}
                </td>
            </tr>

            <tr><td colspan="2"><b>Описание</b></td></tr>
            <tr><td colspan="2">{!! $group->portrait ? $group->portrait : '-' !!}</td></tr>

            <tr><td colspan="2"><b>Владелец</b></td></tr>
            <tr><td colspan="2">{{ $group->userowner->name() }}</td></tr>

        </tbody>
    </table>
</div>
