<div class="ui segment basic left aligned">
    
    @php
        $toEdit = Request::route()->getName() == 'editor.quest.pairs';
    @endphp
    
    {{-- ФОРМА ТОЛЬКО СОЗДАЕТ ВОПРОС, ОБНОВЛЕНИЕ ПРОИСХОДИТ АСИНХРОННО --}}
    <form class="ui form" action="{{ $toEdit ? '' : route('editor.quest.create') }}" method="POST">
        @csrf
        
        {{-- НАИМЕНОВЕНИЕ --}}
        <div class="field{{ $errors->has('name') ? ' error' : ''}}">
            <label>Формулировка вопроса</label>
            <input name="name" type="text" value="{{ $quest->name ?? old('name') }}" placeholder="Наименование" autocomplete="off">
            @if ($errors->has('name'))
                <div class="ui basic red pointing prompt label transition">
                    {{ $errors->first('name') }}
                </div>
            @endif
        </div>
        
        {{-- ТИП ВОПРОСА --}}
		<div class="field{{ $errors->has('type') ? ' error' : ''}}">
			<label>Тип вопроса</label>
			<div id="type" class="ui fluid dropdown selection" previous-value="-1" tabindex="0">
				<select name="type">
					<option value="">Тип вопроса</option>

					@foreach ($types as $type)
						<option value="{{ $type->type }}">{{ $type->name }}</option>
					@endforeach

				</select>
				<i class="dropdown icon"></i>
				<div class="default text">Тип вопроса</div>
				<div class="menu transition hidden" tabindex="0">

					@foreach ($types as $type)
						<div class="item" data-value="{{ $type->type }}">{{ $type->name }}</div>
					@endforeach

				</div>
			</div>
			@if ($errors->has('type'))
				<div class="ui basic red pointing prompt label transition">
					{{ $errors->first('type') }}
				</div>
			@endif
		</div>
        
        {{-- СПИСОК ОТВЕТОВ --}}
        <div id="answers" class="field" style="display:{{ $toEdit ? 'block' : 'none' }};">
            <label>Список ответов</label>
            <table id="tb-answers" class="ui very basic unstackable table">
                <thead>
                    <tr>
                        <th class="center aligned one wide">Поз.</th>
                        <th class="fourteen wide">Название</th>
                        <th class="right aligned one wide"></th>
                    </tr>
                </thead>
                <tbody>
                 	{{-- ОТРАБОТКА ПОЛЕЙ ОТВЕТОВ --}}
                    {{--<tr>
						<td id="count" class="center aligned one wide">1</td>

						<td class="fourteen wide">
							<div class="ui small form">

								<div class="field">
									<input name="answers[]" type="text" placeholder="Формулировка ответа" value="{{ $answer->name ?? '' }}" autocomplete="off">
								</div>

									<div class="fields" style="margin-bottom:0;">
										<div class="five wide field">
											<div class="ui labeled input">
												<div class="ui label">От</div>
												<input name="min[]" type="text" placeholder="Значение от" value="{{ $answer->minvalue ?? '' }}" autocomplete="off">
											</div>
										</div>
										<div class="five wide field">
											<div class="ui labeled input">
												<div class="ui label">До</div>
												<input name="max[]" type="text" placeholder="Значение до" value="{{ $answer->maxvalue ?? '' }}" autocomplete="off">
											</div>
										</div>
									</div>

							</div>
						</td>

						<td class="right aligned one wide">
							<button id="btn-remove" class="ui icon basic negative mini button" type="button">
								<i class="trash icon"></i>
							</button>
						</td>  
					</tr>--}}
                   
                    @isset($answers)
                        
                        @foreach($answers as $answer)
                            
                            <tr>
                                <td id="count" class="center aligned one wide">{{ $answer->position }}</td>
                    
                                <td class="fourteen wide">
                                    <div class="ui small form">

                                        <div class="field">
                                            <input name="answers[]" type="text" placeholder="Наименование ответа" value="{{ $answer->name ?? '' }}" autocomplete="off">
                                        </div>
                                        
                                        @if(in_array($quest->questtype->type, [0, 1, 2]))
                                        
											<div class="fields">
                                                <div class="five wide field">
                                                    <div class="ui labeled input">
                                                       <div class="ui label">Кф.</div> 
                                                       <input name="values[]" type="text" placeholder="Коэффициент" value="{{ $answer->value ?? '' }}" autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="field">
                                                    <div class="ui checkbox">
														<input id="prime" name="primes[]" type="checkbox"{{ $answer->prime ? ' checked' : '' }} value="">
														<label>Отказаться от ответа</label>
                                                    </div>
                                                </div>
                                            </div>
                                       		
                                        @elseif ($quest->questtype->type == 4)

                                            <div class="fields">
                                                <div class="five wide field">
                                                    <div class="ui labeled input">
                                                        <div class="ui label">От</div>
                                                        <input name="min[]" type="text" placeholder="Значение от" value="{{ $answer->minvalue ?? '' }}" autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class="five wide field">
                                                    <div class="ui labeled input">
                                                        <div class="ui label">До</div>
                                                        <input name="max[]" type="text" placeholder="Значение до" value="{{ $answer->maxvalue ?? '' }}" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>

                                        @endif

                                    </div>
                                </td>
                                
                                <td class="right aligned one wide">
                                    <button id="btn-remove" class="ui icon basic negative mini button" type="button">
                                        <i class="trash icon"></i>
                                    </button>
                                </td>  
                            </tr>  
                            
                        @endforeach
                        
                    @endisset
                </tbody>
            </table>
        </div>
        
        @if (!$toEdit)
            <div class="field">
                <div class="ui checkbox">
                    <input id="next" name="next" type="checkbox" checked>
                    <label for="next">Создать следующий вопрос</label>
                </div>
            </div>
        @endif

        <div class="field">
            <div class="ui two column grid">

                <div class="left aligned column">
                    <div class="column">

                        <button id="btn-save" class="ui primary button" type="submit">{{ $toEdit ? 'Сохранить' : 'Создать' }}</button>
                        <button id="btn-add" class="ui basic positive button" type="button" style="display:{{ $toEdit ? '' : 'none' }};">
                            <i class="plus icon"></i>Добавить
                        </button>

                    </div>
                </div>

                <div class="right aligned column">
                    <div class="row">

                        @if ($toEdit)
                            <button id="btn-delete" class="ui negative button" type="button">Удалить</button>
                            <button id="btn-pairs" class="ui button" type="button">Отмена</button>
                        @else
                            <a class="ui button" href="{{ route('console.quest.main') }}">Отмена</a>
                        @endif

                    </div>
                </div>

            </div>
        </div>

    </form>
    
</div>
