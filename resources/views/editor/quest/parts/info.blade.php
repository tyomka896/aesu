<div class="ui segment basic left aligned">          
    <table class="ui very basic unstackable table">
        <tbody>

            <tr>
                <td><b>Формулировка вопроса</b></td>
                <td class="right aligned">
                    <button id="btn-pairs" class="ui icon basic small button">
                        <i class="pencil icon"></i>
                    </button>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    {{ $quest->name }}
                </td>
            </tr>

            <tr>
                <td class="eight wide" colspan="2"><b>Тип вопроса</b></td>
            </tr>
            <tr>
                <td colspan="2">{{ $quest->questtype->name }}</td>
            </tr>

        </tbody>
    </table>

    @if ($quest->questtype->type != 3)

        <div class="ui divider"></div>

        <table class="ui very basic unstackable table">
            <thead>
                <tr>
                    <th colspan="5">Список ответов</th>
                </tr>
            </thead>
            <tbody>

                <tr>
                    <td class="center aligned"><b>Поз.</b></td>
                    <td><b>Формулировка</b></td>
                    <td><b>Кф.</b></td>
                    <td><b>Мин.</b></td>
                    <td><b>Макс.</b></td>
                </tr>

                @foreach ($answers as $answer)
                    <tr>
                        <td class="center aligned one wide">{{ $answer->prime ? '+ ' : '' }}{{ $answer->position }}</td>
                        <td class="twelve wide">{{ $answer->name }}</td>
                        <td class="one wide">{{ $answer->value ?? '-' }}</td>
                        <td class="one wide">{{ $answer->minvalue ?? '-' }}</td>
                        <td class="one wide">{{ $answer->maxvalue ?? '-' }}</td>
                    </tr>
                @endforeach

            </tbody>
        </table>    

    @endif
</div>
