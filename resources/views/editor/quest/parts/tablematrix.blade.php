<form class="ui form" method="post">

    <div class="field" style="overflow:auto">

        <table class="ui unstackable definition celled table">
            <thead>
                <tr>
                    <th class="center aligned" style="width:50px;"></th>

                    @foreach ($hasCompetences as $key => $competence)
                        <th class="center aligned" data-content="{{ $competence->name }}"  data-position="top center" data-variation="very wide">{{ $competence->shortname }}</th>
                    @endforeach

                </tr>
            </thead>

            <tbody>

                @foreach ($answers as $key => $answer)
                    <tr>
                        <td class="one wide center aligned p-0 td-w-min" data-content="{{ $answer->name }}"  data-position="right center" data-variation="wide">
                            Д-{{ $key + 1 }}
                        </td>

                        @foreach ($hasCompetences as $_key => $competence)

                            @php
                                $matrixval = $answer->admatrixes()->where('competence_id', $competence->id)->first();
                            @endphp

                            <td class="center aligned p-0 td-w-min">
                                <div class="ui fluid transparent input">

                                    <input 
                                        type="text"
                                        class="i-cell"
                                        tabindex="{{ ($_key + 1).($key + 1) }}"
                                        name="admatrixes[{{ $answer->id }}][]"
                                        value="{{ $matrixval->value ?? '' }}"
                                        autocomplete="off" >
                                </div>
                            </td>
                        @endforeach

                    </tr>
                @endforeach

            </tbody>
        </table>

    </div>

    <div class="ui ont column grid">
        <div class="left aligned column">
            <div class="column">
                <button id="btn-save" class="ui primary button" type="submit">Сохранить</button>
            </div>
        </div>
    </div>

</form>
