<script type="text/javascript" src="{{ asset('js/table/jquery-ui.js') }}"></script>

<script type="text/javascript">
    {{-- ВАЛИДАЦИЯ ОСНОВНОЙ ФОРМЫ ЗАПОЛНЕНИЯ ВОПРОСА [validate_form]--}}
    const vf = function(ev){
        let rows = $("#tb-answers > tbody").children("tr");
        let count = rows.length;
        
        let hollow = rows.filter(function(){
            return $.trim($(this).find("#answer").val()) == "";
        }).length > 0;
        
        if ((count < 2 || hollow) && $("#type").dropdown("get value") != 3){
            ev.preventDefault();
            
            $('#message').children("p").html("Необходимо заполнить минимум два не пустых варианта ответа");
			$('#message').closest(".grid").slideDown(50);
        }
    };
    {{-- СБРОС НУМЕРАЦИИ ОТВТОВ [reset_counter] --}}
    const rc = function(){
        let val = $("#tb-answers > tbody").children("tr").each(function(ind){
            $(this).find("#count").html(ind + 1);
			$(this).find("#prime").attr("value", ind + 1);
        });
    };
    {{-- ПОЛУЧЕНИЕ НОВОЙ СТРОКИ ОТВЕТА [new_row] --}}
    const nr = function(count){
        let html = [];
        {{-- КОНСТРУКТОР ДЛЯ ВОПРОСОВ ТИПА: 0, 1, 2 --}}
        let answer = $("<div>",{
            class: "field",
			html: '<input id="answer" name="answers[]" type="text" placeholder="Формулировка ответа" autocomplete="off">'
        });
		{{-- ДОП НАСТРОЙКИ ДЛЯ ВОПРОСОВ ТИПА: 0, 1, 2 --}}
		let primary = $("<div>",{
            class: "fields",
            style: "margin-bottom:0;",
			html: [
				$("<div>", {
                    class: "five wide field",
                    html: $("<div>", {
                        class: "ui labeled input",
                        html: '<div class="ui label">Кф.</div> <input name="values[]" type="text" value="0" placeholder="Коэффициент" autocomplete="off">'
                    })
                }), $("<div>", {
                    class: "field",
                    html: $("<div>", {
                        class: "ui checkbox",
                        html: '<input id="prime" name="primes[]" type="checkbox" value="' + (count + 1) + '"><label>Отказаться от ответа</label>'
                    })
                })
			]
        });
        {{-- ДОП НАСТРОЙКИ ДЛЯ ВОПРОСА ТИПА: 4 --}}
        let range = $("<div>", {
            class: "fields",
            style: "margin-bottom:0;",
            html: [
                $("<div>", {
                    class: "five wide field",
                    html: $("<div>", {
                        class: "ui labeled input",
                        html: '<div class="ui label">От</div> <input name="min[]" type="text" value="0" placeholder="Значение от" autocomplete="off">'
                    })
                }), $("<div>", {
                    class: "five wide field",
                    html: $("<div>", {
                        class: "ui labeled input",
                        html: '<div class="ui label">До</div> <input name="max[]" type="text" value="0" placeholder="Значение до" autocomplete="off">'
                    })
                }),
            ]
        });
        
		let type = $("#type").dropdown("get value");
		
        html.push(answer);
		if (type >= 0 && type <= 2) html.push(primary);
        else if (type == 4) html.push(range);

        return $("<div>", {
            class: "ui small form",
            html: html
        });
    };
	
    {{-- КЛИК ПО КНОПКЕ ДОБАВИТЬ ОТВЕТ [add_row] --}}
    const ar = function(){        
        let count = $("#tb-answers > tbody").children("tr").length;

        let row = $('<tr>', {
            html: [
                $("<td>", {
                    id: "count",
                    class: "center aligned one wide",
                    html: count + 1
                }),
                $("<td>", {
                    class: "fourteen wide",
                    html: nr(count)
                }),
                $("<td>", {
                    class: "right aligned one wide",
                    html: $("<button>", {
                        id: "btn-remove",
                        class: "ui icon basic negative mini button",
                        type: "button",
                        html: '<i class="trash icon"></i>'
                    })
                })
            ]
        });

        $("#tb-answers").children("tbody").append(row);

		row.find("input[name^=min], input[name^=max], input[name^=values]").on("keypress", function(ev){
			if ((ev.which != 45 || $(this).val().indexOf('-') != -1) && 
				(ev.which != 46 || $(this).val().indexOf('.') != -1) && 
				(ev.which < 48 || ev.which > 57)) {
				ev.preventDefault();
			}
		});
		
        row.find("#btn-remove").click(function(){
            $(this).parents("tr").remove();
            rc();
        });
    };
    
    {{-- ДОБАВИТЬ СОБЫТИЯ К ФОРМЕ И ТАБЛИЦЕ [add_events] --}}
    const ae = function(){
        $("#btn-add").click(ar);

        {{-- ПРИСВОЕНИЕ СОБЫТИЯ К СПИСКУ ТИПОВ ОТВЕТОВ --}}
        $("#type").dropdown({ 
            onChange: function(val){
				let prev = $(this).attr("previous-value");
				
				if ((prev < 0 || prev > 2) || (val < 0 || val > 2)){
					$("#tb-answers > tbody").empty();
					
					if (val != 3) {
						$("#answers").show();
						$("#btn-add").show();
					}
					else {
						$("#answers").hide();
						$("#btn-add").hide();
					}
				}

				$(this).attr("previous-value", val);
            }
        });

        {{-- ПРИСВОЕНИЕ СПИСОКУ ОТВЕТОВ ВОЗМОЖНОСТЬ ПЕРЕТАСКИВАНИЯ --}}
        $("#tb-answers > tbody").sortable({
            start: function(ev, ui){ 
                $(ui.item).css("background-color", "rgba(251, 251, 251, 0.9)").css("border", "0.5px dashed rgba(0, 0, 0, 0.3)"); 
            },
            stop: function(ev, ui){  
                rc();
                $(ui.item).css("background-color", "transparent"); 
            }
        });
		
        {{-- ВАЛИДАЦИЯ ПОЛЕЙ --}}
		$("#tb-answers > tbody").find("input[name^=min], input[name^=max], input[name^=values]").on("keypress", function(ev){
			if ((ev.which != 45 || $(this).val().indexOf('-') != -1) && 
				(ev.which != 46 || $(this).val().indexOf('.') != -1) && 
				(ev.which < 48 || ev.which > 57)) {
				ev.preventDefault();
			}
		});
    };
</script> 