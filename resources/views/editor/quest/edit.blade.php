@extends('layouts.main', ['title' => config('app.name').' - Вопрос - #'.$quest->id])

@section('style')

<style type="text/css">
    .p-0{
        padding: 0 !important;
    }
    .td-w-min{
        min-width: 45px !important;
    }
    .i-cell{
        text-align: center !important;
        height: 30px !important;
    }
</style>

@endsection

@section('content')

<div class="ui centered grid">
    
    <div class="sixteen wide mobile tablet twelve wide computer centered column">

        <div class="ui tiny breadcrumb">
            <a class="section bread" href="{{ route('console.main') }}">Консоль</a>
            <i class="right angle icon divider"></i>
            <a class="section bread" href="{{ route('console.quest.main') }}">Вопросы</a>
            <i class="right angle icon divider"></i>
            <div class="section active">#{{ $quest->id }}</div>
        </div>
        
        <div class="ui divider"></div>
        
        <div class="ui fluid accordion">
            <div class="title active">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Основные параметры</b>
            </div>


            <div class="content active">
                <div class="main-holder" route-pairs="{{ route('editor.quest.pairs', [$quest->id]) }}" route-update="{{ route('editor.quest.update', ['id' => $quest->id]) }}">

                    @include('editor.quest.parts.info')

                </div>

            </div>
        </div>
        
        @if ($competences)
            <div class="ui divider"></div>

            {{-- КОМПЕТЕНТНОСТИ --}}
            <div class="ui fluid accordion">
                <div class="title">
                    <i class="dropdown icon"></i>
                    <b style="font-size:1.25rem;">Компетенции</b>
                </div>

                <div id="competence" class="content">
                    <br/>
                    <div class="link-holder" route-update="{{ route('editor.quest.compupdate', [$quest->id]) }}">

                        @include('editor.quest.parts.tablecompetence')

                    </div>
                    <br/>
                </div>
            </div>
            
            <div class="ui divider"></div>

            {{-- МАТРИЦА СМЕЖНОСТИ --}}
            <div class="ui fluid accordion">
                <div class="title">
                    <i class="dropdown icon"></i>
                    <b style="font-size:1.25rem;">Матрица смежности</b>
                </div>

                <div id="matrix" class="content">
                    <br/>
                    <div class="link-holder" route-update="{{ route('editor.quest.matrixupdate', [$quest->id]) }}">

                        @if ($hasCompetences->count() != 0)
                            @include('editor.quest.parts.tablematrix')
                        @else
                            <center>- НЕТ ВЫБРАННЫХ КОМПЕТЕНЦИЙ -</center>
                        @endif

                    </div>
                    <br/>
                </div>
            </div>
        @endif
        
    </div>
</div>

@endsection

@section('script')

@include('editor.parts.attach')
@include('editor.quest.parts.script')

<script type="text/javascript">
    {{-- КНОПКА УДАЛИТЬ [button_delete] --}}
    const bd = function(){
        let val = confirm("Вы уверены, что хотите удалить вопрос?");

        if (val) $(".main-holder form").attr("action", "{{ route('editor.quest.delete', [$quest->id]) }}").off("submit").submit();
    };
    {{-- УДАЧНОЕ ОБНОВЛЕНИЕ МОДЕЛИ [on_success] --}}
	const osc = function(holder, data, edit){		
		holder.empty().append(data.view);
		holder.find("#btn-pairs").click(function(){ gmf(this) });
        
        console.log(holder.find("#btn-pairs"));

		if (edit){
			{{-- ПРИСВОИМ СОБЫТИЯ К КНОПКАМ ФОРМЫ РЕДАКТИРОВАНИЯ --}}
			holder.find("form").submit(bms);
			holder.find("#btn-delete").click(bd);
			holder.find("table > tbody").children("tr").each(function(){
				$(this).find("#btn-remove").click(function(){
					$(this).parents("tr").remove();
					rc(); // [reset_counter]
				});
			});
			rc(); // [reset_counter]
			{{-- ВЫБИРАЕМ ЗНАЧЕНИЯ ТИПА ВОПРОСА ПО УМОЛЧАНИЮ --}}
			$("#type").dropdown("set selected", `${data.quest.type}`);
			ae(); // [add_events]
		}
	};
    {{-- ВАЛИДАЦИЯ ПОЛЕЙ ВВОДА МАТРИЦЫ [matix_input_validate] --}}
	const miv = function(ev){
		if ((ev.which != 45 || $(this).val().indexOf('-') != -1) && 
			(ev.which != 46 || $(this).val().indexOf('.') != -1) && 
			(ev.which < 48 || ev.which > 57)) {
			ev.preventDefault();
		}
	};
    
    $(".main-holder #btn-pairs").click(function(){ gmf(this) });    
    $("#competence #btn-save").click(function(ev){
		let btn = $(ev.target);
		let holder = btn.closest(".link-holder");
        ev.preventDefault();
        
        btn.addClass("loading");
		
        $.ajax({
            method: "post",
            url: holder.attr("route-update"),
            headers: {
                "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content'),
            },
            data: holder.find("form").serialize(),
            dataType: "json",
            complete: function(){
                btn.removeClass("loading");
            },
            success: function(data){
                holder.find("#btn-pairs").click();
                
                $("#matrix .link-holder").html(data.view);
                $("#matrix #btn-save").click(bas);
				$("#matrix input").on("keypress", miv);
                $("#matrix table.ui.celled").find("th, td").popup({ delay: {show: 50, hide: 0} });
            },
			error: (jq, text, err) => de(jq),
        });
    });
    $("#matrix #btn-save").click(bas);
	$("#matrix input").on("keypress", miv);
    $("#matrix table.ui.celled").find("th, td").popup({ delay: {show: 50, hide: 0} });
</script>


@endsection
