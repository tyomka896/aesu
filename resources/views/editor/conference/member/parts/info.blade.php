<div class="ui segment basic left aligned">
    <div class="ui one column grid">

        <div class="column">
            <table class="ui very basic unstackable table">
                <tbody>

                    <tr>
                        <td class="four wide"><b>ФИО</b></td>
                        <td class="twelve wide">{{ $member->name() }}</td>
                    </tr>

                    <tr>
                        <td class="four wide"><b>Уровень обучения</b></td>
                        <td class="twelve wide">{{ $member->status }}</td>
                    </tr>

                    <tr>
                        <td class="four wide"><b>Ученая степень</b></td>
                        <td class="twelve wide">{{ $member->degree }}</td>
                    </tr>

                    <tr>
                        <td class="four wide"><b>Организация</b></td>
                        <td class="twelve wide">{{ $member->organization }}</td>
                    </tr>

                    <tr>
                        <td class="four wide"><b>Город</b></td>
                        <td class="twelve wide">{{ $member->city }}</td>
                    </tr>

                    <tr>
                        <td class="four wide"><b>Email-адрес</b></td>
                        <td class="twelve wide">{{ $member->email }}</td>
                    </tr>

                    <tr>
                        <td class="four wide"><b>Форма участия</b></td>
                        <td class="twelve wide">{{ $member->format == 0 ? 'очная' : 'заочная' }}</td>
                    </tr>

                    <tr>
                        <td class="four wide"><b>Дата подачи</b></td>
                        <td class="twelve wide">{{ date('d.m.Y', $member->created_at->timestamp).' г.' }}</td>
                    </tr>

                    <tr>
                        <td class="four wide"><b>Имеется пропуск в город</b></td>
                        <td class="twelve wide">{!! $member->pass == 1 ? '&#10004; Да' : '&#10008; Нет' !!}</td>
                    </tr>

                </tbody>
            </table>

            @if ($member->format == 0)
                <table class="ui basic1 unstackable table">
                    <thead>
                        <tr>
                            <th class="center aligned" colspan="2">Данные для оформления пропуска</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td class="four wide"><b>Дата рождения</b></td>
                            <td class="twelve wide">{{ date('d.m.Y', $member->birthday->timestamp).' г.' }}</td>
                        </tr>

                        <tr>
                            <td class="four wide"><b>Место рождения</b></td>
                            <td class="twelve wide">{{ $member->birthplace }}</td>
                        </tr>

                        <tr>
                            <td class="four wide"><b>Место регистрации</b></td>
                            <td class="twelve wide">{{ $member->registered }}</td>
                        </tr>

                        <tr>
                            <td class="four wide"><b>Место работы</b></td>
                            <td class="twelve wide">{{ $member->work }}</td>
                        </tr>

                        <tr>
                            <td class="four wide"><b>Должность</b></td>
                            <td class="twelve wide">{{ $member->status }}</td>
                        </tr>

                        <tr>
                            <td class="four wide"><b>Способ прибытия</b></td>
                            <td class="twelve wide">{{ $member->route }}</td>
                        </tr>

                        <tr>
                            <td class="four wide"><b>Дата прибытия</b></td>
                            <td class="twelve wide">{{ date('d.m.Y', $member->arrival->timestamp).' г.' }}</td>
                        </tr>

                        <tr>
                            <td class="four wide"><b>Гостинница</b></td>
                            <td class="twelve wide">{!! $member->hotel == 1 ? '&#10004; Да' : '&#10008; Нет' !!}</td>
                        </tr>

                    </tbody>
                </table>
            @endif

        </div>

        @canany(['admin', 'delete'])
            <div class="column right aligned">
                <a id="delete-item" class="control" title="Удалить">
                    <i class="trash link icon"></i>
                </a>
            </div>
        @endcanany

    </div>
</div>
