/**
 * Главная панель навигации
 */
import React from 'react'
import { NavLink } from 'react-router-dom'
import { Grid, Menu, Dropdown } from 'semantic-ui-react'
import { Home, LibraryBooks, LaptopChromebook, School, InputRounded, PersonAdd } from '@material-ui/icons'

import auth, { APP_NAME, TOKEN } from 'utils/auth.jsx'

const styles = {
    mobile: { padding: '0 10px' },
    computer: { padding: '0 16px' },
}

const Navbar = _ => {
    const leftPart = []
    const rightPart = []

    leftPart.push(
        <NavLink key="home" exact to="/" className="item" activeClassName="">
            <Grid>
                <Grid.Row only="mobile" style={styles.mobile}><Home /></Grid.Row>
                <Grid.Row only="tablet computer" style={styles.computer}>Домой</Grid.Row>
            </Grid>
        </NavLink>
    )

    if (auth().check()) {
        leftPart.push(
            <NavLink key="tests" exact to="/tests" className="item" activeClassName="active" isActive={(_, { pathname }) => pathname.includes('test')}>
                <Grid>
                    <Grid.Row only="mobile" style={styles.mobile}><LibraryBooks /></Grid.Row>
                    <Grid.Row only="tablet computer" style={styles.computer}>Тесты</Grid.Row>
                </Grid>
            </NavLink>
        )
    }

    leftPart.push(
        <NavLink key="method" exact to="/methods" className="item" activeClassName="active" isActive={(_, { pathname }) => pathname.includes('method')}>
            <Grid>
                <Grid.Row only="mobile" style={styles.mobile}><School /></Grid.Row>
                <Grid.Row only="tablet computer" style={styles.computer}>Методики</Grid.Row>
            </Grid>
        </NavLink>
    )

    if (auth().check() && auth().canAny(['admin', 'console'])) {
        leftPart.push(
            <Menu.Item key="console" href="/console" active={window.location.href.indexOf("/console") >= 0}>
                <Grid>
                    <Grid.Row only="mobile" style={styles.mobile}><LaptopChromebook /></Grid.Row>
                    <Grid.Row only="tablet computer" style={styles.computer}>Консоль</Grid.Row>
                </Grid>
            </Menu.Item>
        )
    }

    if (auth().check()) {
        const logoutHandler = event => { event.preventDefault(); document.getElementById('logout-form').submit() }
        const shortName = `${auth().surname} ${auth().name.charAt(0)}. ${auth().patronym.charAt(0)}.`

        rightPart.push(
            <Dropdown key="auth" item text={shortName} icon={null}>
                <Dropdown.Menu>
                    <NavLink exact to="/profile" className="item" activeClassName="">Профиль</NavLink>
                    <Dropdown.Divider />
                    <NavLink exact to="/logout" className="item" onClick={logoutHandler}>Выйти</NavLink>
                    <form id="logout-form" action="/logout" method="POST" style={{ display: "none" }}>
                        <input type="hidden" name="_token" value={TOKEN} />
                    </form>
                </Dropdown.Menu>
            </Dropdown>
        )
    }
    else {
        rightPart.push(
            <NavLink key="login" exact to="/login" className="item" activeClassName="">
                <Grid>
                    <Grid.Row only="mobile" style={styles.mobile}><InputRounded /></Grid.Row>
                    <Grid.Row only="tablet computer" style={styles.computer}>Вход</Grid.Row>
                </Grid>
            </NavLink>
        )

        rightPart.push(
            <NavLink key="register" exact to="/register" className="item" activeClassName="">
                <Grid>
                    <Grid.Row only="mobile" style={styles.mobile}><PersonAdd /></Grid.Row>
                    <Grid.Row only="tablet computer" style={styles.computer}>Регистрация</Grid.Row>
                </Grid>
            </NavLink>
        )
    }

    return (
        <Menu inverted borderless size="large" className="fixed">
            <Menu.Item header>{APP_NAME}</Menu.Item>

            {leftPart}

            <Menu.Menu position="right">
                {rightPart}
            </Menu.Menu>
        </Menu>
    )
}

export default Navbar
