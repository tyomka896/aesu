/**
 * Форма регистрации
 */
import React from 'react'
import { Grid, Segment, Form, Checkbox, Divider } from 'semantic-ui-react'
import axios from 'utils/axios.jsx'

import { APP_NAME } from 'utils/auth.jsx'

class RegisterForm extends React.Component {
    state = {
        elements: {
            surname: {
                value: "",
                error: null,
                valid: false,
                rules: { min: 1, max: 50 },
            },
            name: {
                value: "",
                error: null,
                valid: false,
                rules: { min: 1, max: 50 },
            },
            patronym: {
                value: "",
                error: null,
                valid: false,
                rules: { min: 1, max: 50 },
            },
            login: {
                value: "",
                error: null,
                valid: false,
                rules: { min: 2, max: 50 },
            },
            password: {
                value: "",
                error: null,
                valid: false,
                rules: { min: 6, max: 255 },
            },
            password_confirmation: {
                value: "",
                error: null,
                valid: false,
                rules: { min: 6, max: 255 },
            },
            agreement: {
                value: false,
                valid: false,
            },
        },
        loading: false,
        valid: false
    }

    componentDidMount() {
        document.title = `Регистрация - ${APP_NAME}`

        this.loginCheckTimeout = null
    }

    /**
     * Изменение поля ввода формы
     */
    inputChangeHandler = (_, { name, value, checked }) => {
        const elements = { ...this.state.elements }
        const element = { ...elements[name] }

        if (name === "agreement") {
            element.value = checked
            element.valid = checked
        }
        else {
            element.value = value.trim()
            element.error = null
            element.valid = false

            if (element.value.length === 0) element.error = `Поле обязательно к заполнению`
            else if (element.value.length < element.rules.min) element.error = `Минимальная длина ${element.rules.min} символов`
            else if (element.value.length > element.rules.max) element.error = `Максимальная длина ${element.rules.max} символов`
            else element.valid = true
        }

        elements[name] = element

        elements.password_confirmation.error = null
        elements.password_confirmation.valid = true

        if (elements.password.value !== elements.password_confirmation.value) {
            elements.password_confirmation.error = `Не верное подтверждение пароля`
            elements.password_confirmation.valid = false
        }

        this.setState({
            elements: elements,
            valid: this.isValid(elements)
        })
    }

    /**
     * Проверка наличия пользователя с введенным логином в системе
     */
    loginChangeHandler = (_, { value }) => {
        clearTimeout(this.loginCheckTimeout)

        const elements = { ...this.state.elements }
        const login = { ...elements.login }

        login.value = value.trim()
        login.error = null
        login.valid = false

        if (login.value.length === 0) login.error = `Поле обязательно к заполнению`
        if (login.value.length < login.rules.min) login.error = `Минимальная длина ${login.rules.min} символов`
        else if (login.value.length > login.rules.max) login.error = `Максимальная длина ${login.rules.max} символов`

        this.setState(state => ({
            elements: {
                ...state.elements,
                login: login
            },
            valid: false,
        }))

        if (login.error) return

        this.loginCheckTimeout = setTimeout(() => {
            axios.get(`/register/${login.value || '_'}/exists`)
                .then(({ data }) => {
                    login.valid = !data.exists
                    login.error = data.exists ? 'Пользователь с таким логином уже существует' : null

                    elements.login = login

                    this.setState(state => ({
                        elements: {
                            ...state.elements,
                            login: {
                                ...state.elements.login,
                                valid: login.valid,
                                error: login.error,
                            }
                        },
                        valid: this.isValid(elements)
                    }))
                }).catch(() => { })
        }, 450)
    }

    formSubmitHandler = event => {
        event.preventDefault()

        this.setState({ loading: true })

        const elements = { ...this.state.elements }

        const data = {
            surname: elements.surname.value,
            name: elements.name.value,
            patronym: elements.patronym.value,
            login: elements.login.value,
            password: elements.password.value,
            password_confirmation: elements.password_confirmation.value,
            agreement: elements.agreement.value,
        }

        axios.post('/register', data)
            .then(({ data }) => {
                window.location.href = data.redirect || '/'
            })
            .catch(error => {
                if (error.response.status === 422) {
                    const errors = error.response.data.errors

                    if (errors) {
                        Object.keys(errors).forEach(key => {
                            const element = elements[key]

                            element.valid = false
                            element.error = errors[key][0]

                            elements[key] = element
                        })
                    }
                }

                this.setState({
                    elements: elements,
                    isValid: false,
                    loading: false,
                })
            })
    }

    render() {
        const { elements } = this.state

        return (
            <Grid.Column style={{ maxWidth: "550px" }}>
                <Segment>
                    <Form>
                        <div style={{ textAlign: "center" }}>
                            <label style={{ fontSize: "16px", fontWeight: "bold", textTransform: "uppercase" }}>
                                Регистрация
                            </label>
                        </div>

                        <Divider />

                        <Form.Input
                            required
                            name="surname"
                            label="Фамилия"
                            value={elements.surname.value}
                            placeholder="Фамилия"
                            autoComplete="off"
                            error={elements.surname.error ? { content: elements.surname.error } : null}
                            onChange={this.inputChangeHandler} />

                        <Form.Input
                            required
                            name="name"
                            label="Имя"
                            value={elements.name.value}
                            placeholder="Имя"
                            autoComplete="off"
                            error={elements.name.error ? { content: elements.name.error } : null}
                            onChange={this.inputChangeHandler} />

                        <Form.Input
                            required
                            name="patronym"
                            label="Отчество"
                            value={elements.patronym.value}
                            placeholder="Отчество"
                            autoComplete="off"
                            error={elements.patronym.error ? { content: elements.patronym.error } : null}
                            onChange={this.inputChangeHandler} />

                        <Form.Input
                            required
                            name="login"
                            label="Логин"
                            value={elements.login.value}
                            placeholder="Логин"
                            autoComplete="off"
                            error={elements.login.error ? { content: elements.login.error } : null}
                            onChange={this.loginChangeHandler} />

                        <Form.Input
                            required
                            name="password"
                            label="Пароль"
                            type="password"
                            value={elements.password.value}
                            placeholder="******"
                            autoComplete="off"
                            error={elements.password.error ? { content: elements.password.error } : null}
                            onChange={this.inputChangeHandler} />

                        <Form.Input
                            required
                            name="password_confirmation"
                            label="Подтвердите пароль"
                            type="password"
                            value={elements.password_confirmation.value}
                            placeholder="******"
                            autoComplete="off"
                            error={elements.password_confirmation.error ? { content: elements.password_confirmation.error } : null}
                            onChange={this.inputChangeHandler} />

                        <Form.Field>
                            <Checkbox
                                required
                                name="agreement"
                                checked={elements.agreement.value}
                                onChange={this.inputChangeHandler}
                                label="Я даю согласие на обработку своих персональных данных" />
                        </Form.Field>

                        <Divider />

                        <Form.Button
                            primary
                            type="submit"
                            loading={this.state.loading}
                            disabled={!this.state.valid || this.state.loading}
                            onClick={this.formSubmitHandler}
                        >
                            Создать
                        </Form.Button>
                    </Form>
                </Segment>
            </Grid.Column>
        )
    }

    /**
     * Проверка верности заполнения формы
     */
    isValid = elements => {
        return elements.surname.valid &&
            elements.name.valid &&
            elements.patronym.valid &&
            elements.login.valid &&
            elements.password.valid &&
            elements.password_confirmation.valid &&
            elements.agreement.valid
    }
}

export default RegisterForm
