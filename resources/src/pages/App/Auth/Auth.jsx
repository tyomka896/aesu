/**
 * Компонент для регистрации и авторизации пользователей
 */
import React from 'react'
import { Grid } from 'semantic-ui-react'
import { Route, Switch } from 'react-router-dom'

import LoginForm from './_routes/LoginForm.jsx'
import RegisterForm from './_routes/RegisterForm.jsx'

const Auth = () => (
    <Grid stackable>
        <Grid.Row centered columns={1}>
            <Switch>
                <Route exact path="/login" render={() => (<LoginForm />)} />

                <Route exact path="/register" render={() => (<RegisterForm />)} />
            </Switch>
        </Grid.Row>
    </Grid>
)

export default Auth
