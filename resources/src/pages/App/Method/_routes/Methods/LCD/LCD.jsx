/**
 * Методика оценки УРК
 */
import React, { Component } from 'react'
import { Grid, Form, Segment, Dropdown, Input, Button, Divider } from 'semantic-ui-react'
import queryString from 'utils/queryString.jsx'
import axios from 'utils/axios.jsx'
import dayjs from 'dayjs'

import auth, { APP_NAME } from 'utils/auth.jsx'
import PlaceholderContent from 'app/_components/PlaceholderContent/PlaceholderContent.jsx'
import ProfileCard from './ProfileCard/ProfileCard.jsx'

const hasAccess = auth().canAny(['admin', 'console'])
const STORAGE_NAME = 'method_profile'

class LCD extends Component {
    constructor() {
        super()

        const ls = JSON.parse(localStorage.getItem(STORAGE_NAME)) || {}

        /** Запрос с параметрами: ?y={year}&d={discipline_id}&b={basket_id} */
        const wls = queryString()

        /** Список годов заканчивая текущим */
        const currentYear = new Date().getFullYear()
        let yearOptions = []

        for (let q = 2019; q <= currentYear; q++) {
            yearOptions.push({ key: q, value: q, text: q })
        }

        this.state = {
            elements: {
                year: {
                    options: yearOptions,
                    selected: wls.y || ls.year_pass || currentYear,
                },
                discipline: {
                    options: [],
                    selected: null,
                    loading: true,
                },
                basket: {
                    options: [],
                    selected: null,
                    loading: false,
                }
            },
            profile: null,
            wls: wls,
            loading: false,
        }
    }

    componentDidMount() {
        document.title = `Оценка УРК - ${APP_NAME}`

        const { wls } = this.state
        const ls = JSON.parse(localStorage.getItem(STORAGE_NAME)) || {}

        axios.get(`/api/guest/disciplines`)
            .then(({ data }) => {
                const disciplines = data.map(elem => ({
                    key: elem.id,
                    value: `${elem.id}`,
                    text: elem.name,
                }))

                this.setState(state => ({
                    elements: {
                        ...state.elements,
                        discipline: {
                            options: disciplines,
                            selected: wls.d || ls.discipline_id || null,
                            loading: false,
                        }
                    }
                }), _ => this.requestBaskets())
            }).catch(() => { })
    }

    /**
     * Выбор года прохождения тестирований
    */
    yearChangedHandler = (_, { value }) => {
        let ls = JSON.parse(localStorage.getItem(STORAGE_NAME)) || {}
        ls.year_pass = value
        localStorage.setItem(STORAGE_NAME, JSON.stringify(ls))

        this.setState(state => ({
            elements: {
                ...state.elements,
                year: {
                    ...state.elements.year,
                    selected: value
                }
            },
            profile: null
        }), _ => this.requestBaskets())
    }

    /**
     * Выбор дисциплины
    */
    disciplineChangeHandler = (_, { value }) => {
        let ls = JSON.parse(localStorage.getItem(STORAGE_NAME)) || {}
        ls.discipline_id = value
        localStorage.setItem(STORAGE_NAME, JSON.stringify(ls))

        const elements = { ...this.state.elements }
        const { discipline, basket } = elements

        discipline.selected = value

        if (!value) {
            basket.options = []
            basket.selected = null
        }

        elements.discipline = discipline
        elements.basket = basket

        this.setState({
            elements: elements,
            profile: null,
        }, _ => this.requestBaskets())
    }

    /**
     * Выбор пройденного тестирования
    */
    basketChangedHandler = (_, { value, update }) => {
        this.setState(state => ({
            elements: {
                ...state.elements,
                basket: {
                    ...state.elements.basket,
                    selected: value
                }
            },
            profile: null,
            loading: value
        }))

        if (!value) return

        axios.get(`/api/guest/basket/${value}/profile${update ? '?update' : ''}`)
            .then(({ data }) => {
                this.setState({
                    profile: data.length ? data : null,
                    loading: false
                })
            })
            .catch(() => {
                this.setState({ loading: false })
            })
    }

    /**
     * Отображения названия оси диаграммы
    */
    displayChangeHandler = (event, index) => {
        let profile = [...this.state.profile]

        profile[index].display = event.target.value

        this.setState({ profile: profile })
    }

    /**
     * Отображение оси диаграммы
    */
    checkboxChangeHandler = (_, { checked }, index) => {
        let profile = [...this.state.profile]

        profile[index].v = checked

        this.setState({ profile: profile })
    }

    render() {
        const { elements, profile, loading } = this.state
        const { year, discipline, basket } = elements

        let update = null
        const selected = basket.selected

        if (hasAccess && selected && profile) {
            update = (
                <Button
                    basic
                    style={{ margin: "0 0 0 14px", height: "38px" }}
                    onClick={event => this.basketChangedHandler(event, { value: selected, update: true })}>
                    Обновить
                </Button>
            )
        }

        let profileCard = null

        if (loading) {
            profileCard = (<> <Divider /> <PlaceholderContent fluid /> </>)
        }
        else if (profile) {
            profileCard = (
                <ProfileCard
                    profile={[...profile]}
                    basket_id={selected}
                    displayChange={this.displayChangeHandler}
                    checkboxChange={this.checkboxChangeHandler} />
            )
        }

        return (
            <Grid.Column style={{ maxWidth: "800px" }}>
                <Segment>
                    <Form>
                        <Form.Field inline>
                            <label>Год прохождения:</label>
                            <Dropdown
                                inline
                                scrolling
                                name='yearPass'
                                options={year.options}
                                defaultValue={year.selected}
                                onChange={this.yearChangedHandler} />
                        </Form.Field>

                        <Form.Field>
                            <label>Дисциплина</label>
                            <Dropdown
                                fluid
                                search
                                selection
                                clearable
                                selectOnBlur={false}
                                placeholder="Дисциплина"
                                noResultsMessage="Ничего не найдено"
                                loading={discipline.loading}
                                options={discipline.options}
                                value={discipline.selected}
                                onChange={this.disciplineChangeHandler} />
                        </Form.Field>

                        <Form.Field>
                            <label>Пройденный тест</label>
                            <Input>
                                <Dropdown
                                    fluid
                                    selection
                                    search
                                    clearable
                                    selectOnBlur={false}
                                    placeholder="Пройденный тест"
                                    noResultsMessage="Ничего не найдено"
                                    loading={basket.loading}
                                    options={basket.options}
                                    value={basket.selected}
                                    onChange={this.basketChangedHandler} />
                                {update}
                            </Input>
                        </Form.Field>
                    </Form>

                    {profileCard}
                </Segment>
            </Grid.Column>
        )
    }

    /**
     * Получение списка пройденных тестов
    */
    requestBaskets = _ => {
        const elements = { ...this.state.elements }
        const { year, discipline } = elements

        const yearPass = year.selected
        const disciplineId = discipline.selected

        if (!Boolean(yearPass && disciplineId)) return

        this.setState(state => ({
            elements: {
                ...state.elements,
                basket: {
                    options: [],
                    selected: null,
                    loading: true,
                }
            }
        }))

        axios.get(`/api/guest/baskets?yearPass=${yearPass}&disciplineId=${disciplineId}`)
            .then(({ data }) => {
                const options = data.map((elem, index) => {
                    const user = elem.user
                    const userName = hasAccess ? `${user.surname} ${user.name.charAt(0)}. ${user.patronym.charAt(0)}.` : `Студент №${index + 1}`
                    const passed_at = dayjs(elem.passed_at).format('DD.MM.YYYY HH:mm:ss')

                    return {
                        key: elem.id,
                        value: `${elem.id}`,
                        text: `${userName} - ${elem.test.name} [${passed_at}]`
                    }
                })

                this.setState(state => ({
                    elements: {
                        ...state.elements,
                        basket: {
                            ...state.elements.basket,
                            options: options,
                            loading: false,
                        }
                    }
                }))
            })
            .catch(error => {
                this.setState(state => ({
                    elements: {
                        ...state.elements,
                        basket: {
                            ...state.elements.basket,
                            loading: false,
                        }
                    }
                }))
            })
            .then(() => {
                if (!this.state.wls) return

                this.basketChangedHandler(_, { value: this.state.wls.b })

                this.setState({ wls: null })
            })
    }
}

export default LCD
