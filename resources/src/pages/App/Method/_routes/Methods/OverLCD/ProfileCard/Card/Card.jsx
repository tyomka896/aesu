/**
 * Визуализация межпредметного компетентностного профиля
 */
import React from 'react'
import { Segment, Button, Icon } from 'semantic-ui-react'
import Sketch from 'react-p5'

let _p5 = null

const Card = props => {
    const profile = props.profile.reduce((red, elem) => {
            if (elem.v) red.push(elem)
            return red
        }, [])

    const leaf_count = profile.length

    const radius = 150
    const maxСoef = 1
    const minСoef = -1

    const toScale = val => (val - minСoef) * (radius / (maxСoef - minСoef))

    const grouped = profile.length > 0 && !('val' in profile[0])

    /**
     * Преднастройка холста отрисовки
     */
    const setup = (p5, canvasParentRef) => {
        p5.createCanvas(radius * 2 + 140, radius * 2 + 75).parent(canvasParentRef)
        p5.pixelDensity(3)
        p5.frameRate(12)
        _p5 = p5
    }

    /**
     * Цикл отрисовки
     */
    const draw = (p5) => {
        p5.background(255)
        p5.translate(p5.width / 2, p5.height / 2)

        const theta = -90 / 180 * p5.PI
        const next = p5.TWO_PI / profile.length

        /** Фоновое отображение осей */
        p5.noFill()
        for (let q = 0; q < p5.TWO_PI; q += next) {
            p5.strokeWeight(1.2)
            p5.stroke('rgb(190, 190, 190)')

            p5.line(
                0,
                0,
                radius * p5.cos(theta + q),
                radius * p5.sin(theta + q)
            )

            p5.line(
                radius * p5.cos(theta + q),
                radius * p5.sin(theta + q),
                radius * p5.cos(theta + q + next),
                radius * p5.sin(theta + q + next)
            )

            p5.strokeWeight(3)
            p5.stroke('rgba(250, 0, 0, 1)')

            p5.line(
                toScale(0) * p5.cos(theta + q),
                toScale(0) * p5.sin(theta + q),
                toScale(0) * p5.cos(theta + q + next),
                toScale(0) * p5.sin(theta + q + next)
            )
        }

        /** Групповой компетентностный профиль */
        p5.strokeWeight(2.2)
        if (grouped) {
            p5.stroke('rgba(0, 176, 240, 0.8)')
            p5.fill('rgba(0, 176, 240, 0.4)')

            p5.beginShape()
            for (let q = 0; q < leaf_count; q++){
                p5.vertex(
                    toScale(profile[q].max) * p5.cos(theta + next * q),
                    toScale(profile[q].max) * p5.sin(theta + next * q)
                )
            }
            p5.endShape(p5.CLOSE)

            p5.stroke('rgba(112, 48, 160, 0.8)')
            p5.fill('rgba(112, 48, 160, 0.4)')

            p5.beginShape()
            for (let q = 0; q < leaf_count; q++){
                p5.vertex(
                    toScale(profile[q].avg) * p5.cos(theta + next * q),
                    toScale(profile[q].avg) * p5.sin(theta + next * q)
                )
            }
            p5.endShape(p5.CLOSE)

            p5.stroke('rgba(250, 6, 76, 0.8)')
            p5.fill('rgba(250, 6, 76, 0.4)')

            p5.beginShape()
            for (let q = 0; q < leaf_count; q++){
                p5.vertex(
                    toScale(profile[q].min) * p5.cos(theta + next * q),
                    toScale(profile[q].min) * p5.sin(theta + next * q)
                )
            }
            p5.endShape(p5.CLOSE)
        }
        /** Компетентностный профиль */
        else {
            p5.stroke('rgba(0, 50, 250, 0.8)')
            p5.fill('rgba(0, 50, 250, 0.25)')

            p5.beginShape()
            for (let q = 0; q < leaf_count; q++){
                p5.vertex(
                    toScale(profile[q].val) * p5.cos(theta + next * q),
                    toScale(profile[q].val) * p5.sin(theta + next * q)
                )
            }
            p5.endShape(p5.CLOSE)
        }

        /** Числовые обозначение осей */
        p5.fill(0)
        p5.noStroke()

        p5.textSize(16)
        for (let q = 0; q < leaf_count; q++) {
            let indent = 12.5
            const angle = p5.degrees(theta + q * next)
            const word = profile[q].display || q + 1

            if (p5.abs(angle) === 90) indent = p5.textWidth(word) / 2
            else if (angle > 90) indent = p5.textWidth(word) - indent

            p5.text(
                word,
                (radius + 25) * p5.cos(theta + q * next) - indent,
                (radius + 25) * p5.sin(theta + q * next) + 5
            )
        }

        p5.textSize(14)
        p5.text(1, -15, radius * p5.sin(theta))
        p5.text(0, -15, toScale(0) * p5.sin(theta))
        p5.text(-1, -20, -5)
    }

    /**
     * Сохранение изображения профиля
     */
    const saveClickHandler = _ => {
        _p5.saveCanvas('profile', 'png')
    }

    return (
        <Segment>
            <div style={{ position: "absolute", top: "0", right: "0" }}>
                <Button
                    icon
                    basic
                    compact
                    style={{ margin: "0", boxShadow: "none" }}
                    onClick={ saveClickHandler }>
                        <Icon name="save" style={{ fontSize: "1.1em" }} />
                    </Button>
            </div>

            <div style={{ textAlign: "center" }}>
                <Sketch setup={ setup } draw={ draw } />
            </div>
        </Segment>
    )
}

export default Card
