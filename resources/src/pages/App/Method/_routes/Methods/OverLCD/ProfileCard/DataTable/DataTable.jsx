/**
 * Таблица оцениваемых компетенций профиля
 */
import React from 'react'
import { Table, Input, Checkbox, Popup } from 'semantic-ui-react'

const DataTable = props => {
    const grouped = !('val' in props.profile[0])
    const { profile, displayChange, checkboxChange } = props

    const headers = grouped ? (
        <>
            <Table.HeaderCell width={ 2 } textAlign="center">
                <Popup
                    wide
                    position="top center"
                    content="Максимальное значение коэффициента"
                    trigger={ <span style={{ display: "block" }}>МАКС</span> } />
            </Table.HeaderCell>
            <Table.HeaderCell width={ 2 } textAlign="center">
                <Popup
                    wide
                    position="top center"
                    content="Среднее значение коэффициента"
                    trigger={ <span style={{ display: "block" }}>СРЕД</span> } />
            </Table.HeaderCell>
            <Table.HeaderCell width={ 2 } textAlign="center">
                <Popup
                    wide
                    position="top right"
                    content="Минимальное значение коэффициента"
                    trigger={ <span style={{ display: "block" }}>МИН</span> } />
            </Table.HeaderCell>
        </>
    ) : (
        <Table.HeaderCell width={ 2 } textAlign="center">
            <Popup
                wide
                position="top center"
                content="Коэффициент уверенности"
                trigger={ <span style={{ display: "block" }}>КУ</span> } />
        </Table.HeaderCell>
    )

    const rows = profile.map((elem, index) => (
        <Table.Row key={ `${index}${elem.id}` }>
            <Table.Cell>
                <Input
                    fluid
                    value={ elem.display || '' }
                    placeholder={ index + 1 }
                    onChange={ event => displayChange(event, index) }
                    input={ <input type="text" style={{ padding: "6px 4px", textAlign: "center" }} /> } />
            </Table.Cell>
            <Table.Cell>
                <Checkbox
                    checked={ elem.v }
                    label={ elem.name }
                    onChange={ (_, data) => checkboxChange(_, data, index) } />
            </Table.Cell>
            {
                grouped ? (
                    <>
                        <Table.Cell textAlign="center">{ elem.max }</Table.Cell>
                        <Table.Cell textAlign="center">{ elem.avg }</Table.Cell>
                        <Table.Cell textAlign="center">{ elem.min }</Table.Cell>
                    </>
                ) : (
                    <Table.Cell textAlign="center">{ elem.val }</Table.Cell>
                )
            }
        </Table.Row>
    ))

    return (
        <Table basic="very" unstackable compact>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell textAlign="center" style={{ minWidth: "70px" }}>
                        <Popup
                            wide
                            position="top left"
                            content="Обозначение на диаграмме"
                            trigger={ <span style={{ display: "block" }}>#</span> } />
                    </Table.HeaderCell>
                    <Table.HeaderCell width={ grouped ? 9 : 13 }>Компетентность</Table.HeaderCell>
                    { headers }
                </Table.Row>
            </Table.Header>
            <Table.Body>
                { rows }
            </Table.Body>
        </Table>
    )
}

export default DataTable
