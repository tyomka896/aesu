/**
 * Страница отображения методик
 */
import React, { Suspense } from 'react'
import { Route, Switch } from 'react-router-dom'
import { Grid } from 'semantic-ui-react'

import PlaceholderContent from 'app/_components/PlaceholderContent/PlaceholderContent.jsx'

import Cards from './_routes/Cards/Cards.jsx'
const LCD = React.lazy(() => import('./_routes/Methods/LCD/LCD.jsx'))
const OverLCD = React.lazy(() => import('./_routes/Methods/OverLCD/OverLCD.jsx'))

const Method = _ => (
    <Grid stackable>
        <Grid.Row centered columns={1}>
            <Suspense fallback={<PlaceholderContent fluid />}>
                <Switch>
                    <Route exact path="/methods" render={_ => <Cards />} />
                    <Route exact path="/method/profile" render={_ => <LCD />} />
                    <Route exact path="/method/over-discipline" render={_ => <OverLCD />} />
                </Switch>
            </Suspense>
        </Grid.Row>
    </Grid>
)

export default Method