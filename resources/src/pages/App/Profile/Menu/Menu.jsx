/**
 * Элементы бокового меню страницы профиля
 */
import React from 'react'
import { NavLink } from 'react-router-dom'

const Menu = () => (
    <div className="sidebar-menu">
        <NavLink exact to="/profile" className="sidebar-menu__item" activeClassName="">
            <b>Профиль</b>
        </NavLink>
        <NavLink exact to="/profile/info" className="sidebar-menu__item" activeClassName="sidebar-menu__item--active">
            Имя и должность
        </NavLink>
        <NavLink exact to="/profile/branch" className="sidebar-menu__item" activeClassName="sidebar-menu__item--active">
            Группа
        </NavLink>
        <NavLink exact to="/profile/email" className="sidebar-menu__item" activeClassName="sidebar-menu__item--active">
            Настройка почты
        </NavLink>
        <NavLink exact to="/profile/password" className="sidebar-menu__item" activeClassName="sidebar-menu__item--active">
            Изменение пароля
        </NavLink>
        <NavLink exact to="/profile/basket" className="sidebar-menu__item" activeClassName="sidebar-menu__item--active">
            Пройденные тесты
        </NavLink>
    </div>
)

export default Menu
