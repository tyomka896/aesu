/**
 * Настройки общей информации
 */
import React from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { Grid, Form, Breadcrumb, Icon, Divider } from 'semantic-ui-react'
import axios from 'utils/axios.jsx'

import auth, { APP_NAME } from 'utils/auth.jsx'
import store, { ProfileTypes } from 'store/store.jsx'

class ProfileInfo extends React.Component {
    state = {
        elements: {
            surname: {
                value: auth().surname,
                error: false,
                valid: false,
                rules: { min: 1, max: 50, trim: true }
            },
            name: {
                value: auth().name,
                error: false,
                valid: false,
                rules: { min: 1, max: 50, trim: true }
            },
            patronym: {
                value: auth().patronym,
                error: false,
                valid: false,
                rules: { min: 1, max: 50, trim: true }
            },
            status: {
                value: auth().userstatus.id,
                loading: true
            }
        },
        loading: false,
        isValid: false
    }

    componentDidMount() {
        document.title = `Имя и должность - ${APP_NAME}`

        const elements = { ...this.state.elements }
        const status = { ...elements.status }

        const { statuses } = this.props.profile

        if (statuses.loaded) {
            status.loading = false
            elements.status = status

            this.setState({ elements: elements })
        }
        else {
            axios.get('/api/userstatus')
                .then(({ data }) => {
                    const options = data.map(elem => ({
                        key: elem.id,
                        value: elem.id,
                        text: elem.name
                    }))

                    store.dispatch(ProfileTypes.statuses(options))

                    status.loading = false
                    elements.status = status

                    this.setState({ elements: elements })
                }).catch(() => { })
        }
    }

    /**
     * Изменение поля ввода
     */
    inputChangeHandler = (_, { name, value }) => {
        const elements = { ...this.state.elements }
        const element = { ...elements[name] }

        element.value = element.rules.trim ? value.trim() : value
        element.error = null
        element.valid = false

        if (element.value.length === 0) element.error = `Поле обязательно к заполнению`
        else if (element.value.length < element.rules.min) element.error = `Минимальная длина ${element.rules.min} символов`
        else if (element.value.length > element.rules.max) element.error = `Максимальная длина ${element.rules.max} символов`
        else element.valid = true

        elements[name] = element

        this.setState({ elements: elements }, _ => this.isFormValid())
    }

    /**
     * Выбор должности
     */
    dropdownChangeHandler = (_, { value }) => {
        const elements = { ...this.state.elements }
        const status = { ...elements.status }

        status.value = value
        elements.status = status

        this.setState({ elements: elements }, _ => this.isFormValid())
    }

    /**
     * Инициирование формы
     */
    submitFormHandler = () => {
        this.setState({ loading: true })

        const { elements } = this.state

        const data = {
            surname: elements.surname.value,
            name: elements.name.value,
            patronym: elements.patronym.value,
            statusId: elements.status.value,
        }

        axios.put('/api/auth/info', data)
            .then(({ data }) => {
                if (data.result) {
                    auth().surname = data.surname
                    auth().name = data.name
                    auth().patronym = data.patronym
                    auth().userstatus = data.status
                }

                this.setState({
                    loading: false,
                    isValid: false,
                })
            })
            .catch(({ response }) => {
                const errors = response.data.errors

                const keys = Object.keys(errors)

                if (keys.length) {
                    for (let key of keys) {
                        elements[key].error = errors[key]
                        elements[key].valid = false
                    }

                    this.setState({
                        elements: elements,
                        loading: false,
                    })
                }
            })
    }

    render() {
        const { elements, loading, isValid } = this.state
        const { statuses } = this.props.profile

        return (
            <Grid.Row columns={1}>
                <Grid.Column verticalAlign="middle">
                    <div className="button-toggle" onClick={this.props.onToggle}><Icon name="bars" /></div>

                    <Breadcrumb
                        size="tiny"
                        icon="right angle"
                        sections={[
                            { key: 0, content: 'Профиль', to: '/profile', as: (NavLink), activeClassName: '' },
                            { key: 1, content: 'Имя и должность', link: false },
                        ]} />
                </Grid.Column>

                <Divider />

                <Grid.Column>
                    <Form>
                        <Form.Input
                            label="Фамилия"
                            name="surname"
                            type="text"
                            value={elements.surname.value}
                            placeholder="Фамилия"
                            autoComplete="off"
                            error={elements.surname.error}
                            onChange={this.inputChangeHandler} />

                        <Form.Input
                            label="Имя"
                            name="name"
                            type="text"
                            value={elements.name.value}
                            placeholder="Имя"
                            autoComplete="off"
                            error={elements.name.error}
                            onChange={this.inputChangeHandler} />

                        <Form.Input
                            label="Отчество"
                            name="patronym"
                            type="text"
                            value={elements.patronym.value}
                            placeholder="Отчество"
                            autoComplete="off"
                            error={elements.patronym.error}
                            onChange={this.inputChangeHandler} />

                        <Form.Dropdown
                            fluid
                            search
                            selection
                            label="Занимаемая должность"
                            placeholder="Выберите должность"
                            loading={elements.status.loading}
                            value={elements.status.value}
                            options={statuses.data}
                            onChange={this.dropdownChangeHandler} />

                        <Grid columns={1}>
                            <Grid.Column textAlign="left">
                                <Form.Button
                                    primary
                                    size="small"
                                    type="submit"
                                    loading={loading}
                                    disabled={!isValid || loading}
                                    onClick={this.submitFormHandler}
                                >
                                    Сохранить
                                </Form.Button>
                            </Grid.Column>
                        </Grid>
                    </Form>
                </Grid.Column>
            </Grid.Row>

        )
    }

    /**
     * Проверка верности заполнения формы
     */
    isFormValid = _ => {
        let isValid = true

        const { surname, name, patronym, status } = this.state.elements

        isValid = surname.valid && surname.value !== auth().surname ||
            name.valid && name.value !== auth().name ||
            patronym.valid && patronym.value !== auth().patronym ||
            status.value !== auth().userstatus.id

        this.setState({ isValid: isValid })
    }
}

const mapStateToProps = state => ({ profile: state.profile })

export default connect(mapStateToProps)(ProfileInfo)
