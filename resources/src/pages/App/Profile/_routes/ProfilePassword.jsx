/**
 * Настройки пароля
 */
import React from 'react'
import { NavLink } from 'react-router-dom'
import { Grid, Form, Breadcrumb, Icon, Divider } from 'semantic-ui-react'
import axios from 'utils/axios.jsx'
import { APP_NAME } from 'utils/auth.jsx'

class ProfilePassword extends React.Component {
    state = {
        elements: {
            current: {
                value: "",
                icon: {
                    type: 'password',
                    name: 'eye slash',
                    show: false
                },
                error: null,
                valid: false,
                rules: {
                    min: 6,
                    max: 255
                }
            },
            password: {
                value: "",
                error: null,
                valid: false,
                rules: {
                    min: 6,
                    max: 255
                }
            },
            password_confirmation: {
                value: "",
                error: null,
                valid: false,
                rules: {
                    min: 0,
                    max: 255
                }
            }
        },
        loading: false,
        isValid: false,
    }

    componentDidMount() {
        document.title = `Изменение пароля - ${APP_NAME}`
    }

    /**
     * Переключение между скрытым и открытым вводом пароля
     */
    currentIconHandler = _ => {
        const elements = { ...this.state.elements }
        const current = { ...elements.current }

        current.icon.show = !current.icon.show
        current.icon.type = current.icon.show ? 'text' : 'password'
        current.icon.name = current.icon.show ? 'eye' : 'eye slash'

        elements.current = current

        this.setState({ elements: elements })
    }

    /**
     * Изменение поля ввода
     */
    inputChangeHandler = (event, name) => {
        const elements = { ...this.state.elements }
        const element = { ...elements[name] }

        element.value = event.target.value.trim()
        element.error = null
        element.valid = false

        if (element.value.length === 0) element.error = `Поле обязательно к заполнению`
        else if (element.value.length < element.rules.min) element.error = `Минимальная длина ${element.rules.min} символов`
        else if (element.value.length > element.rules.max) element.error = `Максимальная длина ${element.rules.max} символов`
        else element.valid = true

        elements[name] = element

        elements.password_confirmation.error = null
        elements.password_confirmation.valid = true

        if (elements.password.value !== elements.password_confirmation.value &&
            elements.password_confirmation.value.trim() !== '') {
            elements.password_confirmation.error = `Не верное подтверждение пароля`
            elements.password_confirmation.valid = false
        }

        this.setState({ elements: elements }, _ => this.isFormValid())
    }

    /**
     * Инициирование формы
     */
    submitFormHandler = () => {
        this.setState({ loading: true })

        const elements = { ...this.state.elements }

        const data = {
            current: elements.current.value,
            password: elements.password.value,
            password_confirmation: elements.password_confirmation.value
        }

        axios.put('/api/auth/password', data)
            .then(() => {
                window.location.href = window.location.pathname
            })
            .catch(({ response }) => {
                const errors = response.data.errors

                const keys = Object.keys(errors)

                if (keys.length) {
                    for (let key of keys) {
                        elements[key].error = errors[key]
                        elements[key].valid = false
                    }
                }

                this.setState({
                    elements: elements,
                    loading: false
                })
            })
    }

    render() {
        const { elements, loading, isValid } = this.state

        return (
            <Grid.Row columns={1}>
                <Grid.Column verticalAlign="middle">
                    <div className="button-toggle" onClick={this.props.onToggle}><Icon name="bars" /></div>

                    <Breadcrumb
                        size="tiny"
                        icon="right angle"
                        sections={[
                            { key: 0, content: 'Профиль', to: '/profile', as: (NavLink), activeClassName: '' },
                            { key: 1, content: 'Изменение пароля', link: false },
                        ]} />
                </Grid.Column>

                <Divider />

                <Grid.Column>
                    <Form>
                        <Form.Input
                            label="Старый пароль"
                            name="current"
                            type={elements.current.icon.type}
                            value={elements.current.value}
                            placeholder="Старый пароль"
                            icon={
                                <Icon
                                    link
                                    name={elements.current.icon.name}
                                    onClick={this.currentIconHandler} />
                            }
                            autoComplete="off"
                            error={elements.current.error ? { content: elements.current.error } : null}
                            onChange={event => this.inputChangeHandler(event, "current")} />

                        <Form.Input
                            label="Новый пароль"
                            name="password"
                            type="password"
                            value={elements.password.value}
                            placeholder="Новый пароль"
                            autoComplete="off"
                            error={elements.password.error ? { content: elements.password.error } : null}
                            onChange={event => this.inputChangeHandler(event, "password")} />

                        <Form.Input
                            label="Подтверждение пароля"
                            name="password_confirmation"
                            type="password"
                            value={elements.password_confirmation.value}
                            placeholder="Подтверждение пароля"
                            autoComplete="off"
                            error={elements.password_confirmation.error ? { content: elements.password_confirmation.error } : null}
                            onChange={event => this.inputChangeHandler(event, "password_confirmation")} />

                        <Grid columns={1}>
                            <Grid.Column textAlign="left">
                                <Form.Button
                                    primary
                                    size="small"
                                    type="submit"
                                    loading={loading}
                                    disabled={!isValid || loading}
                                    onClick={this.submitFormHandler}
                                >
                                    Сохранить
                                </Form.Button>
                            </Grid.Column>
                        </Grid>
                    </Form>
                </Grid.Column>
            </Grid.Row>
        )
    }

    /**
     * Проверка верности заполнения формы
     */
    isFormValid = () => {
        const { current, password, password_confirmation } = this.state.elements

        let isValid = current.valid &&
            password.valid &&
            password_confirmation.valid

        this.setState({ isValid: isValid })
    }
}

export default ProfilePassword
