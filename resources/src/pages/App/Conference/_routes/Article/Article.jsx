/**
 * Бланк на подачу статьи
 */
import React from 'react'
import { Grid, Form, Segment, Dropdown, Popup, Divider, Button } from 'semantic-ui-react'
import axios from 'utils/axios.jsx'

import { APP_NAME } from 'utils/auth.jsx'

import Members from './Members/Members.jsx'
import Documents from './Documents/Documents.jsx'
import Completed from './Completed/Completed.jsx'

class Article extends React.Component {
    state = {
        name: {
            value: '',
            error: null,
            valid: false,
            rules: { min: 3, max: 65535 },
        },
        section: { options: [], selected: null },
        members: [{ options: [], selected: null }],
        documents: [],
        documentsError: '',
        loading: false,
        completed: null,
        isValid: false,
    }

    componentDidMount() {
        document.title = `Заявка на публикацию статьи - ${APP_NAME}`

        axios.post('/api/guest/conf/sections')
            .then(({ data }) => {
                const section = { ...this.state }

                const options = data.map(elem => ({
                    key: elem.id,
                    text: elem.name,
                    value: elem.id,
                    content: (
                        <Popup
                            wide="very"
                            content={elem.example}
                            trigger={<div>{elem.name}</div>}
                            position="top left" />
                    ),
                }))

                section.options = options

                this.setState({ section: section })
            }).catch(() => { })
    }

    /**
     * Изменить список участников
     */
    setMembers = members => {
        this.setState({ members: members }, _ => this.isFormValid())
    }

    /**
     * Изменить список выбранных документов
     */
    setDocuments = documents => {
        let error = ''

        if (documents.length > 0) {
            let hasWord = documents.some(elem => {
                const extension = elem.name.split('.').slice(-1)[0]

                return ['doc', 'docx'].includes(extension)
            })

            if (!hasWord) {
                error = 'Одни из приложенных документов должен быть формата Word'
            }
        }

        this.setState({
            documents: documents,
            documentsError: error
        }, _ => this.isFormValid())
    }

    /**
     * Изменение наименования статьи
     */
    nameChangeHandler = (_, { value }) => {
        const element = { ...this.state.name }

        element.value = value
        element.error = null
        element.valid = false

        if (element.value.length === 0) element.error = `Поле обязательно к заполнению`
        else if (element.value.length < element.rules.min) element.error = `Минимальная длина ${element.rules.min} символов`
        else if (element.value.length > element.rules.max) element.error = `Максимальная длина ${element.rules.max} символов`
        else element.valid = true

        this.setState({ name: element }, _ => this.isFormValid())
    }

    /**
     * Изменение темы статьи
     */
    sectionChangeHandler = (_, { value }) => {
        const section = { ...this.state.section }

        section.selected = value

        this.setState({ section: section }, _ => this.isFormValid())
    }

    /**
     * Создание статьи
     */
    fromSubmitHandler = _ => {
        const { name, section, members, documents, isValid } = this.state

        if (!isValid) return

        this.setState({ loading: true })

        const _members = members.reduce((red, elem) => {
            if (!elem.selected) return red

            red.push(elem.selected)

            return red
        }, [])

        const formData = new FormData()

        formData.append('name', name.value.trim())
        formData.append('section', section.selected)
        formData.append('members', JSON.stringify(_members))

        for (let document of documents) {
            formData.append('documents[]', document)
        }

        axios.post('/api/guest/conf/article', formData)
            .then(({ data }) => {
                if (data.result) {
                    this.setState({
                        completed: { url: data.url }
                    })
                }
            })
            .catch(() => {
                this.setState({ loading: false })
            })
    }

    render() {
        const {
            name,
            section,
            members,
            documents,
            documentsError,
            loading,
            completed,
            isValid,
        } = this.state

        if (completed) {
            return (<Completed info={completed} />)
        }

        return (
            <Grid.Row centered columns={1}>
                <Grid.Column style={{ maxWidth: "800px" }}>
                    <Segment>
                        <p style={{ textAlign: "center", fontSize: "1.15rem", textTransform: "uppercase" }}>
                            <b>Заявка на публикацию статьи</b>
                        </p>

                        <Divider />

                        <Form>
                            <Form.Input
                                required
                                label="Заглавие статьи"
                                value={name.value}
                                placeholder="Заглавие статьи"
                                autoComplete="off"
                                error={name.error ? { content: name.error } : null}
                                onChange={this.nameChangeHandler} />

                            <Form.Field required>
                                <label>Секция конференции</label>

                                <Dropdown
                                    fluid
                                    selection
                                    selectOnBlur={false}
                                    placeholder="Выбор темы конференции"
                                    options={section.options}
                                    value={section.selected}
                                    onChange={this.sectionChangeHandler} />
                            </Form.Field>

                            <Form.Field required>
                                <label>Авторы статьи</label>

                                <Members
                                    members={members}
                                    setMembers={this.setMembers} />
                            </Form.Field>

                            <Form.Field required>
                                <label>Приложенные документы</label>

                                <Documents
                                    documents={documents}
                                    documentsError={documentsError}
                                    setDocuments={this.setDocuments} />
                            </Form.Field>

                            <Divider />

                            <Button
                                primary
                                loading={loading}
                                disabled={!isValid || loading}
                                style={{ width: "120px" }}
                                onClick={this.fromSubmitHandler} >Создать</Button>
                        </Form>
                    </Segment>
                </Grid.Column>
            </Grid.Row>
        )
    }

    /**
     * Проверка верности заполнения формы
     */
    isFormValid = _ => {
        const { name, section, members, documents } = this.state

        let isValid = true

        /** Имя и раздел */
        if (!name.valid || !section.selected) {
            isValid = false
        }

        /** Участники */
        if (isValid) isValid = Boolean(members[0].selected)

        /** Документы */
        if (isValid) {
            isValid = documents.length > 0

            if (documents.length > 0) {
                let hasWord = documents.some(elem => {
                    const extension = elem.name.split('.').slice(-1)[0]

                    return ['doc', 'docx'].includes(extension)
                })

                if (isValid) isValid = hasWord
            }
        }

        this.setState({ isValid: isValid })
    }
}

export default Article
