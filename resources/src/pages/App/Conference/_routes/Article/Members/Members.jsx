/**
 * Выбор списка авторов на статью
 */
import React from 'react'
import { Table, Message, List, Button, Dropdown } from 'semantic-ui-react'
import Delete from '@material-ui/icons/Delete'
import debounce from 'debounce'
import axios from 'utils/axios.jsx'

class Members extends React.Component {
    state = {
        exceeded: false
    }

    /**
     * Поиск участника конференции
     */
    memberSearchChangeHandler = (_, data) => {
        const index = data.index
        const query = data.searchQuery.trim()

        const members = [...this.props.members]
        const member = { ...members[index] }

        if (query.length < 3) {
            member.options = []
            members[index] = member

            this.props.setMembers(members)

            return
        }

        axios.post(`/api/guest/conf/members`, { fullName: query })
            .then(({ data }) => {
                const options = data.map(elem => ({
                    key: elem.id,
                    text: `${elem.surname} ${elem.name} ${elem.patronym}`,
                    value: elem.id,
                }))

                member.options = options
                members[index] = member

                this.props.setMembers(members)
            }).catch(() => { })
    }

    /**
     * Выбор участника конференции из списка
     */
    memberChangeHandler = (_, { index, value }) => {
        const members = [...this.props.members]
        const member = { ...members[index] }

        const option = [...member.options].find(elem => elem.value === value)
        const options = [option]

        member.options = options
        member.selected = value
        members[index] = member

        this.props.setMembers(members)
    }

    /**
     * Добавить строку поиска участника
     */
    addMemberHandler = _ => {
        const { exceeded } = this.state

        if (exceeded) return

        const members = [...this.props.members]

        members.push({ options: [], selected: null })

        if (members.length >= 10) {
            this.setState({ exceeded: !exceeded })
        }

        this.props.setMembers(members)
    }

    /**
     * Удалить строку поиска участника
     */
    removeMemberHandler = index => {
        const { exceeded } = this.state

        if (exceeded) {
            this.setState({ exceeded: !exceeded })
        }

        const members = [...this.props.members]

        members.splice(index, 1)

        this.props.setMembers(members)
    }

    render() {
        const { exceeded } = this.state
        const { members } = this.props

        const body = (
            members.map((elem, index) => {
                const button = index === 0 ? (
                    <Button
                        basic
                        compact
                        positive
                        value="Добавить"
                        disabled={exceeded}
                        onClick={this.addMemberHandler} >Добавить</Button>
                ) : (
                    <Button
                        basic
                        compact
                        negative
                        icon={<Delete style={{ fontSize: "16px", verticalAlign: "bottom" }} />}
                        onClick={_ => this.removeMemberHandler(index)} />
                )

                return (
                    <Table.Row key={index}>
                        <Table.Cell textAlign="center">{index + 1}</Table.Cell>
                        <Table.Cell>
                            <Dropdown
                                key={index}
                                index={index}
                                fluid
                                search
                                selection
                                selectOnBlur={false}
                                placeholder="Поиск"
                                noResultsMessage="Авторов не найдено"
                                options={elem.options}
                                value={elem.selected}
                                onSearchChange={debounce(this.memberSearchChangeHandler, 600)}
                                onChange={this.memberChangeHandler} />
                        </Table.Cell>
                        <Table.Cell textAlign="center">
                            {button}
                        </Table.Cell>
                    </Table.Row>
                )
            })
        )

        return (
            <>
                <Message info style={{ margin: "8px 0", textAlign: "justify" }}>
                    <List bulleted>
                        <List.Item>
                            Если вы уже заполняли бланк участника, то в ниже приведенном списке{' '}
                            в поле вода введите Ф.И.О. зарегистрированного автора.{' '}
                            После, в раскрывшемся списке найденных участников выберите нужного.
                        </List.Item>
                        <List.Item>
                            Если кто-либо из соавторов статьи не зарегистрирован на стайте,{' '}
                            то внесите данные по каждому из них через соответствующую{' '}
                            <a
                                href="/conf/member"
                                target="_blank"
                                title="Подать заявку на участника конференции"
                                style={{ textDecoration: "underline" }} >
                                ФОРМУ
                            </a>{' '}
                            прежде, чем оформлять заявку.
                        </List.Item>
                    </List>
                </Message>

                <Table basic="very" compact unstackable>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell width={1} textAlign="center">#</Table.HeaderCell>
                            <Table.HeaderCell width={13}>Имя участника конференции</Table.HeaderCell>
                            <Table.HeaderCell width={2} textAlign="center"></Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {body}
                    </Table.Body>
                </Table>
            </>
        )
    }
}

export default Members
