/**
 * Ссылки на подачу заявления
 */
import React from 'react'

import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import ArrowForwardIcon from '@material-ui/icons/ArrowForward'

const Links = props => {
    const { year, show, appealClick } = props

    const links = []

    if (show.left) {
        links.push(
            <div key="left">
                <span
                    onClick={ () => appealClick(year - 1) }
                    style={{ textDecoration: 'underline', cursor: 'pointer' }}
                >
                    <ArrowBackIcon
                        style={{ verticalAlign: 'middle', fontSize: '18px', marginRight: '4px' }} />
                    Конференция { year - 1 }
                </span>
            </div>
        )
    }

    if (show.right) {
        links.push(
            <div key="right" style={{ marginLeft: 'auto' }}>
                <span
                    onClick={ () => appealClick(year + 1) }
                    style={{ textDecoration: 'underline', cursor: 'pointer' }}
                >
                    Конференция { year + 1 }
                    <ArrowForwardIcon
                        style={{ verticalAlign: 'middle', fontSize: '18px', marginLeft: '4px' }} />
                </span>
            </div>
        )
    }

    return (
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            { links }
        </div>
    )
}

export default Links
