/**
 * Проведение тестирования пользователя
 */
import React from 'react'
import { Redirect, Prompt } from 'react-router-dom'
import { Grid, Form, Segment, Divider } from 'semantic-ui-react'
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers'
import axios from 'utils/axios.jsx'
import dayjs from 'dayjs'
import duration from 'dayjs/plugin/duration'
import DateFnsUtils from '@date-io/dayjs'
import 'dayjs/locale/ru'

dayjs.extend(duration)
dayjs.locale('ru')

import auth, { APP_NAME } from 'utils/auth.jsx'

import 'app/_styles/Range/Range.scss'
import Question from './Question/Question.jsx'
import HasNoAccess from './HasNoAccess/HasNoAccess.jsx'
import ExamSummary from './ExamSummary/ExamSummary.jsx'
import PlaceholderContent from 'app/_components/PlaceholderContent/PlaceholderContent.jsx'

// Если статус пользователя "Выпускник"
const isGraduate = auth().userstatus.id === 6

const STORAGE_NAME = 'tests'

class Examination extends React.Component {
    state = {
        current: 0,
        total: 0,
        values: [],
        timer: null,
        access: true,
        completed: false,
        summary: false,
        loading: true,
        backDate: null,
    }

    constructor() {
        super()

        /** Данные о тесте и текущем вопросе */
        this.test = null
        this.questions = []
        this.currentQuestion = null

        /** Хранение ответов по мере прохождения теста */
        this.testResults = []
        this.scoreResult = null

        /** Время начала тестирования */
        this.timeStarted = null
        /** Продолжительность тестирования */
        this.timeDuration = null
        /** Отчет выделенного времени (если тест на время) */
        this.timerCounter = null
    }

    componentDidMount() {
        const local = JSON.parse(localStorage.getItem(STORAGE_NAME)) || {}

        if (local.backDate) {
            this.setState({ backDate: dayjs(local.backDate) })
        }

        const { id } = this.props.match.params;

        (async () => {
            /** Определяем наличие доступа к тесту */
            const response = await axios.get(`/api/test/${id}/access`)
            const data = response.data

            if (!data.access) {
                this.setState({
                    access: false,
                    loading: false,
                })

                return Promise.reject()
            }
        })()
            .then(async () => {
                /** Получаем банк тестовых заданий */
                const response = await axios.get(`/api/test/${id}`)
                const data = response.data

                document.title = `${data.name} - ${APP_NAME}`

                /** Сохраняем информацию о тесте и выбираем текущей вопрос */
                this.test = data
                this.questions = this.test.quests

                /** Засекаем время начала тестирования */
                this.timeStarted = dayjs(new Date())

                /** Если тест на время, готовим отображение отчета */
                this.timerCounter = !this.test.timer ? null : setInterval(() => {
                    const { timer } = this.state

                    if (timer.asSeconds() <= 60) {
                        this.setState({ timer: dayjs.duration(300, 'seconds') })
                    }
                    else {
                        this.setState({ timer: timer.subtract(1, "seconds") })
                    }
                }, 1000)

                this.setState({
                    current: 1,
                    total: this.questions.length,
                    timer: this.test.timer ? dayjs.duration(this.test.timer, 'seconds') : null,
                    values: this.questions[0] ? new Array(this.questions[0].answers.length).fill(0) : [],
                    summary: this.questions.length === 0,
                    loading: false
                })
            })
            .catch(() => {
                this.setState({ loading: false })
            })
    }

    componentWillUnmount = () => {
        if (this.timerCounter) {
            clearInterval(this.timerCounter)
        }
    }

    /**
     * Выбор множественного ответа
     */
    checkChangeHandler = (_, { checked, value }) => {
        let values = [...this.state.values]

        const prime = this.questions[this.state.current - 1].answers.find(elem => elem.prime)

        if (checked && !values.includes(value)) values.push(value)
        else if (!checked) values = values.filter(elem => elem !== value)

        if (prime) values = values.includes(prime.id) ? [value] : values

        this.setState({ values: values })
    }

    /**
     * Выбор одного ответа
     */
    radioChangeHandler = (_, { value }) => {
        this.setState({ values: [value] })
    }

    /**
     * Ответ свободной формы
     */
    inputChangeHandler = (event) => {
        this.setState({ values: [event.target.value] })
    }

    /**
     * Выбор ответа в диапазоне
     */
    rangeChangeHandler = (event, index) => {
        let values = [...this.state.values]

        values[index] = event.target.value

        this.setState({ values: values })
    }

    /**
     * Подтверждение выбранного ответа
     */
    answerClickHandler = _ => {
        let { values } = this.state
        const { current, total } = this.state

        this.testResults = this.testResults.filter(elem => elem.quest_id !== this.currentQuestion.id)

        if (this.currentQuestion.quest_type !== 4) values = values.filter(elem => elem !== 0)

        const type = this.currentQuestion.quest_type

        /** Компоновка ответов для таблицы 'Result' */
        values.forEach((elem, index) => {
            let res = { quest_id: this.currentQuestion.id }

            if (type === 3) {
                res.answer_id = null
                res.value = elem
            }
            else if (type === 4) {
                res.answer_id = this.currentQuestion.answers[index].id
                res.value = elem
            }
            else {
                res.answer_id = elem
                res.value = null
            }

            this.testResults.push(res)
        })

        /** Сохранение и отправка результатов */
        if (current === total) {
            this.completeAndSave()
        }
        /** Переход на следующее тестовое задание */
        else {
            this.setState({
                current: current + 1,
                values: new Array(this.questions[current].answers.length).fill(0)
            })
        }
    }

    backDateCHangeHandler = date => {
        const local = JSON.parse(localStorage.getItem(STORAGE_NAME)) || {}
        local.backDate = date ? date.valueOf() : ''
        localStorage.setItem(STORAGE_NAME, JSON.stringify(local))

        this.setState({ backDate: date })
    }

    render() {
        const { access, summary, completed, loading, backDate } = this.state

        if (loading) {
            return (<PlaceholderContent fluid />)
        }

        if (!access && !loading) {
            return (<HasNoAccess />)
        }

        if (!this.test) {
            return (<Redirect exact to="/tests" />)
        }

        if (summary) return (
            <ExamSummary
                test={this.test}
                duration={this.timeDuration}
                score={this.scoreResult}
                discipline={this.test.discipline} />
        )

        const timer = this.state.timer
        const isSubmit = this.state.values.filter(elem => elem !== 0).length === 0

        this.currentQuestion = this.questions[this.state.current - 1]

        return (
            <>
                <Prompt
                    when={this.testResults.length > 0}
                    message={() => 'Вы уверены, что хотите завершить тестирование? Прогресс не будет сохранен.'} />

                <Grid.Column>
                    <Segment>
                        <Form>
                            <Grid columns={2}>
                                <Grid.Column textAlign="left">
                                    <span>
                                        {this.state.current} из {this.state.total}
                                    </span>
                                </Grid.Column>
                                <Grid.Column textAlign="right">
                                    <span>
                                        {timer ? `${('0' + timer.hours()).slice(-2)}:${('0' + timer.minutes()).slice(-2)}:${('0' + timer.seconds()).slice(-2)}` : null}
                                    </span>
                                </Grid.Column>
                            </Grid>

                            <Divider />

                            <Question
                                quest={this.currentQuestion}
                                values={this.state.values}
                                checkChanged={this.checkChangeHandler}
                                radioChanged={this.radioChangeHandler}
                                inputChanged={this.inputChangeHandler}
                                rangeChanged={this.rangeChangeHandler} />

                            <Divider />

                            <Grid columns={1}>
                                <Grid.Column textAlign="right">
                                    <Form.Button
                                        basic
                                        type="submit"
                                        size="small"
                                        color="blue"
                                        loading={completed}
                                        disabled={isSubmit || completed}
                                        onClick={this.answerClickHandler}
                                    >
                                        Ответить
                                    </Form.Button>
                                </Grid.Column>
                            </Grid>
                        </Form>
                    </Segment>
                </Grid.Column>

                {
                    isGraduate &&
                    <Grid.Column>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <KeyboardDatePicker
                                clearable
                                margin="normal"
                                label="Дата прохождения"
                                format="DD.MM.YYYY"
                                InputProps={{ readOnly: true }}
                                clearLabel="Очистить"
                                cancelLabel="Отмена"
                                value={backDate}
                                onChange={this.backDateCHangeHandler}
                            />
                        </MuiPickersUtilsProvider>
                    </Grid.Column>

                }
            </>
        )
    }

    /** Окончание теста и отправка результатов */
    completeAndSave = () => {
        const difference = dayjs(new Date()).diff(this.timeStarted)
        this.timeDuration = dayjs.duration(difference).asSeconds().toFixed(0)

        this.setState({ completed: true })

        const { backDate } = this.state
        const { id } = this.props.match.params

        axios.post(`/api/test/${id}/complete`, {
            score: this.calculateScore(),
            timer: this.timeDuration,
            results: this.testResults,
            backDate: isGraduate && backDate ? backDate.valueOf() : null,
        }).then(() => {
            this.setState({ summary: true })
        }).catch(() => { })
    }

    /** Расчет балла за тестирование */
    calculateScore = () => {
        if (this.test.option === 0) return null

        let maxScore = 0
        let currentScore = 0

        for (let question of this.questions) {
            if (question.answers.length === 0) continue

            const testResult = this.testResults.filter(elem => elem.quest_id === question.id)

            if (question.quest_type === 0) maxScore += 1
            else maxScore += Math.max(...question.answers.map(elem => elem.value))

            for (let answer of question.answers) {
                if (testResult.some(elem => elem.answer_id === answer.id)) {
                    currentScore += answer.value
                }
            }
        }

        this.scoreResult = (100 * currentScore / maxScore).toFixed(1)

        return this.scoreResult
    }
}

export default Examination
