/**
 * Сообщение о отсутствии доступа к банку заданий
 */
import React from 'react'
import { NavLink } from 'react-router-dom'
import { Grid, Segment, Divider } from 'semantic-ui-react'

const HasNoAccess = _ => (
    <Grid.Column style={{ maxWidth: "750px" }}>
        <Segment>
            <h4>Внимание</h4>
            <span>У вас нет доступа к прохождению данного теста</span>
            <Divider />
            <NavLink to="/tests" className="ui very basic small button">К тестам</NavLink>
        </Segment>
    </Grid.Column>
)

export default HasNoAccess
