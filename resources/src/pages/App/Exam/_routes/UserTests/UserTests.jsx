/**
 * Список доступных тестов для прохождения
 */
import React from 'react'
import { connect } from 'react-redux'
import { Grid } from 'semantic-ui-react'
import axios from 'utils/axios.jsx'
import dayjs from 'dayjs'

import { APP_NAME } from 'utils/auth.jsx'
import store, { TestsTypes } from 'store/store.jsx'

import NoExam from './NoExam/NoExam.jsx'
import UserTest from './UserTest/UserTest.jsx'

import PlaceholderContent from 'app/_components/PlaceholderContent/PlaceholderContent.jsx'

const STORAGE_NAME = 'tests'

class UserTests extends React.Component {
    state = {
        loading: false
    }

    componentDidMount() {
        document.title = `Тесты - ${APP_NAME}`

        const { tests } = this.props.tests

        const ls = JSON.parse(localStorage.getItem(STORAGE_NAME)) || {}
        const lastUpdate = ls.last_update ? dayjs(ls.last_update) : dayjs()

        if (!tests.loaded || dayjs().diff(lastUpdate, 'hours') > 0) {
            this.setState({ loading: true })

            ls.last_update = dayjs().valueOf()
            localStorage.setItem(STORAGE_NAME, JSON.stringify(ls))

            axios.get('/api/tests-to-pass')
                .then(({ data }) => {
                    this.setState({ loading: false })

                    store.dispatch(TestsTypes.tests(data))
                })
                .catch(() => {
                    this.setState({ loading: false })
                })
        }
    }

    render() {
        const { loading } = this.state
        const { tests } = this.props.tests

        if (!tests.loaded || loading) {
            return (<PlaceholderContent fluid />)
        }

        if (!tests.data.length) {
            return (<NoExam />)
        }

        return (
            <Grid.Column>
                {tests.data.map((elem, index) => (
                    <UserTest key={index} test={elem} />
                ))}
            </Grid.Column>
        )
    }
}

const mapStateToProps = state => ({ tests: state.tests })

export default connect(mapStateToProps)(UserTests)
