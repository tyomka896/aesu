/**
 * Разработка и тестирование
 */
import React from 'react'
import ReactDOM from 'react-dom'
import axios from 'utils/axios.jsx'

class DevelopForm extends React.Component {
    clickHandler = () => {
        const params = { name: 'tyomka896' }
        const file = new Blob(['Hello, Blob!'], { type: 'plain/text' })
        const formData = new FormData()

        formData.append('zapros', JSON.stringify(params))
        formData.append('file', file)

        axios.post('/api/study/flm-builder', formData)
            .then(({ data }) => {
                console.log('$ data', data)
            })
            .catch((error) => {
                if (error.response.status === 422) {
                    console.log('$ Request failed', error.response.data)
                }
            })

    }

    render() {
        return (
            <button
                style={{ margin: 16 }}
                onClick={this.clickHandler}
            >
                Click me
            </button>
        )
    }
}

ReactDOM.render(<DevelopForm />, document.getElementById('root'))
