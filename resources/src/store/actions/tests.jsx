export const TESTS_TO_PASS = 'TESTS_TO_PASS'

export const tests = (data, loaded = true) => ({
    type: TESTS_TO_PASS,
    data: {
        data: data || [],
        loaded: Boolean(loaded),
    },
})
