/**
 * Global axios configuration
 */
import axios from 'axios'

import store, { MessageTypes } from 'store/store.jsx'

const instance = axios.create({
    withXSRFToken: true,
})

instance.interceptors.response.use(null, error => {
    let message = null

    if (error.response) {
        const { data, status, statusText } = error.response

        if (status !== 422) {
            message = `${status} ${statusText}`
            if (data.message) message += `: ${data.message}`
        }

        console.log('$ Response error:', error.response)
    } else if (error.request) {
        message = `Не удачный запрос: ${error.request.responseURL}`

        console.log('$ Response not received:', error.request)
    } else {
        message = `Ошибка в запросе: ${error.request.responseURL}`

        console.log('$ Settings error:', error.message)
    }

    if (message) {
        store.dispatch(MessageTypes.box({ format: 'negative', message }))
    }

    return Promise.reject(error)
})

export default instance
