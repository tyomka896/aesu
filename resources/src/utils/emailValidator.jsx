/**
 * --------------------------------------------------------------------------
 * Валидация email-адреса
 * --------------------------------------------------------------------------
 */

let tester = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/

export default function(email) {
    if (!email) return false

    if(email.length > 254) return false

    let valid = tester.test(email)

    if(!valid) return false

    // Further checking of some things regex can't handle
    let parts = email.split('@')

    if(parts[0].length > 64) return false

    let domainParts = parts[1].split('.')
    let hasExceededParts = domainParts.some((part) => part.length > 63)

    if(hasExceededParts) return false

    return true
}
