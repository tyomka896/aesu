/**
 * --------------------------------------------------------------------------
 * Получение параметров текущего GET-запроса
 * --------------------------------------------------------------------------
 */

export default function() {
    let obj = {}

    const wls = window.location.search

    if (!wls) {
        return obj
    }

    const params = wls.slice(1).split('&')

    for (let param of params) {
        let parts = param.split('=')

        obj[parts[0]] = parts[1] || null
    }

    return obj
}
