/**
 * --------------------------------------------------------------------------
 * Глобальный контейнер по управлению и
 * контролю данных о текущем пользователе.
 * --------------------------------------------------------------------------
 */
import axios from "utils/axios.jsx"

/** Текущий пользователь */
let _user = null

/** Валидация проверяемых прав */
let validatePermissions = function (obj) {
    if (typeof obj === "string" && obj.trim()) {
        return [obj.trim()]
    } else if (Array.isArray(obj)) {
        return obj
    } else {
        throw new Error("Parameter 'obj' is not valid")
    }
}

/**
 * Пользователь
 */
class User {
    /**
     * Инициализация пользователя
     *
     * @param {*} [args] - модель пользователя
     */
    init() {
        let obj = arguments[0] || {}

        if (typeof obj !== "object") return this

        this.id = obj.id || null
        this.surname = obj.surname || null
        this.name = obj.name || null
        this.patronym = obj.patronym || null
        this.login = obj.login || null
        this.email = obj.email || null
        this.created_at = obj.created_at || null
        this.updated_at = obj.updated_at || null
        this.last_visit = obj.last_visit || null
        this.userstatus = obj.userstatus || null
        this.branch = obj.branch || null
        this.permissions = obj.permissions || null
        this._csrf_token = obj._csrf_token || null

        return this
    }

    /**
     * Авторизация пользователя
     */
    login() {
        const userData = document.getElementById("user-data")

        if (userData) {
            const user = JSON.parse(userData.textContent)

            this.init(user)

            return Promise.resolve(user)
        } else {
            return axios.get("/auth")
                .then((response) => {
                    this.init(response.data)

                    return Promise.resolve(response.data)
                })
                .catch((error) => {
                    return Promise.reject(error)
                })
        }
    }

    /**
     * Если гость
     */
    guest() {
        return this._csrf_token !== TOKEN
    }

    /**
     * Если авторизован
     */
    check() {
        return !this.guest()
    }

    /**
     * Имеет все права
     *
     * @param {Number | Object} [obj] - список проверяемых прав
     */
    can(obj) {
        if (this.guest()) {
            return false
        }

        let flag = false
        let ps = this.permissions
        let vals = validatePermissions(obj)

        for (let val of vals) {
            flag = false

            for (let p of ps) {
                if (val === p.name) {
                    flag = true
                }
            }

            if (!flag) {
                return false
            }
        }

        return true
    }

    /**
     * Имеет одно из правил
     *
     * @param {Number | Object} [obj] - список проверяемых прав
     */
    canAny(obj) {
        if (this.guest()) {
            return false
        }

        let ps = this.permissions
        let vals = validatePermissions(obj)

        for (let val of vals) {
            for (let p of ps) {
                if (val === p.name) {
                    return true
                }
            }
        }

        return false
    }

    /**
     * Обновление поля имени
     * async request to update 'name' field
     */
    name() {
        throw new Error("Not implemented!")
    }
}

/**
 * Наименование приложения
 */
export const APP_NAME = import.meta.env.VITE_APP_NAME || "AESFU"

/**
 * Токен текущей сессии
 */
export const TOKEN = document.querySelector("meta[name=csrf-token]")
    .getAttribute("content")

/**
 * Основной метод взаимодействия с пользователем
 */
export default function auth() {
    if (_user === null) {
        _user = new User()
    }

    return _user
}
