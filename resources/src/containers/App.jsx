/**
 * Обертка основного приложения
 */
import React from "react"
import { BrowserRouter } from "react-router-dom"
import { Provider } from "react-redux"
import { Container, Menu } from "semantic-ui-react"
import axios from "utils/axios.jsx"

import store from "store/store.jsx"
import auth, { APP_NAME } from "utils/auth.jsx"

import Navbar from "app/_components/Navbar/Navbar.jsx"
import MessageBox from "app/_components/MessageBox/MessageBox.jsx"

/** Дополнительные модули для совместимости */
import "core-js/features/array"
import "core-js/features/symbol"
import "core-js/features/string"
import "core-js/features/object"
import "core-js/features/promise"

/** Обработка ошибок JavaScrip */
const onErrorHandler = (event) => {
    axios.post("/api/react/error", {
        url: event.currentTarget.location.href,
        message: event.message,
        trace: event.error.stack || "NO STACKTRACE",
    })
        .then(({ data }) => {
            if (data.result) {
                console.log("$ Error posted!")
            }
        })
        .catch(() => { })
}

class App extends React.Component {
    state = {
        loading: true,
        hasError: false,
    };

    componentDidMount() {
        window.addEventListener("error", onErrorHandler)

        /** Авторизация пользователя */
        auth().login()
            .then(() => this.setState({ loading: false }))
            .catch((_) => this.setState({ hasError: true }))
    }

    componentWillUnmount() {
        window.removeEventListener("error", onErrorHandler)
    }

    componentDidCatch() {
        this.setState({ hasError: true })
    }

    render() {
        const { loading, hasError } = this.state

        if (hasError) {
            return (
                <div
                    style={{
                        marginLeft: "auto",
                        marginRight: "auto",
                        padding: "16px",
                        textAlign: "center",
                    }}
                >
                    <h3>Что-то пошло не так</h3>
                    <p>Об ошибке уже узнали и ее скоро постараются исправить</p>
                    <a href="#reload" style={{ textDecoration: "underline" }}>
                        ДОМОЙ
                    </a>
                </div>
            )
        }

        if (loading) {
            return (
                <Menu inverted borderless size="large" className="fixed">
                    <Menu.Item header>{APP_NAME}</Menu.Item>
                </Menu>
            )
        }

        const Component = this.props.component

        return (
            <Provider store={store}>
                <BrowserRouter>
                    <Navbar />

                    <Container>
                        <MessageBox />

                        <Component />
                    </Container>
                </BrowserRouter>
            </Provider>
        )
    }
}

export default App
