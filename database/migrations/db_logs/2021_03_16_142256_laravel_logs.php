<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class LaravelLogs extends Migration
{
    /**
     * Наименование файла базы данных
     */
    private $fileName = 'laravel.sqlite';

    /**
     * Наименование базы данны для соединения
     */
    protected $connection = 'logs';
    
    /**
     * Запуск миграции
     */
    public function up()
    {
        /**
         * Ошибки в приложении Laravel
         */
        Schema::create('laravel', function(Blueprint $table){
            $table->increments('id');
            $table->string('url', 250);
            $table->string('file', 250);
            $table->integer('line');
            $table->string('message', 250);
            $table->text('trace')->nullable();
            $table->integer('count')->default(1);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });

        /**
         * Ошибки в среде ReactJS
         */
        Schema::create('react', function(Blueprint $table){
            $table->increments('id');
            $table->string('url', 250);
            $table->string('message', 250);
            $table->text('trace')->nullable();
            $table->integer('count')->default(1);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Отмена миграции
     */
    public function down()
    {
        Schema::drop('laravel');
        Schema::drop('react');
    }

    // 2021_03_16_142256_
}
