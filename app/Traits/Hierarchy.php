<?php

namespace App\Traits;

trait Hierarchy
{
    /*
    |--------------------------------------------------------------------------
    | Model hierarchy trait
    |--------------------------------------------------------------------------
    */

    /**
     * Get the parent associated with the model.
     */
    public function parent()
    {
        return $this->hasOne($this::class, 'id', 'parent_id')
            ->orderBy('name');
    }
    /**
     * Get the parent associated with the model.
     * optimized size for rootpath
     */
    public function shortParent()
    {
        return $this->hasOne($this::class, 'id', 'parent_id')
            ->orderBy('name')
            ->select('name','id','parent_id');
    }

    /**
     * Get the children for the model.
     */
    public function children()
    {
        return $this->hasMany($this::class, 'parent_id', 'id')
            ->orderBy('order');
    }

    /**
     * Get children of the model.
     *
     * @param array args
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function childrenDown($args)
    {
        $args = array_merge([
            'level' => 1,
            'trashed' => false,
            'with' => null,
            'edelement' => false,
            'summary' => false,
        ], $args);

        $level =  intval($args['level']) ? $args['level'] : 0;

        if ($level === 0) return $this;

        $children = $this->children->loadCount('children');

        // if (boolval($args['trashed'])) {
        //     $children->withTrashed();
        // }

        if (isset($args['with'])) {
            $children->load($args['with']);
        }
        
        if ($args['edelement']){
            $children->load('didacticdescription')
            ->load('connections')
            ->load('methods')
            ->load('tasks')
            ->load('quests')
            ->load('edelementtype')
            ->load('competences');
        }

        if ($args['summary']){
            $children->load('didacticdescription')
            ->load('connections2')
            ->load('quests')
            ->load('competences')
            ->load('edelementtype');
        }

        foreach ($children as $child) {
            if ($child->children_count === 0) continue;

            $args['level'] = $level - 1;

            $child->childrenDown($args);
        }
        
        return $this;
    }

    /**
     * children count
     */

     public function childrenCount()
     {
        $children = $this->loadCount('children');
        return $this;
     }

    /**
     * Get the root associated with the model.
     */
    public function root()
    {
        if ($this->parent_id === null) {
            return $this;
        }
        return $this->parent->root();
    }
    /**
     * Get the path to the root in $up steps
     */

    public function rootPath()
    {
        $up = 5;
        $this->load([
            implode('.', array_fill(0, $up, 'shortParent'))
        ]);
        return $this;

    }

    /**
     * Get the root's children associated with the model.
     */
    public function rootChildren()
    {
        $root = $this->root();

        return $root->children;
    }
}
