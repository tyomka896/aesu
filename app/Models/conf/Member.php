<?php

namespace App\Models\conf;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = null;

    protected $connection = 'conf';
    protected $table = 'member';
    public $timestamps = true;

    protected $casts = [
        'birthday' => 'datetime',
        'arrival' => 'datetime',
        'created_at' => 'datetime',
    ];

    protected $fillable = [
        'surname',
        'name',
        'patronym',
        'status',
        'degree',
        'organization',
        'city',
        'email',
        'format',
        'pass',
        'birthday',
        'birthplace',
        'registered',
        'work',
        'position',
        'route',
        'arrival',
        'hotel',
        'created_at',
    ];

    protected $hidden = [

    ];

    /**
     * Связь Многие к Многие с Article
     */
    public function articles()
    {
        return $this->belongsToMany(Article::class, 'memberlinkarticle', 'member_id', 'article_id');
    }

    /**
     * Полное имя участника конференции
     */
    public function name()
    {
        return $this->surname.' '.$this->name.' '.$this->patronym;
    }
}
