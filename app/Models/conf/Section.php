<?php

namespace App\Models\conf;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $connection = 'conf';
    protected $table = 'section';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'example',
        'order',
        'active',
    ];

    protected $hidden = [

    ];
}
