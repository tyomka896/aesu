<?php

namespace App\Models\conf;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    const CREATED_AT = 'last_modified';
    const UPDATED_AT = null;

    protected $connection = 'conf';
    protected $table = 'document';

    protected $fillable = [
        'name',
        'extension',
        'path',
        'size',
        'article_id',
        'last_modified',
    ];

    protected $hidden = [

    ];

    protected $casts = [
        'last_modified' => 'datetime',
    ];

    /**
     * Связь Многие к Одному с Article
     */
    public function article()
    {
        return $this->hasOne(Article::class, 'article_id');
    }
}
