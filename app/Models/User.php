<?php

namespace App\Models;

use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Sanctum\NewAccessToken;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasRoles, Notifiable;

    public $timestamps = true;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'user';

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'last_visit' => 'datetime',
    ];

    protected $fillable = [
        'surname',
        'name',
        'patronym',
        'login',
        'email',
        'password',
        'last_visit',
    ];

    protected $hidden = [
        'password',
        'status_id',
        'branch_id',
        'remember_token',
    ];

    /**
     * Связь Многие к Одному с Branch
     */
    public function branch()
    {
        return $this->belongsTo(\App\Models\mddb\Branch::class, 'branch_id');
    }

    /**
     * Связь Многие к Многие с Subgroup
     */
    public function subgroups()
    {
        return $this->belongsToMany(\App\Models\mddb\Subgroup::class, 'userlinksubgroup', 'user_id', 'subgroup_id');
    }

    /**
     * Связь Один к Многие с Basket
     */
    public function baskets()
    {
        return $this->hasMany(\App\Models\mddb\Basket::class, 'user_id');
    }

    /**
     * Связь Один к Многие с Test
     */
    public function tests()
    {
        return $this->hasMany(\App\Models\mddb\Test::class, 'owner');
    }

    /**
     * Связь Один к Многие с Discipline
     */
    public function disciplines()
    {
        return $this->hasMany(\App\Models\mddb\Discipline::class, 'owner');
    }

    /**
     * Связь Многие к Одному с Userstatus
     */
    public function userstatus()
    {
        return $this->belongsTo(\App\Models\mddb\Userstatus::class, 'status_id');
    }

    /**
     * Связь Один к Многие с UserToken
     */
    public function tokens()
    {
        return $this->hasMany(UserToken::class, 'user_id');
    }

    /**
     * Полное имя пользователя
     */
    public function name()
    {
        return $this->surname.' '.$this->name.' '.$this->patronym;
    }

    /**
     * Создать новый персональный токен доступа для пользователя
     */
    public function createToken(string $name, array $abilities = ['*'])
    {
        $token = $this->tokens()->create([
            'name' => $name,
            'token' => hash('sha256', $plainTextToken = Str::random(40)),
            'abilities' => $abilities,
        ]);

        return new NewAccessToken($token, $token->getKey().'|'.$plainTextToken);
    }
}
