<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Subgroup extends Model
{
    protected $table = 'subgroup'; 
    public $timestamps = false;

    protected $fillable = [
        'name',
        'portrait',
        'owner',
    ];

    protected $hidden = [

    ];

    /**
     * Связь Многие к Многие с User
     */
    public function users()
    {
        return $this->belongsToMany(\App\Models\User::class, 'userlinksubgroup', 'subgroup_id', 'user_id');
    }

    /**
     * Связь Многие к Многие с Test
     */
    public function tests()
    {
        return $this->belongsToMany(Test::class, 'subgrouplinktest', 'subgroup_id', 'test_id');
    }

    /**
     * Связь Многие к Одному с User
     */
    public function userowner()
    {
        return $this->belongsTo(\App\Models\User::class, 'owner');
    }
}
