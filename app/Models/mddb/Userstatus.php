<?php

namespace App\Models\mddb;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Userstatus extends Authenticatable
{
    protected $table = 'userstatus';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'order',
    ];

    protected $hidden = [

    ];

    /**
     * Связь Один к Многие с User
     */
    public function users()
    {
        return $this->hasMany(\App\Models\User::class, 'status_id');
    }
}
