<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Quest extends Model
{
    protected $table = 'quest'; 
    public $timestamps = false;

    protected $fillable = [
        'name',
        'position',
        'type',
    ];

    protected $hidden = [

    ];

    /**
     * Связь Многие к Многие с Test
     */
    public function tests()
    {
        return $this->belongsToMany(Test::class, 'testlinkquest', 'quest_id', 'test_id')->withPivot('position');
    }

    /**
     * Связь Многие к Многие с Competence
     */
    public function competences()
    {
        return $this->belongsToMany(Competence::class, 'questlinkcomp', 'quest_id', 'competence_id');
    }

    /**
     * Связь Один к Многие с Answer
     */
    public function answers()
    {
        return $this->hasMany(Answer::class, 'quest_id');
    }

    /**
     * Связь Многие к Одному с Questtype
     */
    public function questtype()
    {
        return $this->belongsTo(Questtype::class, 'type');
    }

    /**
     * Связь многие ко многим с EdElement
     */
    public function edelements()
    {
        return $this->belongsToMany(EdElement::class, 'edelementlinkquest', 'quest_id', 'edelement_id');
    }
}
