<?php

namespace App\Models\mddb;

use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUlids;

class Document extends Model
{
    use HasUlids;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'document';

    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The data type of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'name',
        'type',
        'size',
        'path',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['path'];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'user_id' => null,
    ];

    /**
     * Create document from UploadedFile instance
     * 
     * @param UploadedFile
     * @param string
     * @return $this
     */
    public static function createFromFile(UploadedFile $file, string $folder = '')
    {
        $uniqueId = strtolower((string) Str::ulid());

        $storedPath = $file->storeAs($folder . '/' . Carbon::now()->format('Y/m'), $uniqueId);

        $document = Document::create([
            'id' => $uniqueId,
            'name' => $file->getClientOriginalName(),
            'type' => $file->getMimeType(),
            'size' => $file->getSize(),
            'path' => $storedPath,
        ]);

        return $document;
    }
}
