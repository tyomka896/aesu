<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Admatrix extends Model
{
    protected $table = 'admatrix';
    protected $primaryKey = 'answer_id';
    public $timestamps = false;

    protected $fillable = [
        'answer_id',
        'competence_id',
        'value',
    ];

    protected $hidden = [

    ];

    /**
     * Связь Многие к Одному с Answer
     */
    public function answer()
    {
        return $this->belongsTo(Answer::class, 'answer_id');
    }

    /**
     * Связь Многие к Одному с Competence
     */
    public function competence()
    {
        return $this->belongsTo(Competence::class, 'competence_id');
    }
}
