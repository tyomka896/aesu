<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'task'; 
    public $timestamps = false;

    protected $fillable = [
        'name',
        'discipline_id',
    ];

    protected $hidden = [
        'pivot'
    ];

    /**
     * Связь Многие к Многие с Discipline
     */
    public function disciplines()
    {
        return $this->belongsToMany(Discipline::class, 'disclinktask', 'task_id', 'discipline_id')->withPivot('position');
    }
}
