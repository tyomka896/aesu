<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class FileStorage extends Model
{
    protected $table = 'filestorage';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'file',
        'path',
    ];

    protected $hidden = [];

}