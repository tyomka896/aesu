<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $table = 'result'; 
    public $timestamps = false;

    protected $fillable = [
        'basket_id',
        'quest_id',
        'answer_id',
        'value',
    ];

    protected $hidden = [

    ];

    /**
     * Связь Многие к Одному с Basket
     */
    public function basket()
    {
        return $this->belongsTo(Basket::class, 'basket_id');
    }

    /**
     * Связь Многие к Одному с Quest
     */
    public function quest()
    {
        return $this->belongsTo(Quest::class, 'quest_id');
    }

    /**
     * Связь Многие к Одному с Answer
     */
    public function answer()
    {
        return $this->belongsTo(Answer::class, 'answer_id');
    }
}
