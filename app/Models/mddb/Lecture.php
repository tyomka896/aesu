<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'lecture';

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'order' => 0,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'order',
        'name',
        'slug',
        'document_id',
        'discipline_id',
    ];

    /**
     * Get the user that owns the lecture.
     */
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    /**
     * Get the document associated with the lecture.
     */
    public function document()
    {
        return $this->hasOne(Document::class, 'id', 'document_id');
    }

    /**
     * Get the discipline that owns the lecture.
     */
    public function discipline()
    {
        return $this->belongsTo(Discipline::class, 'discipline_id', 'id');
    }
}
