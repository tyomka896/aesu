<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class DidacticCore extends Model
{
    protected $table = 'core';
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    protected $hidden = [];

}