<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Didactic extends Model
{
    protected $table = 'didactic'; 
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    protected $hidden = [

    ];
}
