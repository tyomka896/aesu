<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Method extends Model
{
    protected $table = 'method'; 
    public $timestamps = false;

    protected $fillable = [
        'name',
        'discipline_id',
    ];

    protected $hidden = [
        'pivot'
    ];

    /**
     * Связь Многие к Многие с Discipline
     */
    public function disciplines()
    {
        return $this->belongsToMany(Discipline::class, 'disclinkmethod', 'method_id', 'discipline_id')->withPivot('position');
    }
}
