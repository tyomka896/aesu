<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class DidacticDescription extends Model
{
    protected $table = 'didacticdescript';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'type_id',
        'importance',
        'difficulty',
        'core_id',
        'viewtype',
        'packnumber',
        'norms',
        'profile',
        'objnumber',
        'filestorage_id',
        'test_type',
    ];

    protected $hidden = [];

    public function filestorage(){
        return $this->hasOne(FileStorage::class,'id','filestorage_id');
    }
}