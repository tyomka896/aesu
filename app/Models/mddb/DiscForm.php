<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class DiscForm extends Model
{
    protected $table = 'discform';
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    protected $hidden = [];

}