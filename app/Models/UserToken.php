<?php

namespace App\Models;

use Laravel\Sanctum\PersonalAccessToken;

class UserToken extends PersonalAccessToken
{
    protected $table = 'user_token';

    protected $fillable = [
        'name',
        'token',
    ];

    protected $casts = [
        'last_used_at' => 'datetime',
    ];

    /**
     * Получить пользователя, к которому принадлежит токен
     */
    public function tokenable()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}