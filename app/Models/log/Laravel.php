<?php

namespace App\Models\log;

use Illuminate\Database\Eloquent\Model;

class Laravel extends Model
{
    public $timestamps = true;
    protected $table = 'laravel';
    protected $connection = 'logs';

    protected $fillable = [
        'url',
        'file',
        'line',
        'message',
        'trace',
        'count',
    ];

    protected $casts = [
        'line' => 'integer',
        'count' => 'integer',
    ];
}
