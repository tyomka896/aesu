<?php

namespace App\Models\log;

use Illuminate\Database\Eloquent\Model;

class React extends Model
{
    public $timestamps = true;
    protected $table = 'react';
    protected $connection = 'logs';

    protected $fillable = [
        'url',
        'message',
        'trace',
        'count',
    ];

    protected $casts = [
        'count' => 'integer',
    ];
}
