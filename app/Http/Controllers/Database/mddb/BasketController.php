<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\mddb\Basket;
use App\Models\mddb\Result;
use App\Models\mddb\Test;
use App\Http\Requests\ModelsRequest;

class BasketController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Результат пройденного тестирования
    |--------------------------------------------------------------------------
    */

    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }

    /**
     * Получить результат прохождения теста
     *
     * GET /api/basket/{id}
     */
    public function basket($id)
    {
        $basket = Basket::find($id);

        if (!$basket) {
            return $this->toJson(['message' => 'Результат прохождения теста не найден'], 422);
        }

        $basket->load(['test', 'user'])
            ->setHidden(['test_id', 'user_id']);

        return $this->toJson($basket);
    }

    /**
     * Результат прохождения теста в формате HTML
     *
     * GET /api/basket/{id}/result-html
     */
    public function resultHTML($id)
    {
        $basket = Basket::find($id);

        if (!$basket) {
            return $this->toJson(['message' => 'Результат прохождения теста не найден'], 422);
        }

        $html = $basket->toHTML();

        return view('other.components.basket-result', [
            'quests' => $html,
            'basket' => $basket,
            'passedAt' => $this->reformatDate($basket->passed_at, 'd.m.Y H:i:s'),
        ]);
    }

    /**
     * Подсчет количественной оценки прохождения теста
     *
     * GET /api/basket/{id}/score
     */
    public function score($id)
    {
        $basket = Basket::find($id);

        if (!$basket)
            return $this->toJson(['message' => 'Результат прохождения теста не найден'], 422);

        $score = $basket->updateScore();

        return $this->toJson(['score' => $score]);
    }

    /**
     * Получение списка пройденных тестов
     *
     * POST /api/baskets
     */
    public function baskets(Request $request)
    {
        $baskets = Basket::query();

        if ($request->has('day')) {
            $baskets->whereDay('passed_at', '=', $request->day);
        }
        if ($request->has('month')) {
            $baskets->whereMonth('passed_at', '=', $request->month);
        }
        if ($request->has('year')) {
            $baskets->whereYear('passed_at', '=', $request->year);
        }

        if ($request->has('sort')) {
            if ($request->sort === 'pa') {
                $baskets->orderBy('passed_at');
            } else if ($request->sort === 'pd') {
                $baskets->orderByDesc('passed_at');
            }
        }

        $baskets = $baskets->with([
            'test',
            'user',
        ])
            ->get();

        $baskets = $this->modelsPaginator($baskets, $request->perPage, $request->pageCount);

        $baskets->each(function ($elem) {
            $elem->test
                ->setHidden([
                    'test_id',
                    'user_id'
                ]);
        });

        return $this->toJson($baskets);
    }

    /**
     * Получение списка пройденных тестов для CMKD
     *
     * POST /api/baskets/models
     *
     * @param ModelsRequest
     */
    public function models(ModelsRequest $request)
    {
        $models = Basket::whereHas('test', function ($query) use ($request) {
            $query->where('discipline_id', $request->discipline_id)->where('option', 1);
        })
            ->whereHas('user', function ($query) use ($request) {
                $query->where('branch_id', $request->branch_id);
            })
            ->with([
                'user' => function ($query) {
                    $query->select('id', 'surname', 'name', 'patronym');
                }
            ])
            ->with([
                'test' => function ($query) {
                    $query->select('id', 'name');
                }
            ]);

        $models = $this->paginator(
            $models,
            $request->perPage,
            $request->pageCount,
        );

        return $this->toJson($models);
    }

    /**
     * Получение списка пройденных тестов для одного пользователя на момент времени
     *
     * POST /api/baskets/user
     */
    public function userBaskets(ModelsRequest $request)
    {
        $user = User::find($request->user_id);

        $models = Basket::where('user_id', $request->user_id)
            ->whereHas('test', function ($query) use ($request) {
                $query->where('option', 1);
            })
            ->whereDate('passed_at', '<=', $request->date)
            ->orderByDesc('passed_at')
            ->with(['test' => ['discipline']])
            ->get()->unique('test_id');

        $results = collect();

        foreach ($models as $model) {
            $test = Test::find($model->test_id);

            if (!$test) {
                return $this->toJson(['message' => 'Тест для результата прохождения теста не найден'], 422);
            }

            if (!$test->discipline) {
                return $this->toJson(['message' => 'Дисциплина для результата прохождения теста не найдена'], 422);
            }

            $edelement = $test->discipline->edelements->where('edelementtype_id', 3)->where('branch_id', $user->branch_id);

            if (count($edelement) !== 0) {
                $results->push($model);
            }
        }

        return $this->toJson($results);
    }

    /**
     * Результаты пройденного теста для частной CMKD
     *
     * POST /api/baskets/result
     */
    public function result(ModelsRequest $request)
    {
        $models = Result::when($request->basket_id, function ($query, $value) {
            $query->where('basket_id', $value);
        })
            ->whereHas('answer', function ($query) {
                $query->where('prime', null);
            })
            ->with('answer')
            ->with(['quest:id' => ['edelements:id'],])
            ->get();

        return $this->toJson($models);
    }
    /**
     * Результаты пройденной анкеты для режима личных предпочтений на частной CMKD
     *
     * POST /api/baskets/form-preference/result
     */
    public function formresult(ModelsRequest $request)
    {
        $models = Result::when($request->basket_id, function ($query, $value) {
            $query->where('basket_id', $value);
        })
            ->whereHas('answer', function ($query) {
                $query->where('prime', null);
            })
            ->with(['answer' => ['personal'],])
            ->get();

        return $this->toJson($models);
    }

    /**
     * Результаты пройденных тестов для сводной CMKD
     *
     * POST /api/baskets/results
     */
    public function results(ModelsRequest $request)
    {
        $user = User::find($request->user_id);

        $basketItems = Basket::where('user_id', $request->user_id)
            ->whereHas('test', function ($query) use ($request) {
                $query->where('option', 1);
            })
            ->whereDate('passed_at', '<=', $request->date)
            ->orderByDesc('passed_at')
            ->select('id', 'test_id')
            ->get()
            ->unique('test_id');

        $results = collect();

        foreach ($basketItems as $basketItem) {
            $test = Test::find($basketItem->test_id);

            if (!$test) {
                return $this->toJson(['message' => 'Тест для результата прохождения теста не найден'], 422);
            }

            if (!$test->discipline) {
                return $this->toJson(['message' => 'Дисциплина для результата прохождения теста не найдена'], 422);
            }

            $edelement = $test->discipline->edelements->where('edelementtype_id', 3)->where('branch_id', $user->branch_id);

            if (!$edelement->isEmpty()) {
                $result = Result::where('basket_id', $basketItem->id)
                    ->whereHas('answer', function ($query) {
                        $query->where('prime', null);
                    })
                    ->with('answer')
                    ->with(['quest:id' => ['edelements:id'],])
                    ->get();

                $results->push($result);
            }
        }

        return $this->toJson($results->collapse());
    }

    /**
     * Результаты пройденных тестов для сводной CMKD
     *
     * POST /api/baskets/form-preference/results
     */
    public function formresults(ModelsRequest $request)
    {
        $user = User::find($request->user_id);

        $basketItems = Basket::where('user_id', $request->user_id)
            ->whereHas('test', function ($query) use ($request) {
                $query->where('option', 0);
            })
            ->whereDate('passed_at', '<=', $request->date)
            ->orderByDesc('passed_at')
            ->select('id', 'test_id')
            ->get()
            ->unique('test_id');

        $results = collect();

        foreach ($basketItems as $basketItem) {
            $test = Test::find($basketItem->test_id);

            if (!$test) {
                return $this->toJson(['message' => 'Тест для результата прохождения теста не найден'], 422);
            }

            if ($test->discipline) {
                $edelement = $test->discipline->edelements->where('edelementtype_id', 3)->where('branch_id', $user->branch_id);

                if (!$edelement->isEmpty()) {
                    $result = Result::where('basket_id', $basketItem->id)
                        ->whereHas('answer', function ($query) {
                            $query->where('prime', null);
                        })
                        ->with(['answer' => ['personal' => ['edelement']],])
                        ->get();

                    $results->push($result);
                }
            }
        }

        return $this->toJson($results->collapse());
    }


    /**
     * Получение списка пройденных укороченных тестов по дисциплине для одного пользователя
     *
     * POST /api/baskets/results/united
     */
    public function unitedResults(ModelsRequest $request)
    {
        $user_id = $request->user_id;
        if (!$user_id)
        $user_id = \Auth::user()->id;
        $models = Basket::where('user_id', $user_id)
            ->whereHas('test', function ($query) use ($request) {
                $query->where('option', 1);
            })
            ->where('edelement_position', '<=', $request->position)
            ->orderBy('passed_at', 'desc')
            ->get()
        ;

        
        $m = collect();
        foreach ($models as $model) {
            $res = collect();
            $results = Result::when($model->id, function ($query, $value) {
                $query->where('basket_id', $value);
            })
            ->whereHas('answer', function ($query) {
                $query->where('prime', null);
            })
            ->with('answer')
            ->with(['quest:id' => ['edelements:id'],])
            ->get();

            $res->push($results);
            $model->setAttribute('results', $res->collapse());
            $m->push($model);
        }

        return $this->toJson($m);
    }
}

    
