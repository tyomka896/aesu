<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\UserEducationProfile as Requests;
use App\Models\mddb\UserEducationProfile;
use App\Models\mddb\EducationCourse;
use App\Http\Requests\ModelsRequest;

class UserEducationProfileController extends Controller
{
    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }

    // Получение информации об образовательном профиле пользователя
    // [ GET /api/user/profile/{id} ]
    public function usereducationprofile($id)
    {
        $profile = UserEducationProfile::find($id);
        if (!$profile)
            return $this->toJson(['message' => 'Профиль не существует'], 404);
        $profile = UserEducationProfile::find($id)
            ->load('usereducationcourses');

        return $this->toJson($profile);
    }


    // Получение информации об образовательном профиле пользователя
    // [ GET /api/user/authprofile ]
    public function authusereducationprofile()
    {
        $user_id = \Auth::user()->id;

        $profile = UserEducationProfile::when($user_id, function ($query, $value) {
            $query->where('user_id', $value);
        })
            ->with('usereducationcourses')
            ->first();

        if (!$profile) {
            return $this->toJson(['message' => 'Профиль не существует'], 404);
        }

        return $this->toJson($profile);
    }


    // Получение информации о длинном (>1) участке цифрового следа образовательного профиля
    // [ GET /api/user/profile/{id}/dfp ]
    // весь либо в периоде
    public function userdigitalfootprints($id)
    {
        $profile = UserEducationProfile::find($id);
        if (!$profile)
            return $this->toJson(['message' => 'Профиль не существует'], 404);

        $profile = UserEducationProfile::find($id)
            ->load('userdigitalfootprints');

        return $this->toJson($profile);
    }

    // Создание образовательного профиля пользователя
    // [ POST /api/user/profile/create ]
    public function create(Requests\CreateRequest $request)
    {
        $model = UserEducationProfile::create($request->validated());
        return $this->toJson($model->fresh());
    }

    // Изменение образовательного профиля пользователя
    // [ PUT /api/user/profile/{id} ]
    public function update(Requests\UpdateRequest $request, $id)
    {
        $model = UserEducationProfile::find($id)
            ->update($request->validated());

        return $this->toJson(true);
    }

    // Удаление образовательного профиля пользователя
    // [ DELETE /api/user/profile/{id} ]
    public function delete($id)
    {
        Validator::validate(['id' => $id], ['id' => 'exists:usereducationprofile']);

        UserEducationProfile::find($id)->delete();

        return $this->toJson(true);
    }

    public function courses($id)
    {
        $profile = UserEducationProfile::find($id)
            ->load('user');
        $branch_id = $profile->user->branch_id;
        $models = EducationCourse::whereHas('edelement', function ($query) use ($branch_id) {
            $query->where('branch_id', $branch_id);
        })
            ->with('edelement')
            ->get();

        return $this->toJson($models);
    }

    // Постраничный список всех образовательных профилей
    // [ POST /api/user/profiles ]

    public function models(ModelsRequest $request)
    {
        $models = UserEducationProfile::when($request->name, function ($query) {

        })
            ->with('user')
            ->with('edelement');

        $models = $this->paginator(
            $models,
            $request->perPage,
            $request->pageCount,
        );

        return $this->toJson($models);

    }

}