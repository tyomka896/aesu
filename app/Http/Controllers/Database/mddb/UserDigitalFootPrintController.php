<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ModelsRequest;

use App\Models\mddb\UserDigitalFootPrint;
use App\Http\Requests\UserDigitalFootPrint as Requests;

class UserDigitalFootPrintController extends Controller
{
    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }

    // Получение информации о единичном участке цифрового следа
    // [ GET /api/user/dfp/{id} ]
    public function userdigitalfootprint($id)
    {
        $dfp = UserDigitalFootPrint::find($id);

        if (!$dfp) {
            return $this->toJson(['message' => 'Не существует'], 200);
        }

        return $this->toJson($dfp);
    }


    // Получение оцс для профиля
    // [ POST /api/user/{profileId}/dfp ]

    public function profiledfp(ModelsRequest $request, $profileId)
    {
        
        $models = UserDigitalFootPrint::when($profileId, function ($query, $value) {
            $query->where('usereducationprofile_id', $value);
        })
        ->whereDate('created_at', '<=', $request->dateUp)
        ->whereDate('created_at', '>=', $request->dateDown)
        ->get();

        return $this->toJson($models);

    }

    // Создание единичного участка цифрового следа
    // [ POST /api/user/dfp/create ]
    public function create(Requests\CreateRequest $request)
    {
        $model = UserDigitalFootPrint::create($request->validated());

        return $this->toJson($model->fresh());
    }

}