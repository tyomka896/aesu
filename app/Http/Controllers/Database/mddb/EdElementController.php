<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ModelsRequest;
use App\Http\Requests\EdElement as Requests;
use App\Http\Requests\Connection as RequestsConnection;
use App\Http\Requests\MethodEdElement as RequestsMethod;
use App\Http\Requests\TaskEdElement as RequestsTask;
use App\Http\Requests\QuestEdElement as RequestsQuest;
use App\Http\Requests\CompetenceEdElement as RequestsCompetence;

use App\Models\mddb\EdElement;
use App\Models\mddb\DidacticDescription;
use App\Models\mddb\DidacticCore;
use App\Models\mddb\DidacticControl;
use App\Models\mddb\DidacticType;
use App\Models\mddb\EdElementType;
use App\Models\mddb\Connection;
use App\Models\mddb\DiscForm;

class EdElementController extends Controller
{
    //образовательный элемент (от учебного плана до дидактической единицы)

    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }
    //GET /api/edelement/{id}
    public function edelement($id)
    {

        $edelem = EdElement::find($id)
            ->load('didacticdescription')
            ->load('connections');
        if (!$edelem)
            return $this->toJson(['message' => 'Не существует'], 200);

        return $this->toJson($edelem);
    }
    //POST /api/edelement/create
    public function create(Requests\CreateRequest $request)
    {

        $model = EdElement::create($request->validated());
        $id = $model->only('id');

        if ($request->edelementtype_id == '5')//является ли дидактической единицей
        {
            $didacticdescription = $request->only(['type_id', 'importance', 'difficulty', 'core_id', 'viewtype', 'packnumber', 'norms', 'profile', 'filestorage_id', 'test_type']);
            $didacticdescription = array_merge($id, $didacticdescription);
            DidacticDescription::create($didacticdescription);
            $model = EdElement::find($id)
                ->load('didacticdescription');
        }


        return $this->toJson($model->fresh());
    }
    //PUT /api/edelement/{id}
    public function update(Requests\UpdateRequest $request, $id)
    {

        $model = EdElement::find($id)
            ->update($request->validated());
        if ($request->edelementtype_id == '5') {
            $didacticdescription = $request->only(['type_id', 'importance', 'difficulty', 'core_id', 'viewtype', 'packnumber', 'norms', 'profile', 'filestorage_id', 'test_type']);
            
            if (DidacticDescription::find($id) == null) {
                
                $aid = (array) (['id' => $id]);
                //return $this->toJson($aid);
                $didacticdescription = array_merge($aid, $didacticdescription);
                DidacticDescription::create($didacticdescription);
            } else {

                DidacticDescription::find($id)
                    ->update($didacticdescription);
            }
        } else if ($request->edelementtype_id != null) {
            DidacticDescription::find($id)->delete();
        }
        return $this->toJson(true);
    }
    //DELETE /api/edelement/{id}
    public function delete($id)
    {

        Validator::validate(['id' => $id], ['id' => 'exists:edelement']);

        EdElement::find($id)->delete();

        return $this->toJson(true);
    }

    //GET /api/edelement/wbs
    public function edelementwbs()
    {
        $core = DidacticCore::all()->toArray();
        $type = DidacticType::all()->toArray();
        $control = DidacticControl::all()->toArray();
        $edelementtype = EdElementType::all()->toArray();
        $discform = DiscForm::all()->toArray();
        $all = collect([
            'core' => $core,
            'type' => $type,
            'control' => $control,
            'edelementtype' => $edelementtype,
            'discform' => $discform,
        ]);

        return $this->toJson($all);
    }

    //POST /api/edelements

    public function models(ModelsRequest $request)
    {
        $models = EdElement::when($request->parent_id, function ($query, $value) {
            $query->where('parent_id', $value);
        }, function ($query) use ($request) {
            $query->whereNull('parent_id');
        })
            ->when($request->level, function ($query, $value) {
                $query->where('level', $value);
            })
            ->orderBy('order')
            ->with('didacticdescription')
            ->with('connections')
            ->with('methods')
            ->with('tasks')
            ->with('quests')
            ->with('competences');

        if ($request->rootPath)
            $models->with([
                implode('.', array_fill(0, 5, 'shortParent'))
            ]);

        $models = $this->paginator(
            $models,
            $request->perPage,
            $request->pageCount,
        );

        return $this->toJson($models);

    }

    public function leveledmodels(ModelsRequest $request)
    {
        $models = null;
        if ($request->level){
            $models = EdElement::when($request->level, function ($query, $value) {
                $query->where('level', $value - 1);
            })->get();
        }
        else {
            $models = EdElement::all()->select('id','name','level');
        }

        return $this->toJson($models);

    }

    public function allmodels(ModelsRequest $request)
    {
        $models = EdElement::when($request->level, function ($query, $value) {
            $query->where('level', $value - 1);
        })->get();

        return $this->toJson($models);

    }

    //GET /api/edelement/{id}/children

    public function children(ModelsRequest $request, $id)
    {
        Validator::validate([
            'id' => $id,
            'with' => $request->with,
        ], [
            'id' => 'exists:edelement',
            'with.*' => 'in:didacticdescription,edelementtype,disctype,method,personalpreference,newcompetence,quest',
        ]);

        $model = EdElement::find($id)
            ->childrenDown( [
                'level' => $request->level,
                'with' => $request->with,
                'edelement' => $request->edelement,
                'summary' => $request->summary,
            ]);

        return $this->toJson($model);

    }

    //GET /api/edelement/{id}/childrencount

    public function childrenCount($id)
    {
        Validator::validate([
            'id' => $id,
        ], [
            'id' => 'exists:edelement',
        ]);

        $model = EdElement::find($id)
            ->childrenCount();


        return $this->toJson($model->children_count);

    }

    //POST /api/eeconnect
    public function createConnection(RequestsConnection\CreateRequest $request)
    {
        $model = Connection::create($request->validated());
        return $this->toJson($model->fresh());
    }
    //PUT /api/eeconnect/{id}
    public function updateConnection(RequestsConnection\UpdateRequest $request, $id)
    {
        $model = Connection::find($id)
            ->update($request->validated());
        return $this->toJson(true);
    }
    //DEL /api/eeconnect/{id}
    public function deleteConnection($id)
    {
        Validator::validate(['connection_id' => $id], ['connection_id' => 'exists:connection']);

        Connection::find($id)->delete();

        return $this->toJson(true);
    }

    //POST /api/edelementlinkmethod
    public function createMLink(RequestsMethod\CreateRequest $request)
    {
        $edelem = Edelement::find($request->edelement_id);
        if ($edelem->methods()->find($request->method_id))
            return $this->toJson(
                ['message' => 'Такое значение уже существует.'],
                422
            );

        $edelem->methods()->attach($request->method_id);
        return $this->toJson(true);
    }

    //DEL /api/edelementlinkmethod/{id}
    public function deleteMLink(RequestsMethod\CreateRequest $request)
    {

        $edelem = Edelement::find($request->edelement_id);
        if ($edelem->methods()->find($request->method_id)) {
            $edelem->methods()->detach($request->method_id);
            return $this->toJson(true);
        }

        return $this->toJson(
            ['message' => 'Такое значение не существует.'],
            422
        );
    }

    //POST /api/edelementlinktask
    public function createTLink(RequestsTask\CreateRequest $request)
    {
        $edelem = Edelement::find($request->edelement_id);
        if ($edelem->tasks()->find($request->task_id))
            return $this->toJson(
                ['message' => 'Такое значение уже существует.'],
                422
            );

        $edelem->tasks()->attach($request->task_id);
        return $this->toJson(true);
    }

    //DEL /api/edelementlinktask
    public function deleteTLink(RequestsTask\CreateRequest $request)
    {

        $edelem = Edelement::find($request->edelement_id);
        if ($edelem->tasks()->find($request->task_id)) {
            $edelem->tasks()->detach($request->task_id);
            return $this->toJson(true);
        }

        return $this->toJson(
            ['message' => 'Такое значение не существует.'],
            422
        );
    }

    //POST /api/edelementlinkquest
    public function createQLink(RequestsQuest\CreateRequest $request)
    {
        $edelem = Edelement::find($request->edelement_id);
        if ($edelem->quests()->find($request->quest_id))
            return $this->toJson(
                ['message' => 'Такое значение уже существует.'],
                422
            );

        $edelem->quests()->attach($request->quest_id, ['position' => $request->position]);
        return $this->toJson(true);
    }

    //DEL /api/edelementlinkquest
    public function deleteQLink(RequestsQuest\CreateRequest $request)
    {

        $edelem = Edelement::find($request->edelement_id);
        if ($edelem->quests()->find($request->quest_id)) {
            $edelem->quests()->detach($request->quest_id);
            return $this->toJson(true);
        }

        return $this->toJson(
            ['message' => 'Такое значение не существует.'],
            422
        );
    }
    //POST /api/edelementlinkcomp
    public function createCLink(RequestsCompetence\CreateRequest $request)
    {
        $edelem = Edelement::find($request->edelement_id);
        if ($edelem->competences()->find($request->competence_id))
            return $this->toJson(
                ['message' => 'Такое значение уже существует.'],
                422
            );

        $edelem->competences()->attach($request->competence_id);
        return $this->toJson(true);
    }

    //DEL /api/edelementlinkcomp/{id}
    public function deleteCLink(RequestsCompetence\CreateRequest $request)
    {

        $edelem = Edelement::find($request->edelement_id);
        if ($edelem->competences()->find($request->competence_id)) {
            $edelem->competences()->detach($request->competence_id);
            return $this->toJson(true);
        }

        return $this->toJson(
            ['message' => 'Такое значение не существует.'],
            422
        );
    }
}
