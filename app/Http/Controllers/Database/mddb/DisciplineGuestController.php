<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\mddb\Discipline;

class DisciplineGuestController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Дисциплина (для гостя)
    |--------------------------------------------------------------------------
    */

    /**
     * Получение списка дисциплин
     *
     * GET /api/guest/disciplines
     *
     * @param \Illuminate\Http\Request
     */
    public function models(Request $request)
    {
        $discs = Discipline::all();

        $discs = $this->modelsPaginator($discs, $request->perPage, $request->pageCount);

        return $this->toJson($discs);
    }
}
