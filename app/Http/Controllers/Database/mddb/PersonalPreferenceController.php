<?php

namespace App\Http\Controllers\Database\mddb;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ModelsRequest;
use App\Http\Requests\PersonalPreference as Requests;

use App\Models\mddb\PersonalPreference;

class PersonalPreferenceController extends Controller
{
    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }
    /**
     * POST /api/method
     */
    public function create(Requests\CreateRequest $request){
        $model = PersonalPreference::create($request->validated());
        
        return $this->toJson($model->fresh());
    }

    /**
     * DELETE /api/method
     */
    public function delete(Requests\CreateRequest $request)
    {
        if ($request->has([ 'answer_id', 'task_id' ]))
            PersonalPreference::when($request->answer_id, function ($query, $value) {
                $query->where('answer_id', $value);
            })
            ->when($request->task_id, function ($query, $value) {
                $query->where('task_id', $value);
            })
            ->delete();
        if ($request->has([ 'answer_id', 'edelement_id' ]))
            PersonalPreference::when($request->answer_id, function ($query, $value) {
                $query->where('answer_id', $value);
            })
            ->when($request->edelement_id, function ($query, $value) {
                $query->where('edelement_id', $value);
            })
            ->delete();
        if ($request->has([ 'answer_id', 'competence_id' ]))
            PersonalPreference::when($request->answer_id, function ($query, $value) {
                $query->where('answer_id', $value);
            })
            ->when($request->competence_id, function ($query, $value) {
                $query->where('competence_id', $value);
            })
            ->delete();
        
        

        return $this->toJson(true);
    }

    /**
     *  PUT /api/method/{id}
     */
    public function update(Requests\UpdateRequest $request, $id)
    {
        
        $model = PersonalPreference::find($id)
            ->update($request->validated());
        return $this->toJson(true);
    }
    /**
     *  POST /api/methods
     */
    public function models(ModelsRequest $request)
    {
        //$quest_id = $request->quest_id;
        if ($request->connectionType == 3){
            $models = PersonalPreference::when($request->name, function ($query, $value) {
                $words = explode(' ', $value);
    
                foreach ($words as $word) {
                    $query->where('name', 'like', '%'.$word.'%');
                }
            })
            ->when($request->connectionType, function ($query, $value) {
                $query->where('connectionType', $value);
            })
            ->when($request->answer_id, function ($query, $value) {
                $query->where('answer_id', $value);
            })
            // ->whereHas('answer', function ($query) use ($quest_id){
            //     $query->where('quest_id', '=', $quest_id);
            // })
            ->orderBy('id')
            ->get();
        
        }
        else{
            $models = PersonalPreference::when($request->name, function ($query, $value) {
                $words = explode(' ', $value);
    
                foreach ($words as $word) {
                    $query->where('name', 'like', '%'.$word.'%');
                }
            })
            ->when($request->connectionType, function ($query, $value) {
                $query->where('connectionType', $value);
            })
            ->when($request->answer_id, function ($query, $value) {
                $query->where('answer_id', $value);
            })
            // ->whereHas('answer', function ($query) use ($quest_id){
            //     $query->where('quest_id', '=', $quest_id);
            // })
            ->orderBy('id');
        
            $models = $this->paginator(
            $models,
            $request->perPage,
            $request->pageCount,
            );
        }
        

        return $this->toJson($models);

    }
}