<?php

namespace App\Http\Controllers\Database\mddb;

use App\Http\Classes\LCD;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\mddb\Quest;
use App\Models\mddb\Basket;
use App\Models\mddb\Branch;
use App\Models\mddb\Result;
use App\Models\mddb\OverDiscipline;

class OverDisciplineGuestController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Надпредметная оценка (для гостя)
    |--------------------------------------------------------------------------
    */

    /**
     * Расчет надпредметной оценки УРК
     *
     * GET /api/guest/over-discipline/profile
     */
    public function profile(Request $request)
    {
        #region ВАЛИДАЦИЯ ПАРАМЕТРОВ

        $request->validate([
            'periodAt' => 'required',
            'branchId' => 'required',
            'userId' => 'required',
        ], [
            'required' => 'Поле обязательно к заполнению',
        ]);

        $periodAt = Carbon::createFromTimestamp((int) $request->periodAt / 1000);

        if (!$periodAt->isValid())
            return $this->toJson(['message' => 'Не верный формат периода времени'], 422);

        $periodAt = $this->reformatDate($periodAt)->endOfMonth();

        $branch = Branch::find($request->branchId);

        if (!$branch)
            return $this->toJson(['message' => 'Факультативная группа не найдена'], 422);

        $usersID = collect();

        if ($request->userId === '0') {
            $usersID = $branch->users->map(function ($elem) {
                return $elem->id; });
        } else {
            $user = User::find($request->userId);
            if ($user)
                $usersID->push($user->id);
        }

        if ($usersID->count() === 0)
            return $this->toJson(['message' => 'Не найдено ни одного пользователя'], 422);

        #endregion

        #region СБОР ОСНОВНОЙ ИНФОРМАЦИИ

        $branch->load(['disciplines.tests', 'disciplines.competences']);

        $tests = collect();
        foreach ($branch->disciplines as $discipline)
            $tests = $tests->merge($discipline->tests);
        $tests = $tests->unique('id')->where('option', 1)->values();

        // dd($tests->map(function($elem){ return $elem->id; })->join(', '));

        if ($tests->count() === 0)
            return $this->toJson(['message' => 'Тестов для дисциплин факультативной группы не найдено'], 422);

        $competences = collect();
        foreach ($branch->disciplines as $discipline)
            $competences = $competences->merge($discipline->competences);
        $competences = $competences->unique('id')->values();

        if ($competences->count() === 0)
            return $this->toJson(['message' => 'Компетенций для дисциплин факультативной группы не найдено'], 422);

        #endregion

        $overDiscipline = OverDiscipline::profileWhere($request->branchId, $request->userId, $periodAt);

        $overProfiles = OverDiscipline::profilesWhere($request->branchId, $usersID, $periodAt);

        // dump($overDiscipline);

        if ($request->has('update') || !$overDiscipline) {

            $testsID = $tests->map(function ($elem) {
                return $elem->id; });

            foreach ($usersID as $userID) {

                #region СБОР ДАННЫХ

                $baskets = Basket::selectRaw('basket.*')
                    ->join(
                        \DB::raw(
                            '(select user_id, test_id, max(passed_at) as passed_max ' .
                            "from basket where passed_at <= '" . $periodAt->format('Y-m-d') . "' " .
                            'group by user_id, test_id) as bt'
                        ),
                        function ($join) {
                            $join->on('basket.user_id', '=', 'bt.user_id')
                                ->on('basket.test_id', '=', 'bt.test_id')
                                ->on('basket.passed_at', '=', 'bt.passed_max');
                        }
                    )
                    ->join('test', 'basket.test_id', '=', 'test.id')
                    ->where('basket.user_id', '=', $userID)
                    ->where('test.option', '=', 1)
                    ->whereIn('test.id', $testsID)
                    ->get();

                $basketsID = $baskets->map(function ($elem) {
                    return $elem->id; });

                // dump($basketsID);

                $results = Result::whereIn('basket_id', $basketsID)->get();

                $questsID = $results->map(function ($elem) {
                    return $elem->quest_id; });

                $quests = Quest::whereIn('id', $questsID)
                    ->with([
                        'answers',
                        'competences',
                        'questtype',
                    ])
                    ->get();

                #endregion

                #region РАСЧЕТ ПРОФИЛЯ

                $LCD = new LCD();
                $LCD->setBelief($request->coef);

                $profile = $LCD->profile($competences, $quests, $results);

                $overProfile = $overProfiles->where('user_id', $userID)->first();

                if ($overProfile) {
                    $overProfile->profile = $profile;
                    $overProfile->save();
                } else {
                    $createdProfile = OverDiscipline::Create([
                        'branch_id' => $request->branchId,
                        'user_id' => $userID,
                        'profile' => $profile,
                        'period_at' => strtotime($periodAt->year . '-' . $periodAt->month . '-01'),
                    ]);

                    $overProfiles->push($createdProfile);
                }

                #endregion

            }

            #region РАСЧЕТ ГРУППОВОГО ПРОФИЛЯ

            if ($request->userId === '0') {
                $overStatic = collect();

                foreach ($overProfiles as $overProfile) {
                    $values = collect(json_decode($overProfile->profile));

                    foreach ($values as $value) {
                        $compID = $value->comp;

                        if ($overStatic->has($compID)) {
                            $overStatic[$compID]->val->push($value->val);
                        } else {
                            $overStatic->put($compID, $value);
                            $overStatic[$compID]->val = collect($value->val);
                        }
                    }
                }

                $overStatic->each(function ($elem) {
                    $elem->max = (float) number_format($elem->val->max(), 3);
                    $elem->avg = (float) number_format($elem->val->avg(), 3);
                    $elem->min = (float) number_format($elem->val->min(), 3);

                    unset($elem->val);
                });

                if ($overDiscipline) {
                    $overDiscipline->profile = $overStatic->values();
                    $overDiscipline->save();
                } else {
                    $overDiscipline = OverDiscipline::Create([
                        'branch_id' => $request->branchId,
                        'user_id' => 0,
                        'profile' => $overStatic->values(),
                        'period_at' => strtotime($periodAt->year . '-' . $periodAt->month . '-01'),
                    ]);
                }
            } else {
                $overDiscipline = $overProfiles->where('user_id', $request->userId)->first();
            }

            #endregion

        }

        // dump($overDiscipline);

        $profile = collect(json_decode($overDiscipline->profile));

        $competences->each(function ($elem) use ($profile) {
            $value = $profile->where('comp', $elem->id)->first();

            if (!$value)
                return false;

            $elem->setAttribute('v', true)
                ->setAttribute('uc', $value->uc ?? null)
                ->setAttribute('mc', $value->mc ?? null)
                ->setHidden(['pivot']);

            if (isset($value->val)) {
                $elem->setAttribute('val', $value->val);
            } else {
                $elem->setAttribute('max', $value->max)
                    ->setAttribute('avg', $value->avg)
                    ->setAttribute('min', $value->min);
            }
        });

        // dump($competences);
        return $this->toJson($competences);
    }
}
