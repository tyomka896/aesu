<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ModelsRequest;
use App\Http\Requests\Method as Requests;

use App\Models\mddb\Method;

class MethodController extends Controller
{
    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }

    /**
     * POST /api/method
     */
    public function create(Requests\CreateRequest $request)
    {
        $model = Method::create($request->validated());

        return $this->toJson($model->fresh());
    }

    /**
     * DELETE /api/method/{id}
     */
    public function delete($id)
    {

        Validator::validate(['id' => $id], ['id' => 'exists:method']);

        Method::find($id)->delete();

        return $this->toJson(true);
    }

    /**
     *  PUT /api/method/{id}
     */
    public function update(Requests\UpdateRequest $request, $id)
    {

        $model = Method::find($id)
            ->update($request->validated());
        return $this->toJson(true);
    }
    /**
     *  POST /api/methods
     */
    public function models(ModelsRequest $request)
    {
        $models = Method::when($request->name, function ($query, $value) {
            $words = explode(' ', $value);

            foreach ($words as $word) {
                $query->where('name', 'like', '%' . $word . '%');
            }
        })
            // ->when($request->discipline_id, function ($query, $value) {
            //     $query->where('discipline_id', $value);
            // }, function ($query) use ($request) {
            //     $query->whereNull('discipline_id');
            // })
            ->orderBy('id');

        $models = $this->paginator(
            $models,
            $request->perPage,
            $request->pageCount,
        );

        return $this->toJson($models);

    }
}