<?php

namespace App\Http\Controllers\Database\mddb;

use App\Models;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ModelsRequest;
use App\Http\Requests\Competence as Requests;

use App\Models\mddb\Competence;

class CompetenceController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Компетенции
    |--------------------------------------------------------------------------
    */

    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }

    //GET /api/competence/{id}
    public function competence($id)
    {

        $comp = Competence::find($id);
        if (!$comp)
            return $this->toJson(['message' => 'Не существует'], 200);

        return $comp->toJson();
    }

    //POST /api/competence/create
    public function create(Requests\CreateRequest $request)
    {

        $model = Competence::create($request->validated());
        return $this->toJson($model->fresh());
    }

    //PUT /api/competence/{id}
    public function update(Requests\UpdateRequest $request, $id)
    {

        $model = Competence::find($id)
            ->update($request->validated());
        return $this->toJson(true);
    }

    //DELETE /api/competence/{id}
    public function delete($id)
    {

        Validator::validate(['id' => $id], ['id' => 'exists:competence']);

        Competence::find($id)->delete();

        return $this->toJson(true);
    }

    //POST /api/competences

    public function models(ModelsRequest $request)
    {
        $models = Competence::when($request->name, function ($query, $value) {
            $words = explode(' ', $value);

            foreach ($words as $word) {
                $query->where('name', 'like', '%' . $word . '%');
            }
        })
            ->when($request->parent_id, function ($query, $value) {
                $query->where('parent_id', $value);
            }, function ($query) use ($request) {
                $query->whereNull('parent_id');
            })
            ->orderBy('order');

        $models = $this->paginator(
            $models,
            $request->perPage,
            $request->pageCount,
        );

        return $this->toJson($models);

    }

    //GET /api/competence/{id}/children
    public function children(ModelsRequest $request, $id)
    {
        Validator::validate([
            'id' => $id,
            'with' => $request->with,
        ], [
            'id' => 'exists:competence',
            'with.*' => 'in:didacticdescription,edelementtype,disctype,method,personalpreference,newcompetence,quest',
        ]);

        $model = Competence::find($id)
            ->childrenDown([
                'level' => $request->level,
                'with' => $request->with,
            ]);


        return $this->toJson($model->children);

    }

    //GET /api/edelement/{id}/childrencount
    public function childrenCount($id)
    {
        Validator::validate([
            'id' => $id,
        ], [
            'id' => 'exists:competence',
        ]);

        $model = Competence::find($id)
            ->childrenCount();


        return $this->toJson($model->children_count);
    }
}
