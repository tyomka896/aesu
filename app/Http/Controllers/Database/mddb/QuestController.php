<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\mddb\Quest;

class QuestController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Тестовое задание
    |--------------------------------------------------------------------------
    */

    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }

    /**
     * Получение тестового задания
     *
     * GET /api/quest/{id}
     */
    public function quest($id)
    {
        $quest = Quest::find($id);

        if (!$quest) {
            return $this->toJson(['message' => 'Вопрос не найден'], 422);
        }

        $quest->load(['answers']);

        return $this->toJson($quest);
    }

    /**
     * Получение списка тестовых заданий
     *
     * POST /api/quests
     */
    public function quests(Request $request)
    {
        $quests = Quest::query();

        if ($request->has('name')) {
            $quests->where('name', 'like', str_replace("*", "%", $request->name));
        }

        $quests = $quests->with(['answers'])->get();

        $quests = $this->modelsPaginator($quests, $request->perPage, $request->pageCount);

        return $this->toJson($quests);
    }

    /**
     * GET /api/quest/{id}/answers/
     */
    public function answerslist($id)
    {
        $quest = Quest::find($id);

        if (!$quest) {
            return $this->toJson(['message' => 'Не существует'], 200);
        }

        $answers = $quest->answers()
            ->orderBy('position')
            ->get();
        return $answers->toJson();
    }
}
