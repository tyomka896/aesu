<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\mddb\Userstatus;

class UserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Пользователь
    |--------------------------------------------------------------------------
    */

    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }

    /**
     * Получение информации о пользователе
     *
     * GET /api/user/{id}
     */
    public function user($id)
    {
        $user = User::find($id);

        if (!$user)
            return $this->toJson(null);

        $user->load(['userstatus']);

        if ($user->branch) {
            $user->branch->setHidden(['speciality_id'])->speciality;
        }

        return $this->toJson($user);
    }

    /**
     * Получение списка пройденных тестов пользователем
     *
     * GET /api/user/{id}/basket
     */
    public function basket($id)
    {
        $user = User::find($id);

        if (!$user)
            return $this->toJson(['message' => 'Пользователь не найден'], 422);

        $baskets = $user->baskets()->with(['test'])->get();

        $baskets->each(function ($elem) {
            $elem->setHidden([
                'user_id',
                'test_id',
            ]);
        });

        return $this->toJson($baskets);
    }

    /**
     * Получение списка пользователей
     *
     * POST /api/users
     */
    public function users(Request $request)
    {
        $users = User::query();

        if ($request->filled('login'))
            $users->where('login', 'like', str_replace("*", "%", $request->login));
        if ($request->filled('surname'))
            $users->where('surname', 'like', str_replace("*", "%", $request->surname));
        if ($request->filled('name'))
            $users->where('name', 'like', str_replace("*", "%", $request->name));
        if ($request->filled('patronym'))
            $users->where('patronym', 'like', str_replace("*", "%", $request->patronym));

        $users = $users->get();
        $total = $users->count();

        $users = $this->modelsPaginator($users, $request->perPage, $request->pageCount);

        return $this->toJson([
            'users' => $users,
            'total' => $total,
        ]);
    }

    /**
     * Получение упрощённого списка пользователей
     *
     * POST /api/users/short
     */
    public function shortusers(Request $request)
    {
        $users = User::query();

        $users = $users
            ->orderBy('surname')
            ->get();

        $users = $users->makeHidden(['email', 'login', 'created_at', 'updated_at', 'last_visit']);

        return $this->toJson($users);
    }

    /**
     * Получение списка занимаемых должностейPOST
     *
     * GET /api/userstatus
     */
    public function userStatus()
    {
        $statuses = Userstatus::orderBy('order')->get();

        return $this->toJson($statuses);
    }
}
