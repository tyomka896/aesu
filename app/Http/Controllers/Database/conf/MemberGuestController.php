<?php

namespace App\Http\Controllers\Database\conf;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;

use App\Models\conf\Member;

class MemberGuestController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Участник конференции (для гостя)
    |--------------------------------------------------------------------------
    */

    /**
     * Создание участника
     *
     * POST /api/guest/conf/member
     */
    public function create(Request $request)
    {
        $this->validateRequest($request);

        $fields = [
            'surname' => $request->surname,
            'name' => $request->name,
            'patronym' => $request->patronym,
            'status' => $request->status,
            'degree' => $request->degree,
            'organization' => $request->organization,
            'city' => $request->city,
            'email' => $request->email,
            'format' => $request->format,
            'pass' => (int) $request->pass,
            'hotel' => (int) $request->hotel,
        ];

        if ($request->format == 0 && !$request->pass) {
            $birthday = Carbon::createFromTimestamp((int) $request->birthday / 1000);
            $arrival = Carbon::createFromTimestamp((int) $request->arrival / 1000);

            $fields = array_merge($fields, [
                'birthday' => $birthday,
                'birthplace' => $request->birthplace,
                'registered' => $request->registered,
                'work' => $request->work,
                'position' => $request->position,
                'route' => $request->route,
                'arrival' => $arrival,
            ]);
        }

        Member::Create($fields);

        return $this->toJson(['result' => true]);
    }

    /**
     * Проверка верности заполнения полей участника
     */
    protected function validateRequest(Request $request)
    {
        $rules = [
            'surname' => 'required|string|max:50',
            'name' => 'required|string|max:50',
            'patronym' => 'required|string|max:50',
            'status' => 'required|string|max:100',
            'degree' => 'required|string|max:100',
            'organization' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'email' => 'required|email|max:100|unique:conf.member',
            'format' => 'required',
            'pass' => 'required',
            'hotel' => 'required',
            'rsci' => 'accepted',
        ];

        $messages = [
            'required' => 'Поле обязательно к заполнению',
            'max' => 'Максимальная допустимая длина :max символов',
            'email' => 'Не верный формат email-адреса',
            'email.unique' => 'Пользователь с указанным email-адресом уже существует',
            'rsci.accepted' => 'Необходимо дать согласие на индексацию публикации',
            'date' => 'Не верно задан формат даты',
            'agreement.accepted' => 'Необходимо дать согласие на обработку персональных данных',
        ];

        if ($request->format == 0 && !$request->pass) {
            $rules = array_merge($rules, [
                'birthday' => 'required|integer',
                'birthplace' => 'required|string|max:255',
                'registered' => 'required|string|max:255',
                'work' => 'required|string|max:100',
                'position' => 'required|string|max:100',
                'route' => 'required|string|max:100',
                'arrival' => 'required|integer',
                'agreement' => 'accepted',
            ]);
        }

        return $request->validate($rules, $messages);
    }

    /**
     * Получение списка участников
     *
     * GET /api/guest/conf/members
     */
    public function members(Request $request)
    {
        $members = Member::query();

        if ($request->has('surname'))
            $members->whereSurname($request->surname);
        if ($request->has('name'))
            $members->whereName($request->name);
        if ($request->has('patronym'))
            $members->wherePatronym($request->patronym);
        if ($request->has('fullName')) {
            $parts = explode(' ', $request->fullName);

            foreach ($parts as $part) {
                $members->whereRaw('concat(surname, " ", name, " ", patronym) like \'%' . $part . '%\'');
            }
        }

        $members = $this->modelsPaginator($members->get(), $request->perPage, $request->pageCount);

        return $this->toJson($members);
    }
}
