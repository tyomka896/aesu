<?php

namespace App\Http\Controllers\Database\conf;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\conf\Member;

class MemberController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Участник конференции
    |--------------------------------------------------------------------------
    */

    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }

    /**
     * Получение участника
     *
     * GET /api/conf/member/{id}
     */
    public function member($id)
    {
        $member = Member::find($id);

        if (!$member)
            return $this->toJson(['message' => 'Участник конференции не найден'], 422);

        return $this->toJson($member);
    }

    /**
     * Обновление участника
     *
     * PUT /api/conf/member/{id}
     */
    public function update(Request $request, $id)
    {
        abort(501);

        $member = Member::find($id);

        if (!$member)
            return $this->toJson(['message' => 'Участник конференции не найден'], 422);



        return $this->toJson($member);
    }

    /**
     * Удаление участника
     *
     * DELETE /api/conf/member/{id}
     */
    public function delete($id)
    {
        $member = Member::find($id);

        if (!$member)
            return $this->toJson(['message' => 'Участник конференции не найден'], 422);

        $member->delete();

        return $this->toJson(['result' => true]);
    }

    /**
     * Получение списка участников
     *
     * POST /api/conf/members
     */
    public function members(Request $request)
    {
        $members = Member::query();

        if ($request->filled('name'))
            $members->whereName($request->name);
        if ($request->filled('year') && (int) $request->year)
            $members->whereYear('created_at', $request->year);
        if ($request->filled('sort')) {
            if ($request->sort === 'o')
                $members->orderBy('name');  // По имени (А-Я)
            else if ($request->sort === 'n')
                $members->orderByDesc('name');  // По имени (Я-А)

            if ($request->sort === 'co')
                $members->orderBy('created_at');  // По дате (старые)
            else if ($request->sort === 'cn')
                $members->orderByDesc('created_at');  // По дате (новые)
        }

        $members = $members->get();
        $total = $members->count();

        $members = $this->modelsPaginator($members, $request->perPage, $request->pageCount);

        if ($request->has('html')) {
            $members = $members->map(function ($elem) {
                return \View::make('console.conference.parts.member', [
                    'member' => $elem,
                ])->render();
            })->values();
        }

        return $this->toJson([
            'data' => $members,
            'total' => $total,
        ]);
    }
}
