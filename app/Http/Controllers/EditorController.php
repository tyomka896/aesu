<?php

namespace App\Http\Controllers;

use App\Models;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;

use App\Models\conf\Member;
use App\Models\conf\Status;
use App\Models\conf\Article;

/**
 * !!! ПЕРЕНЕСТИ ФУНКЦИОНАЛ НА REST API !!!
 */

class EditorController extends Controller
{
    // >> СПЕЦИАЛЬНОСТИ <<

    public function SpecialityNew(Request $request)
    {
        return view('editor.speciality.create');
    }

    public function SpecialityCreate(Request $request)
    {
        $this->specialityValidate($request);

        $spec = Models\mddb\Speciality::Create([
            'code' => $request->code,
            'name' => $request->name,
            'shortname' => $request->shortname,
        ]);

        return redirect()->route('editor.spec.edit', [$spec->id]);
    }

    public function SpecialityEdit(Request $request, $id)
    {
        $spec = Models\mddb\Speciality::find((int) $id);

        if (!$spec) {
            return redirect()->back()->withErrors($this->serverError('speciality with ID ' . $id . ' was not found'));
        }

        return view('editor.speciality.edit', [
            'edit' => false,
            'spec' => $spec,
            // 'competences' => Models\mddb\Competence::orderBy('name')->get(),
            // 'has' => $spec->competence()->get(),
        ]);
    }

    public function SpecialityUpdate(Request $request, $id) // [ASYNC]
    {
        $spec = Models\mddb\Speciality::find((int) $id);

        if (!$spec) {
            return response()->json(['message' => 'speciality with ID ' . $id . ' was not found'], 500);
        }

        $this->specialityValidate($request, $spec->id);

        $spec->code = $request->code;
        $spec->name = $request->name;
        $spec->shortname = $request->shortname;

        $spec->save();

        $view = \View::make('editor.speciality.parts.info', [
            'spec' => $spec,
        ])->render();

        return response()->json([
            'view' => $view,
        ]);
    }

    public function SpecialityDelete(Request $request, $id)
    {
        $spec = Models\mddb\Speciality::find((int) $id);

        if (!$spec) {
            return response()->json(['message' => 'speciality with ID ' . $id . ' was not found'], 500);
        }

        $spec->delete();

        return redirect()->route('console.spec.main');
    }

    public function SpecialityPairs(Request $request, $id) // [ASYNC]
    {
        $spec = Models\mddb\Speciality::find((int) $request->id);

        if (!$spec) {
            return response()->json($this->serverError('speciality with ID ' . $id . ' was not found'), 500);
        }

        if (!$request->has('edit')) {
            return response()->json($this->serverError('Expected parameter EDIT was not passed'), 500);
        }

        $edit = filter_var($request->edit, FILTER_VALIDATE_BOOLEAN);

        $view = \View::make('editor.speciality.parts.' . ($edit ? 'edit' : 'info'), [
            'spec' => $spec,
        ])->render();

        return response()->json([
            'view' => $view,
        ]);
    }

    // [ DEPRECATED ]
    public function SpecialityCompetenceUpdate(Request $request, $id) // [ASYNC]
    {
        $spec = Models\mddb\Speciality::find((int) $id);

        if (!$spec) {
            return response()->json(['message' => 'speciality with ID ' . $id . ' was not found'], 500);
        }

        $before = $spec->competence()->orderBy('name')->get();
        $after = collect($request->competences)->filter(function ($val) {
            return $val != null && $val > 0;
        });

        // УДАЛЯЕМ НЕДОСТАЮЩИЕ
        foreach ($before as $br) {
            if (!$after->contains($br->id)) {
                $spec->competence()->wherePivot('competence_id', $br->id)->detach();
            }
        }

        // ДОБАВЛЯЕМ НОВЫХ
        $added = $after->filter(function ($val) use ($before) {
            return !$before->contains('id', $val);
        });

        foreach ($added as $add) {
            $cm = Models\mddb\Competence::find($add);
            $spec->competence()->save($cm);
        }

        return response()->json();
    }

    private function specialityValidate(Request $request, $id = 0)
    {
        return $request->validate([
            'name' => [
                'required',
                'max:255',
                Rule::unique('speciality')->ignore($id),
            ],
            'code' => 'required|max:50',
            'shortname' => 'required|max:15',
        ], [
            'name.required' => 'Нименование обязательно к заполнению',
            'name.max' => 'Максимальная длина нименования группы :max символов',
            'name.unique' => 'Группа с указанным нименованием уже существует',
            'code.required' => 'Код специальности обязателен к заполнению',
            'code.max' => 'Максимальная длина кода специальности :max символов',
            'shortname.required' => 'Сокращение обязателено к заполнению',
            'shortname.max' => 'Максимальная длина сокращения :max символов',
        ]);
    }

    // >> ФАКУЛЬТАТИВНЫЕ ГРУППЫ <<

    public function BranchNew(Request $request)
    {
        $specs = Models\mddb\Speciality::orderBy('name')->get();

        return view('editor.branch.create', [
            'specs' => $specs,
        ]);
    }

    public function BranchCreate(Request $request)
    {
        $this->branchValidate($request);

        $branch = Models\mddb\Branch::Create([
            'name' => $request->name,
            'speciality_id' => $request->speciality,
        ]);

        return redirect()->route('editor.branch.edit', [$branch->id]);
    }

    public function BranchEdit(Request $request, $id)
    {
        $branch = Models\mddb\Branch::find((int) $id);

        if (!$branch) {
            return redirect()->back()->withErrors($this->serverError('branch with ID ' . $id . ' was not found'));
        }

        return view('editor.branch.edit', [
            'edit' => false,
            'branch' => $branch,
            'disciplines' => $branch->disciplines()->orderBy('name')->get(),
            'users' => $branch->users()->orderByRaw('concat(surname, " ", name, " ", patronym)')->get(),
        ]);
    }

    public function BranchUpdate(Request $request, $id) // [ASYNC]
    {
        $branch = Models\mddb\Branch::find((int) $id);

        if (!$branch) {
            return response()->json(['message' => 'branch with ID ' . $id . ' was not found'], 500);
        }

        $this->branchValidate($request, $branch->id);

        $branch->name = $request->name;
        $branch->speciality_id = $request->speciality;

        $branch->save();

        $view = \View::make('editor.branch.parts.info', [
            'branch' => $branch,
        ])->render();

        return response()->json([
            'view' => $view,
        ]);
    }

    public function BranchDelete(Request $request, $id)
    {
        $branch = Models\mddb\Branch::find((int) $id);

        if (!$branch) {
            return response()->json(['message' => 'branch with ID ' . $id . ' was not found'], 500);
        }

        $branch->delete();

        return redirect()->route('console.branch.main');
    }

    public function BranchPairs(Request $request, $id) // [ASYNC]
    {
        $branch = Models\mddb\Branch::find((int) $request->id);

        if (!$branch) {
            return response()->json($this->serverError('branch with ID ' . $id . ' was not found'), 500);
        }

        if (!$request->has('edit')) {
            return response()->json($this->serverError('Expected parameter EDIT was not passed'), 500);
        }

        $edit = filter_var($request->edit, FILTER_VALIDATE_BOOLEAN);

        if ($edit) {
            $view = \View::make('editor.branch.parts.edit', [
                'branch' => $branch,
                'specs' => Models\mddb\Speciality::orderBy('name')->get(),
            ]);
        } else {
            $view = \View::make('editor.branch.parts.info', [
                'branch' => $branch,
            ]);
        }

        $view = $view->render();

        return response()->json([
            'view' => $view,
            'branch' => [
                'spec' => $branch->speciality_id,
            ],
        ]);
    }

    public function BranchUserUpdate(Request $request, $id) // [ASYNC]
    {
        $branch = Models\mddb\Branch::find((int) $id);

        if (!$branch) {
            return response()->json(['message' => 'branch with ID ' . $id . ' was not found'], 500);
        }

        $before = $branch->users()->orderByRaw('concat(surname, " ", name, " ", patronym)')->get();
        $after = collect($request->users)->filter(function ($val) {
            return $val != null && $val > 0;
        });

        // УДАЛЯЕМ НЕДОСТАЮЩИЕ
        foreach ($before as $br) {
            if (!$after->contains($br->id)) {
                $br->branch_id = null;
                $br->save();
            }
        }

        // ДОБАВЛЯЕМ НОВЫХ
        $added = $after->filter(function ($val) use ($before) {
            return !$before->contains('id', $val);
        });

        foreach ($added as $add) {
            $ur = Models\User::find($add);
            $ur->branch_id = $branch->id;
            $ur->save();
        }

        return response()->json();
    }

    public function BranchUserSearch(Request $request) // [ASYNC]
    {
        if (!$request->has('param') || $request->param == '') {
            return response()->json([]);
        }

        $params = explode(' ', $request->param);
        $concat = 'concat(surname, " ", name, " ", patronym)';
        $users = Models\User::whereRaw($concat . ' like ?', ['%' . reset($params) . '%']);

        foreach ($params as $param) {
            $users->whereRaw($concat . ' like ?', ['%' . $param . '%']);
        }

        $users = $users->orderByRaw($concat)->get();

        $views = [];

        foreach ($users as $user) {
            $views[] = '<div class="item" data-value="' . $user->id . '">' . $user->name() . '</div>';
        }

        return response()->json(['views' => $views]);
    }

    public function BranchUserPairs(Request $request, $id) // [ASYNC]
    {
        $branch = Models\mddb\Branch::find((int) $id);

        if (!$branch) {
            return response()->json(['message' => 'branch with ID ' . $id . ' was not found'], 500);
        }

        if (!$request->has('edit')) {
            return response()->json($this->serverError('Expected parameter EDIT was not passed'), 500);
        }

        $edit = filter_var($request->edit, FILTER_VALIDATE_BOOLEAN);

        $view = \View::make('editor.branch.parts.tableuser', [
            'edit' => $edit,
            'users' => $branch->users()->orderBy('name')->get(),
        ])->render();

        return response()->json([
            'view' => $view,
        ]);
    }

    private function branchValidate(Request $request, $id = 0)
    {
        return $request->validate([
            'name' => [
                'required',
                'max:50',
                Rule::unique('branch')->ignore($id),
            ],
            'speciality' => function ($attr, $val, $fail) {
                if (!$val)
                    $fail('Направление подготовки обязателено к заполнению');
                else if (!Models\mddb\Speciality::find($val))
                    $fail('Направление подготовки по ID ' . $val . ' не найдена');
            },
        ], [
            'name.required' => 'Нименование обязательно к заполнению',
            'name.max' => 'Максимальная длина нименования группы :max символов',
            'name.unique' => 'Группа с указанным нименованием уже существует',
        ]);
    }

    // >> СПЕЦГРУППЫ <<

    public function SubgroupNew(Request $request)
    {
        return view('editor.subgroup.create');
    }

    public function SubgroupCreate(Request $request)
    {
        $this->subgroupValidate($request);

        $group = Models\mddb\Subgroup::Create([
            'name' => $request->name,
            'portrait' => $request->portrait,
            'owner' => \Auth::user()->id,
        ]);

        return redirect()->route('editor.subgroup.edit', [$group->id]);
    }

    public function SubgroupEdit(Request $request, $id)
    {
        $group = Models\mddb\Subgroup::find((int) $id);

        if (!$group) {
            return redirect()->back()->withErrors($this->serverError('subgroup with ID ' . $id . ' was not found'));
        }

        return view('editor.subgroup.edit', [
            'edit' => false,
            'group' => $group,
            'users' => $group->users()->orderBy('surname')->get(),
            'tests' => $group->tests()->orderBy('name')->get(),
        ]);
    }

    public function SubgroupUpdate(Request $request, $id) // [ASYNC]
    {
        $group = Models\mddb\Subgroup::find((int) $id);

        if (!$group) {
            return response()->json(['message' => 'subgroup with ID ' . $id . ' was not found'], 500);
        }

        $this->branchValidate($request, $group->id);

        $group->name = $request->name;
        $group->portrait = $request->portrait;

        $group->save();

        $view = \View::make('editor.subgroup.parts.info', [
            'group' => $group,
        ])->render();

        return response()->json([
            'view' => $view,
        ]);
    }

    public function SubgroupDelete(Request $request, $id)
    {
        $group = Models\mddb\Subgroup::find((int) $id);

        if (!$group) {
            return response()->json(['message' => 'subgroup with ID ' . $id . ' was not found'], 500);
        }

        $group->delete();

        return redirect()->route('console.subgroup.main');
    }

    public function SubgroupPairs(Request $request, $id) // [ASYNC]
    {
        $group = Models\mddb\Subgroup::find((int) $request->id);

        if (!$group) {
            return response()->json($this->serverError('subgroup with ID ' . $id . ' was not found'), 500);
        }

        if (!$request->has('edit')) {
            return response()->json($this->serverError('Expected parameter EDIT was not passed'), 500);
        }

        $edit = filter_var($request->edit, FILTER_VALIDATE_BOOLEAN);

        $view = \View::make('editor.subgroup.parts.' . ($edit ? 'edit' : 'info'), [
            'group' => $group,
        ])->render();

        return response()->json([
            'view' => $view,
        ]);
    }

    public function SubgroupUserUpdate(Request $request, $id) // [ASYNC]
    {
        $group = Models\mddb\Subgroup::find((int) $id);

        if (!$group) {
            return response()->json(['message' => 'subgroup with ID ' . $id . ' was not found'], 500);
        }

        $before = $group->users()->orderByRaw('concat(surname, " ", name, " ", patronym)')->get();
        $after = collect($request->users)->filter(function ($val) {
            return $val != null && $val > 0;
        });

        // УДАЛЯЕМ НЕДОСТАЮЩИЕ
        foreach ($before as $br) {
            if (!$after->contains($br->id)) {
                $group->users()->wherePivot('user_id', $br->id)->detach();
            }
        }

        // ДОБАВЛЯЕМ НОВЫХ
        $added = $after->filter(function ($val) use ($before) {
            return !$before->contains('id', $val);
        });

        foreach ($added as $add) {
            $sg = Models\User::find($add);
            $group->users()->save($sg);
        }

        return response()->json([]);
    }

    public function SubgroupUserSearch(Request $request) // [ASYNC]
    {
        if (!$request->has('param') || $request->param == '') {
            return response()->json([]);
        }

        $params = explode(' ', $request->param);
        $concat = 'concat(surname, " ", name, " ", patronym)';
        $users = Models\User::whereRaw($concat . ' like ?', ['%' . reset($params) . '%']);

        foreach ($params as $param) {
            $users->whereRaw($concat . ' like ?', ['%' . $param . '%']);
        }

        $users = $users->orderByRaw($concat)->get();

        $views = [];

        foreach ($users as $user) {
            $views[] = '<div class="item" data-value="' . $user->id . '">' . $user->name() . '</div>';
        }

        return response()->json(['views' => $views]);
    }

    public function SubgroupUserPairs(Request $request, $id) // [ASYNC]
    {
        $group = Models\mddb\Subgroup::find((int) $id);

        if (!$group) {
            return response()->json(['message' => 'subgroup with ID ' . $id . ' was not found'], 500);
        }

        if (!$request->has('edit')) {
            return response()->json($this->serverError('Expected parameter EDIT was not passed'), 500);
        }

        $edit = filter_var($request->edit, FILTER_VALIDATE_BOOLEAN);

        $view = \View::make('editor.subgroup.parts.tableuser', [
            'edit' => $edit,
            'users' => $group->users()->orderBy('surname')->get(),
        ])->render();

        return response()->json([
            'view' => $view,
        ]);
    }

    public function SubgroupTestUpdate(Request $request, $id) // [ASYNC]
    {
        $group = Models\mddb\Subgroup::find((int) $id);

        if (!$group) {
            return response()->json(['message' => 'subgroup with ID ' . $id . ' was not found'], 500);
        }

        $before = $group->tests()->orderBy('name')->get();
        $after = collect($request->tests)->filter(function ($val) {
            return $val != null && $val > 0;
        });

        // УДАЛЯЕМ НЕДОСТАЮЩИЕ
        foreach ($before as $br) {
            if (!$after->contains($br->id)) {
                $group->tests()->wherePivot('test_id', $br->id)->detach();
            }
        }

        // ДОБАВЛЯЕМ НОВЫХ
        $added = $after->filter(function ($val) use ($before) {
            return !$before->contains('id', $val);
        });

        foreach ($added as $add) {
            $sg = Models\mddb\Test::find($add);
            $group->tests()->save($sg);
        }

        return response()->json([]);
    }

    public function SubgroupTestSearch(Request $request) // [ASYNC]
    {
        if (!$request->has('param') || $request->param == '') {
            return response()->json([]);
        }

        $params = explode(' ', $request->param);
        $tests = Models\mddb\Test::where('name', 'like', '%' . reset($params) . '%');

        foreach ($params as $param) {
            $tests->where('name', 'like', '%' . reset($params) . '%');
        }

        $tests = $tests->orderBy('name')->get();

        $views = [];

        foreach ($tests as $test) {
            $views[] = '<div class="item" data-value="' . $test->id . '">' . $test->name . '</div>';
        }

        return response()->json(['views' => $views]);
    }

    public function SubgroupTestPairs(Request $request, $id) // [ASYNC]
    {
        $group = Models\mddb\Subgroup::find((int) $id);

        if (!$group) {
            return response()->json(['message' => 'subgroup with ID ' . $id . ' was not found'], 500);
        }

        if (!$request->has('edit')) {
            return response()->json($this->serverError('Expected parameter EDIT was not passed'), 500);
        }

        $edit = filter_var($request->edit, FILTER_VALIDATE_BOOLEAN);

        $view = \View::make('editor.subgroup.parts.tabletest', [
            'edit' => $edit,
            'tests' => $group->tests()->orderBy('name')->get(),
        ])->render();

        return response()->json([
            'view' => $view,
        ]);
    }

    private function subgroupValidate(Request $request, $id = 0)
    {
        return $request->validate([
            'name' => [
                'required',
                'max:50',
                Rule::unique('branch')->ignore($id),
            ],
            'portrait' => 'max:65535',
        ], [
            'name.required' => 'Нименование обязательно к заполнению',
            'name.max' => 'Максимальная длина нименования спецгруппы :max символов',
            'name.unique' => 'Спецгруппа с указанным нименованием уже существует',
            'portrait.max' => 'Максимальная длина описания спецгруппы :max символов',
        ]);
    }

    // >> ДИСЦИПЛИНЫ <<

    public function DisciplineNew(Request $request)
    {
        $statuses = Models\mddb\Disciplinestatus::all();

        return view('editor.discipline.create', [
            'statuses' => $statuses,
        ]);
    }

    public function DisciplineCreate(Request $request)
    {
        $this->disciplineValidate($request);

        $disc = Models\mddb\Discipline::Create([
            'name' => $request->name,
            'portrait' => $request->portrait,
            'owner' => \Auth::user()->id,
            'shortname' => $request->shortname,
            'status_id' => $request->status,
        ]);

        return redirect()->route('editor.disc.edit', [$disc->id]);
    }

    public function DisciplineEdit(Request $request, $id)
    {
        $disc = Models\mddb\Discipline::find((int) $id);

        if (!$disc) {
            return redirect()->back()->withErrors($this->serverError('discipline with ID ' . $id . ' was not found'));
        }

        return view('editor.discipline.edit', [
            'edit' => false,
            'disc' => $disc,
            'branches' => $disc->branches()->orderBy('name')->get(),
            'tests' => $disc->tests()->orderBy('name')->get(),
            'competences' => Models\mddb\Competence::orderBy('name')->get(),
            //			'didactics' => $disc->didactic()->orderBy('name')->get(),
//			'methods' => $disc->method()->orderBy('position')->get(),
//			'tasks' => $disc->task()->orderBy('position')->get(),
            'hasCompetences' => $disc->competences()->get(),
        ]);
    }

    public function DisciplineUpdate(Request $request, $id) // [ASYNC]
    {
        $disc = Models\mddb\Discipline::find((int) $id);

        if (!$disc) {
            return response()->json(['message' => 'discipline with ID ' . $id . ' was not found'], 500);
        }

        $this->disciplineValidate($request, $disc->id);

        $disc->name = $request->name;
        $disc->portrait = $request->portrait;
        $disc->shortname = $request->shortname;
        $disc->status_id = $request->status;

        $disc->save();

        $view = \View::make('editor.discipline.parts.info', [
            'disc' => $disc,
            'branches' => $disc->branches()->orderBy('name')->get(),
        ])->render();

        return response()->json([
            'view' => $view,
        ]);
    }

    public function DisciplineDelete(Request $request, $id)
    {
        $disc = Models\mddb\Discipline::find((int) $id);

        if (!$disc) {
            return response()->json(['message' => 'discipline with ID ' . $id . ' was not found'], 500);
        }

        $disc->delete();

        return redirect()->route('console.disc.main');
    }

    public function DisciplinePairs(Request $request, $id) // [ASYNC]
    {
        $disc = Models\mddb\Discipline::find((int) $request->id);

        if (!$disc) {
            return response()->json($this->serverError('discipline with ID ' . $id . ' was not found'), 500);
        }

        if (!$request->has('edit')) {
            return response()->json($this->serverError('Expected parameter EDIT was not passed'), 500);
        }

        $edit = filter_var($request->edit, FILTER_VALIDATE_BOOLEAN);

        if ($edit) {
            $view = \View::make('editor.discipline.parts.edit', [
                'disc' => $disc,
                'statuses' => Models\mddb\Disciplinestatus::all(),
                'branches' => $disc->branches()->orderBy('name')->get(),
            ]);
        } else {
            $view = \View::make('editor.discipline.parts.info', [
                'disc' => $disc,
                'branches' => $disc->branches()->orderBy('name')->get(),
            ]);
        }

        $view = $view->render();

        return response()->json([
            'view' => $view,
            'disc' => [
                'spec' => $disc->speciality_id,
                'status' => $disc->status_id,
            ],
        ]);
    }

    public function DisciplineBranchUpdate(Request $request, $id) // [ASYNC]
    {
        $disc = Models\mddb\Discipline::find((int) $id);

        if (!$disc) {
            return response()->json(['message' => 'discipline with ID ' . $id . ' was not found'], 500);
        }

        $result = [];

        $before = $disc->branches()->orderBy('name')->get();
        $after = collect($request->branches)->filter(function ($val) {
            return $val != null && $val > 0;
        });

        // УДАЛЯЕМ НЕДОСТАЮЩИЕ
        foreach ($before as $br) {
            if (!$after->contains($br->id)) {
                $disc->branches()->wherePivot('branch_id', $br->id)->detach();
            }
        }

        // ДОБАВЛЯЕМ НОВЫЕ
        $added = $after->filter(function ($val) use ($before) {
            return !$before->contains('id', $val);
        });

        foreach ($added as $add) {
            $ds = Models\mddb\Branch::find($add);
            $disc->branches()->save($ds);
        }

        return response()->json([]);
    }

    public function DisciplineBranchSearch(Request $request) // [ASYNC]
    {
        if (!$request->has('param') || $request->param == '') {
            return response()->json([]);
        }

        $params = explode(' ', $request->param);
        $branches = Models\mddb\Branch::where('name', 'like', '%' . reset($params) . '%');

        foreach ($params as $param) {
            $branches->where('name', 'like', '%' . $param . '%');
        }

        $branches = $branches->orderBy('name')->get();

        $views = [];

        foreach ($branches as $branch) {
            $views[] = '<div class="item" data-value="' . $branch->id . '">' . $branch->name . '</div>';
        }

        return response()->json(['views' => $views]);
    }

    public function DisciplineBranchPairs(Request $request, $id) // [ASYNC]
    {
        $disc = Models\mddb\Discipline::find((int) $id);

        if (!$disc) {
            return response()->json(['message' => 'discipline with ID ' . $id . ' was not found'], 500);
        }

        if (!$request->has('edit')) {
            return response()->json($this->serverError('Expected parameter EDIT was not passed'), 500);
        }

        $edit = filter_var($request->edit, FILTER_VALIDATE_BOOLEAN);

        $view = \View::make('editor.discipline.parts.tablebranch', [
            'edit' => $edit,
            'branches' => $disc->branches()->orderBy('name')->get(),
        ])->render();

        return response()->json([
            'view' => $view,
        ]);
    }

    public function DisciplineCompetenceUpdate(Request $request, $id) // [ASYNC]
    {
        $disc = Models\mddb\Discipline::find((int) $id);

        if (!$disc) {
            return response()->json(['message' => 'discipline with ID ' . $id . ' was not found'], 500);
        }

        $before = $disc->competences()->orderBy('name')->get();
        $after = collect($request->competences)->filter(function ($val) {
            return $val != null && $val > 0;
        });

        // УДАЛЯЕМ НЕДОСТАЮЩИЕ
        foreach ($before as $br) {
            if (!$after->contains($br->id)) {
                $disc->competences()->wherePivot('competence_id', $br->id)->detach();
            }
        }

        // ДОБАВЛЯЕМ НОВЫХ
        $added = $after->filter(function ($val) use ($before) {
            return !$before->contains('id', $val);
        });

        foreach ($added as $add) {
            $cm = Models\mddb\Competence::find($add);
            $disc->competences()->save($cm);
        }

        return response()->json();
    }

    private function disciplineValidate(Request $request, $id = 0)
    {
        return $request->validate([
            'name' => [
                'required',
                'max:255',
                Rule::unique('discipline')->ignore($id),
            ],
            'shortname' => 'required|max:15',
            'status' => function ($attr, $val, $fail) {
                if (!$val)
                    $fail('Статус обязателен к заполнению');
                else if (!Models\mddb\Disciplinestatus::find($val))
                    $fail('Статус по ID ' . $val . ' не найден');
            },
            'portrait' => 'max:65535',
        ], [
            'name.required' => 'Нименование обязательно к заполнению',
            'name.max' => 'Максимальная длина нименования дисциплины :max символов',
            'name.unique' => 'Дисциплина с указанным нименованием уже существует',
            'shortname.required' => 'Абривеатура обязательна к заполнению',
            'shortname.max' => 'Максимальная длина абривеатуры :max символов',
            'portrait.max' => 'Максимальная длина описания теста :max символов',
        ]);
    }

    // >> ТЕСТЫ <<

    public function TestNew(Request $request)
    {
        $discs = Models\mddb\Discipline::orderBy('name')->get();

        return view('editor.test.create', [
            'discs' => $discs,
        ]);
    }

    public function TestCreate(Request $request)
    {
        $this->testValidate($request);

        $test = Models\mddb\Test::Create([
            'name' => $request->name,
            'portrait' => $request->portrait,
            'discipline_id' => $request->discipline,
            'timer' => $request->timer ?? null,
            'owner' => \Auth::user()->id,
            'hidden' => 1,
            'option' => $request->option,
        ]);

        return redirect()->route('editor.test.edit', [$test->id]);
    }

    public function TestEdit(Request $request, $id)
    {
        $test = Models\mddb\Test::find((int) $id);

        if (!$test) {
            return redirect()->back()->withErrors($this->serverError('discipline with ID ' . $id . ' was not found'));
        }

        return view('editor.test.edit', [
            'edit' => false,
            'test' => $test,
            'accessTill' => $this->reformatDate($test->access_till, 'd.m.Y H:i'),
            'quests' => $test->quests()->orderBy('position')->get(),
        ]);
    }

    public function TestUpdate(Request $request, $id) // [ASYNC]
    {
        $test = Models\mddb\Test::find((int) $id);

        if (!$test) {
            return response()->json(['message' => 'test with ID ' . $id . ' was not found'], 500);
        }

        $this->testValidate($request, $test->id);

        $test->name = $request->name;
        $test->portrait = $request->portrait;
        $test->discipline_id = $request->discipline;
        $test->timer = $request->timer ?? null;
        $test->option = $request->option;

        $test->save();

        $view = \View::make('editor.test.parts.info', [
            'test' => $test,
        ])->render();

        return response()->json([
            'view' => $view,
        ]);
    }

    public function TestAccess(Request $request, $id) // [ASYNC]
    {
        $request->validate([
            'accessTill' => 'required',
        ]);

        $test = Models\mddb\Test::find((int) $id);

        if (!$test)
            return $this->toJson(['message' => 'Тест не найден'], 422);

        $accessTill = Carbon::createFromTimestamp((int) $request->accessTill / 1000);

        $test->access_till = $accessTill;
        $test->save();

        return response()->json(['result' => true]);
    }

    public function TestDelete(Request $request, $id)
    {
        $test = Models\mddb\Test::find((int) $id);

        if (!$test) {
            return response()->json(['message' => 'test with ID ' . $id . ' was not found'], 500);
        }

        $test->delete();

        return redirect()->route('console.test.main');
    }

    public function TestPairs(Request $request, $id) // [ASYNC]
    {
        $test = Models\mddb\Test::find((int) $id);

        if (!$test) {
            return response()->json($this->serverError('test with ID ' . $id . ' was not found'), 500);
        }

        if (!$request->has('edit')) {
            return response()->json($this->serverError('Expected parameter EDIT was not passed'), 500);
        }

        $edit = filter_var($request->edit, FILTER_VALIDATE_BOOLEAN);

        if ($edit) {
            $view = \View::make('editor.test.parts.edit', [
                'discs' => Models\mddb\Discipline::orderBy('name')->get(),
                'test' => $test,
            ]);
        } else {
            $view = \View::make('editor.test.parts.info', [
                'test' => $test,
            ]);
        }

        $view = $view->render();

        return response()->json([
            'view' => $view,
            'test' => [
                'disc' => $test->discipline_id,
                'option' => $test->option,
            ],
        ]);
    }

    public function TestQuestUpdate(Request $request, $id) // [ASYNC]
    {
        $test = Models\mddb\Test::find((int) $id);

        if (!$test) {
            return response()->json(['message' => 'test with ID ' . $id . ' was not found'], 500);
        }

        $result = [];

        $before = $test->quests()->orderBy('id')->get();
        $after = collect($request->quests)->unique()->filter(function ($val) {
            return $val != null && $val > 0;
        });

        $bcount = $before->count();
        $acount = $after->count();

        // УДАЛЯЕМ НЕДОСТАЮЩИЕ
        foreach ($before as $br) {
            if (!$after->contains($br->id)) {
                $test->quests()->wherePivot('quest_id', '=', $br->id)->detach();

                $bcount--;
            }
        }

        // ДОБАВЛЯЕМ НОВЫЕ
        $added = $after->filter(function ($val) use ($before) {
            return !$before->contains('id', $val);
        });

        foreach ($added as $add) {
            $qt = Models\mddb\Quest::find($add);

            $test->quests()->save($qt, ['position' => ++$bcount]);
        }

        // ОБНОВЛЯЕМ ПОЗИЦИИ СПИСОКА ВОПРОСОВ
        $after = $after->values();

        for ($q = 0; $q < $acount; $q++) {
            $qt = $test->quests()->wherePivot('quest_id', '=', $after[$q])->get()->first();

            $test->quests()->updateExistingPivot($qt, ['position' => $q + 1]);
        }

        return response()->json([
            'result' => $result,
        ]);
    }

    public function TestQuestSearch(Request $request) // [ASYNC]
    {
        if (!$request->has('param') || $request->param == '') {
            return response()->json([]);
        }

        $params = explode(' ', $request->param);
        $quests = Models\mddb\Quest::where('name', 'like', '%' . reset($params) . '%');

        foreach ($params as $param) {
            $quests->where('name', 'like', '%' . $param . '%');
        }

        $quests = $quests->orderBy('name')->get();

        $views = [];

        foreach ($quests as $quest) {
            $views[] = '<div class="item" data-value="' . $quest->id . '">' . $quest->name . '</div>';
        }

        return response()->json(['views' => $views]);
    }

    public function TestQuestPairs(Request $request, $id) // [ASYNC]
    {
        $test = Models\mddb\Test::find((int) $id);

        if (!$test) {
            return response()->json(['message' => 'test with ID ' . $id . ' was not found'], 500);
        }

        if (!$request->has('edit')) {
            return response()->json($this->serverError('Expected parameter EDIT was not passed'), 500);
        }

        $edit = filter_var($request->edit, FILTER_VALIDATE_BOOLEAN);

        $view = \View::make('editor.test.parts.tablequest', [
            'edit' => $edit,
            'quests' => $test->quests()->orderBy('position')->get(),
        ])->render();

        return response()->json([
            'view' => $view,
        ]);
    }

    private function testValidate(Request $request, $id = 0)
    {
        return $request->validate([
            'discipline' => function ($attr, $val, $fail) {
                if (!$val)
                    return; // $fail('Дисциплина обязательна к заполнению');
                else if (!Models\mddb\Discipline::find($val))
                    $fail('Дисциплина по ID ' . $val . ' не найдена');
            },
            'name' => [
                'required',
                'max:255',
                Rule::unique('test')->ignore($id),
            ],
            'option' => function ($attr, $val, $fail) {
                if (!in_array($val, [0, 1]))
                    $fail('Не верно задан тип теста');
            },
            'timer' => 'integer|min:1|max:65535|nullable',
            'portrait' => 'max:65535',
        ], [
            'name.required' => 'Нименование обязательно к заполнению',
            'name.max' => 'Максимальная длина нименования теста :max символов',
            'name.unique' => 'Тест с указанным нименованием уже существует',
            'timer.integer' => 'Таймер может быть только целочисленным значением',
            'timer.min' => 'Минимальное значение таймера :min секунда',
            'timer.max' => 'Максимальное значение таймера :max секунд',
            'portrait.max' => 'Максимальная длина описания теста :max символов',
        ]);
    }

    // >> ВОПРОСЫ <<

    public function QuestNew(Request $request)
    {
        $types = Models\mddb\Questtype::orderBy('type')->get();

        return view('editor.quest.create', [
            'types' => $types,
        ]);
    }

    public function QuestCreate(Request $request)
    {
        $this->questValidate($request);

        $type = Models\mddb\Questtype::where('type', '=', $request->type)->get()->first();

        $quest = Models\mddb\Quest::Create([
            'name' => $request->name,
            'type' => $type->id,
        ]);

        $answers = [];

        for ($q = 0; $q < count($request->answers); $q++) {
            $answer = [
                'quest_id' => $quest->id,
                'name' => $request->answers[$q],
                'position' => $q + 1,
                'prime' => null,
                'value' => null,
                'minvalue' => null,
                'maxvalue' => null,
            ];

            if (in_array($type->type, [0, 1, 2])) {
                $answer['value'] = $request->values[$q];
            } else if ($type->type == 4) {
                $answer['minvalue'] = $request->min[$q];
                $answer['maxvalue'] = $request->max[$q];
            }

            $answers[] = $answer;
        }

        if ($request->has('primes')) {
            $prime = current($request->primes);
            $answers[$prime - 1]['prime'] = 1;
        }

        Models\mddb\Answer::insert($answers);

        if ($request->next == "on")
            return redirect()->route('editor.quest.new');

        return redirect()->route('console.quest.main');
    }

    public function QuestEdit(Request $request, $id)
    {
        $quest = Models\mddb\Quest::find((int) $id);

        if (!$quest) {
            return response()->json(['message' => 'quest with ID ' . $id . ' was not found'], 500);
        }

        $tests = $quest->tests()->whereNotNull('discipline_id')->get();

        if ($tests->count() != 0) {
            $competences = collect();

            foreach ($tests as $test)
                $competences = $competences->concat($test->discipline->competences()->get());

            $competences = $competences->count() > 0 ? $competences->unique('id')->sortBy('name')->values() : null;
        }

        return view('editor.quest.edit', [
            'quest' => $quest,
            'answers' => $quest->answers()->orderBy('position')->get(),
            'competences' => $competences ?? null,
            'hasCompetences' => $quest->competences()->orderBy('name')->get(),
        ]);
    }

    public function QuestUpdate(Request $request, $id) // [ASYNC]
    {
        $quest = Models\mddb\Quest::find((int) $id);

        if (!$quest) {
            return response()->json(['message' => 'quest with ID ' . $id . ' was not found'], 500);
        }

        $this->questValidate($request, $quest->id);

        $type = Models\mddb\Questtype::where('type', '=', $request->type)->get()->first();

        $prime = $request->primes[0] ?? -1;

        $before = $quest->answers()->orderBy('position')->get();
        $after = collect($request->answers);

        $bcount = $before->count();
        $acount = $after->count();

        // УДАЛЯЕМ НЕДОСТАЮЩИЕ ОТВЕТЫ
        for ($q = $acount; $q < $bcount; $q++) {
            $before[$q]->delete();
            $before->forget($q);
        }

        // ОБНОВЛЯЕМ ОСТАВШИЕСЯ ОТВЕТЫ
        $bcount = $before->count();

        for ($q = 0; $q < $bcount; $q++) {
            $an = $before[$q];

            $an->name = $after[$q];
            $an->position = $q + 1;

            if ($prime - 1 == $q)
                $an->prime = 1;
            else
                $an->prime = null;

            if (in_array($type->type, [0, 1, 2])) {
                $an->value = $request->values[$q];
            }
            if ($type->type == 4) {
                $an->minvalue = $request->min[$q];
                $an->maxvalue = $request->max[$q];
            }

            $an->save();
        }

        // ДОБАВЛЯЕМ НОВЫЕ ОТВЕТЫ
        $new = [];

        for ($q = $bcount; $q < $acount; $q++) {
            $answer = [
                'quest_id' => $quest->id,
                'name' => $request->answers[$q],
                'position' => $q + 1,
                'prime' => null,
                'value' => null,
                'minvalue' => null,
                'maxvalue' => null,
            ];

            if ($prime - 1 == $q)
                $answer['prime'] = 1;

            if (in_array($type->type, [0, 1, 2])) {
                $answer['value'] = $request->values[$q];
            } else if ($type->type == 4) {
                $answer['minvalue'] = $request->min[$q];
                $answer['maxvalue'] = $request->max[$q];
            }

            $new[] = $answer;
        }

        Models\mddb\Answer::insert($new);

        // ОБНОВЛЯЕМ ПАРАМЕТРЫ ВОПРОСА
        $quest->name = $request->name;
        $quest->type = $type->id;

        $quest->save();

        $view = \View::make('editor.quest.parts.info', [
            'quest' => $quest,
            'answers' => $quest->answers()->orderBy('position')->get(),
        ])->render();

        return response()->json([
            'view' => $view,
        ]);
    }

    public function QuestDelete(Request $request, $id)
    {
        $quest = Models\mddb\Quest::find((int) $id);

        if (!$quest) {
            return redirect()->back()->withErrors($this->serverError('quest with ID ' . $id . ' was not found'));
        }

        $quest->delete();

        return redirect()->route('console.quest.main');
    }

    public function QuestPairs(Request $request, $id) // [ASYNC]
    {
        $quest = Models\mddb\Quest::find((int) $request->id);

        if (!$quest) {
            return response()->json($this->serverError('quest with ID ' . $id . ' was not found'), 500);
        }

        if (!$request->has('edit')) {
            return response()->json($this->serverError('Expected parameter EDIT was not passed'), 500);
        }

        $edit = filter_var($request->edit, FILTER_VALIDATE_BOOLEAN);

        if ($edit) {
            $view = \View::make('editor.quest.parts.edit', [
                'quest' => $quest,
                'types' => Models\mddb\Questtype::orderBy('type')->get(),
                'answers' => $quest->answers()->orderBy('position')->get(),
            ]);
        } else {
            $view = \View::make('editor.quest.parts.info', [
                'quest' => $quest,
                'answers' => $quest->answers()->orderBy('position')->get(),
            ]);
        }

        $view = $view->render();

        return response()->json([
            'view' => $view,
            'quest' => [
                'type' => $quest->questtype->type,
            ],
        ]);
    }

    public function QuestCompetenceUpdate(Request $request, $id) // [ASYNC]
    {
        $quest = Models\mddb\Quest::find((int) $id);

        if (!$quest)
            return response()->json(['message' => 'quest with ID ' . $id . ' was not found'], 500);

        $before = $quest->competences()->orderBy('name')->get();
        $after = collect($request->competences)->filter(function ($val) {
            return $val != null && $val > 0;
        });

        // УДАЛЯЕМ НЕДОСТАЮЩИЕ
        foreach ($before as $br) {
            if (!$after->contains($br->id)) {
                $quest->competences()->wherePivot('competence_id', $br->id)->detach();

                $answers = $quest->answers;

                foreach ($answers as $answer) {
                    Models\mddb\Admatrix::where([
                        'answer_id' => $answer->id,
                        'competence_id' => $br->id
                    ])->delete();
                }
            }
        }

        // ДОБАВЛЯЕМ НОВЫХ
        $added = $after->filter(function ($val) use ($before) {
            return !$before->contains('id', $val);
        });

        foreach ($added as $add) {
            $cm = Models\mddb\Competence::find($add);
            $quest->competences()->save($cm);
        }

        $hasCompetences = $quest->competences()->get();

        if ($hasCompetences->count() != 0) {
            $view = \View::make('editor.quest.parts.tablematrix', [
                'answers' => $quest->answers()->orderBy('position')->get(),
                'hasCompetences' => $quest->competences()->orderBy('name')->get(),
            ])->render();
        }

        return response()->json([
            'view' => $view ?? '<center>- НЕТ ВЫБРАННЫХ КОМПЕТЕНЦИЙ -</center>',
        ]);
    }

    public function QuestMatrixUpdate(Request $request, $id)
    {
        $quest = Models\mddb\Quest::find((int) $id);

        if (!$quest)
            return response()->json(['message' => 'quest with ID ' . $id . ' was not found'], 500);

        $answers = $quest->answers;
        $admatrix = $request->admatrixes;
        $competences = $quest->competences()->orderBy('name')->get();

        foreach ($admatrix as $key => $vals) {
            $current = $answers->where('id', $key)->first();

            if (!$current)
                continue;

            $current = $current->admatrixes;

            foreach ($vals as $pos => $val) {
                $compId = $competences[$pos]->id;
                $has = $current->where('competence_id', $compId)->first();

                if ($has) {
                    $row = Models\mddb\Admatrix::where([
                        'answer_id' => $key,
                        'competence_id' => $compId
                    ]);

                    if ($val)
                        $row->update(['value' => $val]);
                    else
                        $row->delete();
                } else {
                    if ($val) {
                        Models\mddb\Admatrix::Create([
                            'answer_id' => $key,
                            'competence_id' => $competences[$pos]->id,
                            'value' => $val,
                        ]);
                    }
                }
            }
        }

        return response()->json();
    }

    private function questValidate(Request $request, $id = 0)
    {
        $request->validate([
            'name' => [
                'required',
                'max:65535',
                Rule::unique('quest')->ignore($id),
            ],
            'type' => 'required|integer|min:0|max:4',
        ], [
            'name.required' => 'Нименование обязательно к заполнению',
            'name.max' => 'Максимальная длина нименования вопроса :max символов',
            'name.unique' => 'Вопрос с указанным Нименованием уже существует',
            'type.required' => 'Тип вопроса обязателен к заполнению',
            'type.min' => 'Минимальное значение для типа вопроса :min',
            'type.max' => 'Максимальное значение типа вопроса :max',
        ]);

        if ($request->type != 2 && !$request->has('answers')) {
            return redirect()->back()->withErrors($this->serverError('quest with selected type should has at least two answers'));
        }

        $hollow = collect($request->answers)->filter(function ($val) {
            return $val == null || trim($val) == "";
        });

        if ($hollow->count() > 0) {
            return redirect()->back()->withErrors($this->serverError('all answers should be filled'));
        }

        return true;
    }

    // >> КОМПЕТЕНЦИИ <<

    public function CompetenceCreate(Request $request)
    {
        $this->competenceValidate($request);

        Models\mddb\Competence::Create([
            'shortname' => $request->shortname,
            'name' => $request->name,
        ]);

        return response()->json();
    }

    public function CompetenceEdit(Request $request, $id)
    {
        $competence = Models\mddb\Competence::find((int) $id);

        if (!$competence) {
            return response()->withErrors($this->serverError('competence with ID ' . $id . ' was not found'));
        }

        return response()->json(['competence' => $competence]);
    }

    public function CompetenceUpdate(Request $request, $id) // [ASYNC]
    {
        $competence = Models\mddb\Competence::find((int) $id);

        if (!$competence) {
            return response()->json(['message' => 'competence with ID ' . $id . ' was not found'], 500);
        }

        $this->competenceValidate($request, $competence->id);

        $competence->shortname = $request->shortname;
        $competence->name = $request->name;

        $competence->save();

        return response()->json();
    }

    public function CompetenceDelete(Request $request, $id)
    {
        $competence = Models\mddb\Competence::find((int) $id);

        if (!$competence) {
            return response()->json(['message' => 'competence with ID ' . $id . ' was not found'], 500);
        }

        $competence->delete();

        return redirect()->route('console.competence.main');
    }

    private function competenceValidate(Request $request, $id = 0)
    {
        return $request->validate([
            'name' => [
                'required',
                'max:65535',
                Rule::unique('competence')->ignore($id),
            ],
        ], [
            'name.required' => 'Нименование обязательно к заполнению',
            'name.max' => 'Максимальная длина нименования компетенции :max символов',
            'name.unique' => 'Компетенция с указанным нименованием уже существует',
        ]);
    }

    // >> ПОЛЬЗОВАТЕЛИ <<

    public function UserEdit(Request $request, $id)
    {
        $user = Models\User::find((int) $id);

        if (!$user) {
            return redirect()->back()->withErrors($this->serverError('user ID ' . $id . ' in basket row ID ' . $id . ' was not found'));
        }

        if ($user->id == \Auth::user()->id) {
            return redirect()->route('profile.main');
        }

        $has = $user->getAllPermissions()->map(function ($val) {
            return strval($val->name);
        });

        $perms = Permission::orderBy('definition')->get();

        $passed = Models\mddb\Basket::where('user_id', '=', $user->id)->get();

        return view('editor.user.edit', [
            'user' => $user,
            'created_at' => $this->reformatDate($user->created_at, 'd.m.Y H:i:s'),
            'last_visit' => $this->reformatDate($user->last_visit, 'd.m.Y H:i:s'),
            'perms' => $perms,
            'has' => $has,
            'passed' => $passed,
        ]);
    }

    public function UserDelete(Request $request, $id)
    {
        $user = Models\User::find((int) $id);

        if (!$user) {
            return response()->json(['message' => 'user with ID ' . $id . ' was not found'], 500);
        }

        $user->delete();

        return redirect()->route('console.user.main');
    }

    public function UserPermission(Request $request, $id) // [ASYNC]
    {
        $user = Models\User::find((int) $id);

        if (!$user) {
            return response()->json(['message' => 'user ID ' . $id . ' was not found'], 500);
        }

        $has = $user->getAllPermissions();
        $perms = collect($request->permissions);

        foreach ($has as $hs) {
            if (!$perms->contains($hs->id)) {
                $user->revokePermissionTo($hs->id);
            }
        }

        $user->givePermissionTo($perms);

        return response()->json(['status' => true]);
    }

    public function UserResetPassword(Request $request, $id) // [ASYNC]
    {
        $user = Models\User::find((int) $id);

        if (!$user) {
            return response()->json(['message' => 'user ID ' . $id . ' was not found'], 500);
        }

        $user->password = Hash::make('123456');
        $user->save();

        return response()->json([]);
    }

    // >> ПРОЙДЕННЫЕ <<

    public function PassedEdit(Request $request, $id)
    {
        $basket = Models\mddb\Basket::find((int) $id);

        if (!$basket)
            return $this->toJson(['message' => 'Результаты теста не найдены'], 422);

        $test = $basket->test;

        if (!$test)
            return $this->toJson(['message' => 'Тест не найден'], 422);

        return view('editor.passed.edit', [
            'basket' => $basket,
            'passed_at' => $this->reformatDate($basket->passed_at, 'd.m.Y H:i:s'),
        ]);
    }

    public function PassedDelete(Request $request, $id)
    {
        $basket = Models\mddb\Basket::find((int) $id);

        if (!$basket) {
            return redirect()->back()->withErrors($this->serverError('basket with ID ' . $id . ' was not found'));
        }

        $basket->delete();

        return redirect()->route('console.passed.main');
    }

    // >> СТАТЬЯ НА КОНФЕРЕНЦИЮ <<

    public function Article(Request $request, $id)
    {
        $article = Article::find($id);

        if (!$article)
            return $this->toJson(['message' => 'Научная статья не найдена'], 422);

        $article->load([
            'section',
            'status',
            'members',
            'documents',
        ]);

        return view('editor.conference.article.edit', [
            'article' => $article,
            'statuses' => Status::all(),
        ]);
    }


    // >> УЧАСТНИК КОНФЕРЕНЦИИ <<

    public function Member(Request $request, $id)
    {
        $member = Member::find($id);

        if (!$member)
            return $this->toJson(['message' => 'Участник конференции не найден'], 422);

        $member->load(['articles']);

        $member->created_at = $this->reformatDate($member->created_at, 'd.m.Y');

        if (!(bool) $member->format) {
            $member->birthday = $this->reformatDate($member->birthday, 'd.m.Y');
            $member->arrival = $this->reformatDate($member->arrival, 'd.m.Y');
        }

        return view('editor.conference.member.edit', [
            'member' => $member,
        ]);
    }

    // >> СОБСТВЕННЫЕ МЕТОДЫ <<

    protected function serverError($msg)
    {
        return [
            'header' => '500 Internal Server Error',
            'message' => 'ERROR: ' . $msg,
        ];
    }
}
