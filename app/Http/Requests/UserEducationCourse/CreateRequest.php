<?php

namespace App\Http\Requests\UserEducationCourse;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest{
    public function rules()
    {
        return [
            'usereducationprofile_id' => 'exists:usereducationprofile,id|required',
            'current_edelement_id' => 'exists:edelement,id',
            'score' => 'numeric',
            'educationcourse_id' => 'exists:educationcourse,id|required',
            'finished' => 'numeric', 
        ];
    }

}
