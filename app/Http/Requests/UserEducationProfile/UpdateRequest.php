<?php

namespace App\Http\Requests\UserEducationProfile;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest{

    protected function prepareForValidation()
    {
        $this->merge([ 'id' => $this->route('id') ]);
    }

    public function rules()
    {
        return [
            'id' => 'exists:usereducationprofile',
            'edelement_id' => 'exists:edelement,id',
            'started_at' => 'date',
            'finished_at' => 'date',
        ];
    }

}
