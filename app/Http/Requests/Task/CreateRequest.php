<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest{
    public function rules()
    {
        return [
            'name' => 'max:100|required',
            'discipline_id' => 'exists:discipline,id|required',
        ];
    }

}