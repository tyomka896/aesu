<?php

namespace App\Http\Requests\EducationCourse;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest{
    public function rules()
    {
        return [
            'name' => 'max:150|required',
            'about' => 'max:1000',
            'edelement_id' => 'exists:edelement,id',
        ];
    }

}
