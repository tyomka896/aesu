<?php

namespace App\Exceptions;

use Throwable;
use App\Models\Logs;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

use App\Models\log\Laravel;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $e
     * @return void
     */
    public function report(Throwable $e)
    {
        parent::report($e);

        if ($this->shouldntReport($e)) {
            return;
        }

        if (Storage::disk('logs')->exists('laravel.sqlite')) {
            $logs = Laravel::whereUrl(\Request::url())
                ->whereFile($e->getFile())
                ->whereLine($e->getLine())
                ->whereMessage($e->getMessage())
                ->get();

            if ($logs->count() > 0) {
                Laravel::whereId($logs->first()->id)
                    ->increment('count');
            }
            else {
                $trace = $e->getTraceAsString();

                Laravel::Create([
                    'url' => \Request::url(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'message' => $e->getMessage(),
                    'trace' => implode("\n", array_slice(explode("\n", $trace), 0, 10)),
                    'count' => 1,
                ]);
            }
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Throwable $e)
    {
        if ($e instanceof ValidationException){
            return response()->json([
                'message' => 'Данные неверно заполнены',
                'errors' => $e->validator->getMessageBag()
            ], 422);
        }

        return parent::render($request, $e);
    }
}
